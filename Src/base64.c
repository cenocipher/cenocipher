#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//output_size = ((input_size - 1) / 3) * 4 + 4;
const char * basetable="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const int padchar='=';

void Base64Encode(char *input, char *output, int bytes)
{
   while (bytes>0)
   {
        output[0] = basetable[ (input[0] >> 2) & 63 ];
        output[1] = basetable[ ((input[0] & 0x03) << 4) | ((input[1] & 0xf0) >> 4) ];
        output[2] = (bytes > 1 ? basetable[ ((input[1] & 0x0f) << 2) | ((input[2] & 0xc0) >> 6) ] : padchar);
        output[3] = (bytes > 2 ? basetable[ input[2] & 0x3f ] : padchar);
        input+=3;output+=4;bytes-=3;
   }
   return;
}

void Base64Decode(char *input, char *output)
{
   int chars=strlen(input)-strlen(input)%4;
   while(chars>0)
    {
        int padchars=(memchr(&input[2],padchar,1)>0) + (memchr(&input[3],padchar,1)>0) + (memchr(&input[1],padchar,1)>0) + (memchr(&input[0],padchar,1)>0);
        if (padchars<4){output[0]=(((strchr(basetable,input[0]))-basetable) << 2) | (((strchr(basetable,input[1]))-basetable) >> 4);}
        if (padchars<2){output[1]=(((strchr(basetable,input[1]))-basetable) << 4) | (((strchr(basetable,input[2]))-basetable) >> 2);}
        if (padchars<1){output[2]=(((strchr(basetable,input[2]))-basetable) << 6) | (((strchr(basetable,input[3]))-basetable) >> 0);}
        output+=3;input+=4;chars-=4;
    }
    return;
}


