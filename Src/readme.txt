*  CenoCipher 4.0
 *
 *  Copyright (C) 2015, the CenoCipher development team
 *
 *  All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 

This software was built using the GNU C Compiler (GCC/MinGW 4.7.1 32-bit) and Code::Blocks version 13.12 for Windows, so the easiest way to build it from source yourself is to first download Code::Blocks from this page (at the time of this writing) :

http://www.codeblocks.org/downloads/26

Install Code::Blocks on your computer, and then use the File menu to open the project file "CenoCipher2.cbp" included in this archive.

Then select "Build and Run" from the Build menu, and it should compile pretty quickly.

If you are familiar with C++ you can also try using MinGW on its own or with any other IDE to achieve the same result.

