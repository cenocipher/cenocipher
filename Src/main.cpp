/*
 *
 *  CenoCipher 4.0
 *
 *  Copyright (C) 2015, the CenoCipher development team
 *
 *  All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

// Needed for MinGW:
#ifndef SHGFP_TYPE_CURRENT
#define SHGFP_TYPE_CURRENT 0
#endif

#define _WIN32_IE 0x0300
#define _WIN32_WINNT 0x0501

#include <gdiplus.h>
using namespace Gdiplus;

#include <tchar.h>
#include <windows.h>
#include <richedit.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <shlobj.h>
#include <ctime>
#include "sha256.h"
#include "IsaacRand.hh"
#include "aes.h"
#include "twofish.h"
#include "std_defs.h"
#include <windef.h>
#include <winnt.h>
#include <vector>
using namespace std;


//Macros
#define safescat(d,s,args...) safescatfunc((char*)d,(char*)s,max((int)(sizeof(d)-strlen((char*)d)-1),0),##args)
inline void safescatfunc(char* d, char* s, size_t maxsize, size_t reqsize=0)
{strncat((char*)d,(char*)s, reqsize == 0 ? maxsize : min(reqsize,maxsize));}

#define safescopy(d,s,args...) safescopyfunc(d,(char*)s,sizeof(d)-1,##args)
inline void safescopyfunc(char* d, char* s, size_t maxsize, size_t reqsize=0)
{strncpy(d,s, reqsize == 0 ? maxsize : min(reqsize,maxsize));}


//Declare functions
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WindowProcedure2 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WindowProcedure3 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureD (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureH (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK OutListProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK OptsProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK EnterClickProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK KeepSelProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK EditorStaticProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK EditorStaticProc2 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK FolderDialogProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK FolderDialogProc2 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK FolderDialogProc3 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureI (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureO (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureC(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureW(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
STDAPI SHGetTargetFolderPath(LPCITEMIDLIST pidlFolder, LPWSTR pszPath, UINT cchPath);
DWORD CALLBACK EditStreamCallback(DWORD_PTR dwCookie,LPBYTE pbBuff,LONG cb,LONG *pcb);
DWORD CALLBACK EditStreamCallbackCheck(DWORD_PTR dwCookie,LPBYTE pbBuff,LONG cb,LONG *pcb);
BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam);
BOOL CALLBACK EnumDWFunction(HWND hwnd,LPARAM lparam);
UINT_PTR CALLBACK FolderSelect3(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
DWORD WINAPI EncryptComm(long v=(long)NULL);
DWORD WINAPI DecryptComm(long v);
HBITMAP LoadPictResource(LPCTSTR resId, LPCTSTR resType);
HRESULT CreateDropSource(IDropSource **ppDropSource);
HRESULT CreateDataObject(FORMATETC *fmtetc, STGMEDIUM *stgmeds, UINT count, IDataObject **ppDataObject);
string Str(int i);
string Padleft(string s, unsigned int num, char c);
long long OEditStreamOutCheck();
int ScrollWindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
int ListJFiles(char refcstrRootDirectory[MAX_PATH*2+1], vector<char*>& jpegs,long long neededsize);
int DeleteDirectory(char refcstrRootDirectory[MAX_PATH],bool bDeleteSubdirectories=true);
static void sureset( void *v, unsigned char u, size_t n );
void OEditStreamOut();
void PKCS5_PBKDF2_HMAC(unsigned char *password, size_t plen,unsigned char *salt, size_t slen,const unsigned long iteration_count, const unsigned long key_length,unsigned char *output);
void LinkCatcher(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
void SizeWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
void ZeroFile(char* dfile);
void Mbox(string a);
void FontBox(HWND hwnd);
void MakeBitmap(LPCTSTR resId, HBITMAP* h, int width, int height);
void SaveFolderRequestor(char savepath[MAX_PATH*2+1]);
void syme(int mode=0, const char* text="",HWND flash=0);
void Addicons(HWND hwnd);
void EncReady(void * kset);
void StegOption (void * kset);
void ContAck(int mode, int source, char * p);
void FolderOutputRequestor(HWND afolder=0,int mode=0);
void ShowPrompt(int mode=0);
void PromptAction(int choice);
void Testo(),BoldToggle(),ItalicToggle(),UlineToggle(),FileRequestor(),FileLoader(),InsToggle(),PMaskEToggle(),ShowOptsToggle(),SavePrefs(),CheckPrefs(),DesktopCopy(),PMaskDToggle();
void OpenInListFiles(int mode=0),ReplyWithQuote(),ShowHelp(),JpegRequestor(int mode=0),JFolderRequestor(),ClearDecDetails(),ClipLoader(),oletest(long whatever),QuickHelp(),NewMessage();
bool RtlFallback(PVOID,ULONG);
int CheckStatus();
long redge(HWND h);

extern "C" int cenostegembed (const char* infilename,const char* outfilename,const char* hidefilename,const char* pseudokey,long rseed);
extern "C" int cenostegextract (const char* infilename,const char* outfilename, const char * pseudokey);
extern "C" void Base64Encode(char *input, char *output, int bytes);
extern "C" void Base64Decode(char *input, char *output);


//Declare constants
const char * filetableheader="\x1b\x1b\x1b///filetable///\x1b\x1b\x1b";
const long segmentsize=1000000;
const unsigned long prounds=100000;
const size_t defmin=4000000;
const size_t defmax=8000000;


//Declare global variables
TCHAR szClassName[ ] = _T("CCWindow");
HWND hwndButton,hwndButton2,OutEdit,OutEdit2,hwndmain,DWindow,FontButton,BoldButton,ItalicButton,UlineButton,NewTextButton,OutList,OutSectiontitle,OutPass,EncryptButton,AddFilesButton ;
HWND RemoveFilesButton, ShowInsBox, PMaskE,MoveToButton,CFileHolder,MoveDest,ShowOpts,OptsHolder,RandExtOpt,ExtName,CustNameOpt,CustName,EncProg,EncStat;
HWND InSectiontitle,InEdit,BrowseFileButton,InList,DecryptButton,DecProg,DecStat,InPass,PMaskD,OpenFilesButton,SaveFilesButton,ReplyButton,IncludeFiles,MoreHelpButton,HelpEdit1,HelpEdit2;
HWND IllusButton,picbox1,picbox2,Linkbox,ShowOpts2,FileOpt1,FileOpt2,OptsHolder2,SpecificJpegOpt,SpecificJpegName,RandomJpegOpt,RandomJpegName,ChooseJpeg,ChooseJFolder;
HWND ClearPassword,WithQuote,EncDestOpt1,EncDestOpt2,EncDestOpt3,EncFormatOpt1,EncFormatOpt2,EncFormatOpt3,EncDestList,OptsHolder3,FolderOutput,ChooseFolderOutput;
HWND DecryptButton1,DecryptButton2,SysMessage,AutoDecrypt,InSectiontitle2,InOptsHolder,OthOptsHolder,OptsHolder4,OptsHolder5,TextLabel2,SaveSettings,OthTitle1;
HWND OptPrompt,OptUseDefault,OptUseSpecified,ChooseSpecFolder,SpecFolder,AffectFolder,PromptUseSpecified,PromptUseDefault,PromptSpecFolder,PromptChooseSpecFolder;
HWND PromptOkay,PromptCancel,PromptAbort,promptmess,NoIns,OutIns,IncIns,QuickHelpButton,OutBlocker,OutBlocker2,WaitWindow,WaitLabel,QuickOkay,QuitButton;
HWND HelpWindow=0,PromptWindow=0;
HBITMAP ebuttonpic1,ebuttonpic0,ebuttonpic2,dbuttonpic0,dbuttonpic1,helpimage1,helpimage2;
HFONT arial14,arial14b,arial12,arial12u,segoe16,webdings108,hsym;
HINSTANCE hti;
HANDLE mtx, mtx2;
COLORREF labelinstext=RGB(0,0,0),labelinsback=RGB(233,236,216),labelinsback2=RGB(150,175,150);
RECT maxrect,viewrect;
int hpos,vpos,initwait,enctraffic=0,dectraffic=0,clipflag=0,commode=0,formode=1,desmode=1,promptresult=0,promptver=0,insmode=0;
long dproc,lvproc,emproc,ofnproc;
long long messbytes;
double hhorizontal,vvertical,fontratio;
char * messbuff=NULL;
#define vv *vvertical
#define hh *hhorizontal

typedef bool (WINAPI *RTLGR)(PVOID,ULONG);
RTLGR RtlGenRandom;


//Create objects
HBRUSH beigebrush=CreateSolidBrush(RGB(233,236,216));
COLORREF beigecolor=RGB(233,236,216);
HBRUSH greenbrush=CreateSolidBrush(RGB(150,175,150));
COLORREF greencolor=RGB(150,175,150);
HPEN greenpen=CreatePen(PS_SOLID, 1, RGB(150,175,150));
HPEN sagepen=CreatePen(PS_SOLID, 1, RGB(200,250,200));
HBRUSH sagebrush=CreateSolidBrush(RGB(200,250,200));
COLORREF sagecolor=RGB(200,250,200);
COLORREF ccolor=RGB(180,200,180);
HPEN greypen=CreatePen(PS_SOLID, 1, RGB(100,100,100));
HBRUSH greybrush=CreateSolidBrush(RGB(80,80,80));
COLORREF greycolor=RGB(80,80,80);
HPEN othpen,inpen,outpen,outpen2,outpen3;

COLORREF othecolor[3][3]={};
HBRUSH othebrush[3][3]={};
COLORREF incolor[3][3]={};
HBRUSH inbrush[3][3]={};
COLORREF outcolor[5][3]={};
HBRUSH outbrush[5][5]={};
COLORREF maincolor[5][5]={};

//Custom structures
struct LABELSTRUCT
{
    HWND h;
    char help[255];
    char nohelp[255];
    char ihelp[255];
} labels[15]={};

struct DecInfo
{
    int cformat;
    char cfile[MAX_PATH*2+1];
    STGMEDIUM * cstgmed;
    IStream * csp;
    int dformat;
    char dfile[MAX_PATH*2+1];
    unsigned char * dbuffer;
    long long dbuffsize;
    char sfile[MAX_PATH*2+1];
}  DecDetails={};

struct keyset
{
    unsigned char buffer[segmentsize];
    unsigned char key[32];
    unsigned char iv[16];
    unsigned char salt[16];
    Twofish_Byte twofishkey[32];
    unsigned char twofishiv[16];
    unsigned char twofishsalt[16];
    unsigned char serpentkey[32];
    unsigned char serpentiv[16];
    unsigned char serpentsalt[16];
    size_t ANCOffset;
    size_t TNCOffset;
    size_t SNCOffset;
    unsigned char astreamblock[16];
    unsigned char tstreamblock[16];
    unsigned char sstreamblock[16];
    unsigned char hmackey[32];
    unsigned char hmachash[32];
    unsigned char hmachashresult[32];
    unsigned char passkey[255];
    aes_context aes;
    Twofish_key tkey;
    char cipherfile[MAX_PATH*2+1];
    char stegofile[MAX_PATH*2+1];
    unsigned char pseudosalt[32];
    unsigned char pseudokey[32];
    unsigned iseeds[40];
    unsigned char obfhash[32];
    unsigned char obfheader[256];
    unsigned char oldheader[256];
    unsigned randdist;
};


char SaveFilePath[MAX_PATH*2+1]={};

const long headersize=sizeof(keyset().hmachash)+sizeof(keyset().iv)+sizeof(keyset().salt)+sizeof(keyset().twofishiv)+sizeof(keyset().twofishsalt)+sizeof(keyset().serpentiv)+sizeof(keyset().serpentsalt);
HANDLE hProcess = GetCurrentProcess();
#include "CDropTarg.cpp"


//Main window creation procedure
int WINAPI WinMain (HINSTANCE hThisInstance,HINSTANCE hPrevInstance,LPSTR lpszArgument,int nCmdShow)
{

    if (strlen(lpszArgument)>0)
    {

        HWND fw=FindWindow(NULL,"CenoCipher");
        if (fw!=NULL)
        {

            COPYDATASTRUCT cds={};
            strrchr(lpszArgument,'"')[0]='\0';
            cds.cbData=strlen(strchr(lpszArgument,'"')+1);
            cds.lpData=strchr(lpszArgument,'"')+1;
            SendMessage(fw,WM_COPYDATA,0,(LPARAM)&cds);
            SetForegroundWindow(fw);
            printf("Cdata: %s \n",cds.lpData);
            Sleep(5000);
            sureset(&cds,0,sizeof(cds));
            return 0;
        }
    }

    RtlGenRandom =(RTLGR)GetProcAddress(LoadLibrary("AdvAPI32.dll"),"SystemFunction036");
    if(RtlGenRandom==0){RtlGenRandom=(RTLGR)(&RtlFallback);}


    OleInitialize(0);
    HWND hwnd;
    MSG messages;
    WNDCLASSEX wincl;

    srand(time(0));
    mtx=CreateMutex(NULL,FALSE,NULL);
    mtx2=mtx;

    SetProcessWorkingSetSize(hProcess,defmin,defmax);
    VirtualLock(&DecDetails,sizeof(DecDetails));
    VirtualLock(&labels,sizeof(labels));
    VirtualLock(SaveFilePath,sizeof(SaveFilePath));


    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;
    wincl.style = CS_DBLCLKS;
    wincl.cbSize = sizeof (WNDCLASSEX);
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;
    wincl.cbClsExtra = 0;
    wincl.cbWndExtra = 0;
    wincl.hbrBackground = beigebrush;


    if (!RegisterClassEx (&wincl)){return 0;}

    initwait=1;
    RECT desktoprect={};
    SystemParametersInfo(SPI_GETWORKAREA,0,&desktoprect,0);

    hwnd = CreateWindowEx (0,szClassName,_T("CenoCipher"),WS_OVERLAPPEDWINDOW|WS_HSCROLL|WS_VSCROLL|WS_MAXIMIZE,(int)((double)desktoprect.right*0.125),(int)((double)desktoprect.bottom*0.125),(int)((double)desktoprect.right*0.75),(int)((double)desktoprect.bottom*0.75),HWND_DESKTOP,NULL,hThisInstance,NULL);

    //Calculations related to window size
    initwait=0;
    hwndmain=hwnd;
    hti=hThisInstance;
    GetClientRect(hwnd,&maxrect);
    hhorizontal=(double)(maxrect.right-maxrect.left)/(double)1680;
    vvertical=(double)(maxrect.bottom-maxrect.top)/(double)988;
    HDC hdc=GetDC(hwnd);
    fontratio=(double)96/(double)GetDeviceCaps(hdc,LOGPIXELSY);
    double picratio;
    hhorizontal<vvertical ? picratio=hhorizontal : picratio=vvertical;
    fontratio*=picratio;
    if (picratio<=0.625){fontratio-=0.01;}
    int ps = -MulDiv(12*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps16 = -MulDiv(18*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps11 = -MulDiv(11*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps14 = -MulDiv(14*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps13 = -MulDiv(13*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps16z = -MulDiv(16*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps8 = -MulDiv(8*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps108 = -MulDiv(108*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int ps100 = -MulDiv(100*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    ReleaseDC(hwnd,hdc);
    HFONT hf=CreateFont(ps,0,0,0,400,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
    HFONT hf2=CreateFont(ps16z,0,0,0,400,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Segoe UI");
    HFONT hf3=CreateFont(ps8,0,0,0,400,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
    HFONT hft=CreateFont(ps11,0,0,0,400,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
    HFONT hfab=CreateFont(ps,0,0,0,FW_BOLD,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
    HFONT hfb=CreateFont(ps16,0,0,0,FW_ULTRABOLD,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"times new roman");
    HFONT hfi=CreateFont(ps16,0,0,0,FW_NORMAL,true,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"times new roman");
    HFONT hfu=CreateFont(ps16,0,0,0,FW_NORMAL,0,true,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"times new roman");
    HFONT hfsu=CreateFont(ps16,0,0,0,FW_NORMAL,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Segoe UI");
    HFONT hff=CreateFont(ps16,0,0,0,FW_NORMAL,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Bookman Old Style");
    HFONT hfal=CreateFont(ps14,0,0,0,FW_NORMAL,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Tahoma");
    HFONT hfwd=CreateFont(ps,0,0,0,FW_BOLD,0,0,0,SYMBOL_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,ANTIALIASED_QUALITY,FF_DONTCARE,"Wingdings");
    arial14=CreateFont(ps13,0,0,0,FW_NORMAL,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Verdana");
    arial14b=CreateFont(ps14,0,0,0,FW_BOLD,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,ANTIALIASED_QUALITY,FF_DONTCARE,"Arial");
    arial12u=CreateFont(ps,0,0,0,FW_NORMAL,0,true,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
    webdings108=CreateFont(ps108,0,0,0,FW_NORMAL,0,0,0,SYMBOL_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,ANTIALIASED_QUALITY,FF_DONTCARE,"Wingdings");
    HFONT arial100=CreateFont(ps100,0,0,0,FW_BOLD,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,ANTIALIASED_QUALITY,FF_DONTCARE,"Arial");
    arial12=hf;
    segoe16=hf2;
    hsym=hfwd;


    othecolor[0][0]=RGB(220,200,200);
    othecolor[1][0]=RGB(200,180,180);
    othecolor[0][1]=othecolor[1][1]=othecolor[0][2]=othecolor[1][2]=greycolor;
    othebrush[0][0]=CreateSolidBrush(othecolor[0][0]);
    othebrush[1][0]=CreateSolidBrush(othecolor[1][0]);
    othebrush[0][1]=othebrush[1][1]=othebrush[0][2]=othebrush[1][2]=greybrush;

    outcolor[0][0]=outcolor[0][1]=RGB(200,250,200);
    outcolor[1][0]=outcolor[1][1]=RGB(150,175,150);
    outcolor[2][0]=RGB(150,175,150);
    outcolor[2][1]=RGB(255,255,100);
    outcolor[3][0]=outcolor[3][1]=RGB(180,200,180);
    outcolor[0][2]=outcolor[1][2]=outcolor[2][2]=outcolor[3][2]=greycolor;
    outcolor[4][1]=RGB(0,0,255);
    outbrush[0][0]=outbrush[0][1]=CreateSolidBrush(outcolor[0][0]);
    outbrush[1][0]=outbrush[1][1]=CreateSolidBrush(outcolor[1][0]);
    outbrush[2][0]=CreateSolidBrush(outcolor[2][0]);
    outbrush[3][0]=outbrush[3][1]=CreateSolidBrush(outcolor[3][0]);
    outbrush[3][2]=outbrush[2][2]=outbrush[1][2]=outbrush[0][2]=greybrush;

    incolor[0][0]=RGB(210,210,255);
    incolor[0][2]=RGB(255,0,255);
    incolor[1][0]=incolor[1][2]=(RGB(180,180,255));
    incolor[2][2]=RGB(255,255,0);
    incolor[0][1]=incolor[1][1]=greycolor;

    inbrush[0][0]=inbrush[0][2]=CreateSolidBrush(incolor[0][0]);
    inbrush[1][0]=inbrush[1][2]=CreateSolidBrush(incolor[1][0]);
    inbrush[0][1]=inbrush[1][1]=greybrush;

    maincolor[1][0]=maincolor[1][2]=maincolor[2][0]=maincolor[2][2]=beigecolor;
    maincolor[1][1]=maincolor[2][1]=outcolor[2][1];
    maincolor[2][2]=incolor[0][2];
    maincolor[3][1]=maincolor[4][1]=RGB(0,0,255);
    maincolor[4][2]=RGB(255,255,50);


    SCROLLINFO si={};
    si.cbSize=sizeof(SCROLLINFO);
    si.fMask=SIF_PAGE|SIF_POS|SIF_RANGE;
    si.nMin=1;
    si.nMax=101;
    si.nPage=50;
    si.nPos=1;
    LPCSCROLLINFO lpsi=&si;
    SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
    LPSCROLLINFO lpsi3=&si;
    GetScrollInfo(hwnd,SB_HORZ,lpsi3);


    HWND hwnd2 = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,350 hh,-15 vv,1680 hh,1025 vv,hwnd,NULL,hThisInstance,NULL);
    SetWindowLong(hwnd2,GWL_WNDPROC,(long)WindowProcedure2);
    SetWindowLong(hwnd2,GWL_STYLE,0);
    SetWindowLong(hwnd2,GWL_STYLE,WS_CHILD);
    SetWindowPos(hwnd2, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(hwnd2,SW_SHOW);

    DWindow = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,0 hh,0 vv, 350 hh, 1025 vv,hwnd,NULL,hThisInstance,NULL);
    SetWindowLong(DWindow,GWL_WNDPROC,(long)WindowProcedureD);
    SetWindowLong(DWindow,GWL_STYLE,0);
    SetWindowLong(DWindow,GWL_STYLE,WS_CHILD);
    SetWindowPos(DWindow, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(DWindow,SW_SHOW);

    LoadLibrary("Riched20.dll");
    OutEdit = CreateWindowEx(WS_EX_CLIENTEDGE,RICHEDIT_CLASS,"",ES_MULTILINE | WS_CHILD | WS_TABSTOP | ES_NOHIDESEL | ES_AUTOVSCROLL |WS_VSCROLL|ES_WANTRETURN,
    15 hh,100 vv,960 hh,650 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

    SendMessage(OutEdit,WM_SETFONT,(WPARAM)hf2,true);
    long eventmask=SendMessage(OutEdit,EM_GETEVENTMASK,0,0);
    eventmask=(ENM_SELCHANGE | ENM_LINK);
    SendMessage(OutEdit,EM_SETEVENTMASK,0,eventmask);
    SendMessage(OutEdit, EM_AUTOURLDETECT, true, 0);
    SendMessage(OutEdit, EM_SETLIMITTEXT, 10000000, 0);
    dproc=SetWindowLong(OutEdit,GWL_WNDPROC,(long)WindowProcedure3);
    ShowWindow(OutEdit,SW_SHOW);

    SysMessage = CreateWindowEx(WS_EX_CLIENTEDGE,RICHEDIT_CLASS,"",WS_CHILD | ES_READONLY | SS_CENTERIMAGE,15 hh,765 vv,960 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(SysMessage,WM_SETFONT,(WPARAM)arial14,true);
    SendMessage(SysMessage,EM_SETMARGINS,EC_LEFTMARGIN,10 hh);
    SetWindowLong(SysMessage, GWL_EXSTYLE, GetWindowLong(SysMessage,GWL_EXSTYLE)&~WS_EX_CLIENTEDGE );
    SetWindowPos(SysMessage, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    SendMessage(SysMessage,EM_SETBKGNDCOLOR,0,RGB(50,50,50));
    dproc=SetWindowLong(SysMessage,GWL_WNDPROC,(long)EditorStaticProc);
    syme();
    ShowWindow(SysMessage,SW_SHOW);

    CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE | SS_SUNKEN |SS_CENTERIMAGE,418 hh,66 vv,145 hh,34 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    FontButton = CreateWindow("BUTTON","F",WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,530 hh,68 vv,30 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(FontButton,WM_SETFONT,(WPARAM)hff,true);
    BoldButton = CreateWindow("BUTTON","B",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE,420 hh,68 vv,30 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(BoldButton,WM_SETFONT,(WPARAM)hfb,true);
    ItalicButton = CreateWindow("BUTTON","I",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE,451 hh,68 vv,30 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ItalicButton,WM_SETFONT,(WPARAM)hfi,true);
    UlineButton = CreateWindow("BUTTON","U",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX | BS_PUSHLIKE,482 hh,68 vv,30 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(UlineButton,WM_SETFONT,(WPARAM)hfu,true);

    QuitButton = CreateWindow("BUTTON","Quit",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,1230 hh,15 vv,100 hh,40 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(QuitButton,WM_SETFONT,(WPARAM)hf,true);

    NewTextButton = CreateWindow("BUTTON","New Message",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,15 hh,805 vv,150 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(NewTextButton,WM_SETFONT,(WPARAM)hf,true);

    ClearPassword = CreateWindow("BUTTON","Clear Passphrase",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,15 hh,840 vv,155 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ClearPassword,WM_SETFONT,(WPARAM)hf,true);

    OutSectiontitle = CreateWindow("STATIC","  CenoCipher 4.1",WS_CHILD | WS_VISIBLE | SS_SUNKEN |SS_CENTERIMAGE,0 hh,15 vv,200 hh,35 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OutSectiontitle,WM_SETFONT,(WPARAM)hfal,true);

    int hoff=0;
    int voff=-35;
    int labelnum=3;
    labels[labelnum].h = CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE ,15 hh,70 vv,400 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(labels[labelnum].h,WM_SETFONT,(WPARAM)arial14,true);
    safescopy(labels[labelnum].help,"Step 3. Enter outgoing message text");
    safescopy(labels[labelnum].nohelp,"Message");

    labelnum=4;
    labels[labelnum].h = CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE,1013 hh,70 vv,270 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(labels[labelnum].h,WM_SETFONT,(WPARAM)arial14,true);
    safescopy(labels[labelnum].help,"Step 4. Attach files if desired");
    safescopy(labels[labelnum].nohelp,"Attached Files");

    labelnum=5;
    labels[labelnum].h = CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE,(375-hoff) hh,(770-voff) vv,170 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(labels[labelnum].h,WM_SETFONT,(WPARAM)arial14,true);
    safescopy(labels[labelnum].help,"Step 5. Type a passphrase");
    safescopy(labels[labelnum].nohelp,"Passphrase");
    safescopy(labels[labelnum].ihelp,"Step 2. Enter passphrase");

    CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE | SS_BLACKRECT,0 hh,(330+voff) vv,400 hh,0 ,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE | SS_BLACKRECT,994 hh,100 vv,3,735 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE | SS_BLACKRECT,1013 hh,765 vv,300 hh,30 vv ,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

    OptsHolder = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,(5+hoff) hh,(149+voff) vv,340 hh,155 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OptsHolder,GWL_WNDPROC,(long)WindowProcedureC);
    SetWindowLong(OptsHolder,GWL_STYLE,0);
    SetWindowLong(OptsHolder,GWL_STYLE,WS_CHILD|WS_BORDER);
    SetWindowPos(OptsHolder, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(OptsHolder,SW_SHOW);

    RandExtOpt = CreateWindow("BUTTON","Random name + extension:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP,5 hh,25 vv,235 hh,30 vv,OptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(RandExtOpt,WM_SETFONT,(WPARAM)hf,true);

    HWND RandDot = CreateWindow("STATIC",".",WS_CHILD | WS_VISIBLE,240 hh,27 vv,10 hh,25 vv,OptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(RandDot,WM_SETFONT,(WPARAM)arial14b,true);

    ExtName=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","d",WS_CHILD | WS_VISIBLE|WS_TABSTOP,248 hh,24 vv,65 hh,25 vv,OptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ExtName,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(ExtName,WM_SETTEXT,0,(LPARAM)"dat");

    CustNameOpt = CreateWindow("BUTTON","Custom name:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,5 hh,80 vv,135 hh,30 vv,OptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(CustNameOpt,WM_SETFONT,(WPARAM)hf,true);

    CustName=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","d",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_AUTOHSCROLL,150 hh,80 vv,165 hh,25 vv,OptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(CustName,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(CustName,WM_SETTEXT,0,(LPARAM)"example.txt");

    SendMessage(RandExtOpt,BM_SETCHECK,BST_CHECKED,0);

    OptsHolder2 = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,(5+hoff) hh,(149+voff) vv,340 hh,155 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OptsHolder2,GWL_WNDPROC,(long)WindowProcedureC);
    SetWindowLong(OptsHolder2,GWL_STYLE,0);
    SetWindowLong(OptsHolder2,GWL_STYLE,WS_CHILD|WS_BORDER);
    SetWindowPos(OptsHolder2, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(OptsHolder2,SW_HIDE);

    SpecificJpegOpt = CreateWindow("BUTTON","Specific Source Jpeg:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP,5 hh,13 vv,185 hh,30 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(SpecificJpegOpt,WM_SETFONT,(WPARAM)hf,true);

    SpecificJpegName=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_READONLY,5 hh,45 vv,310 hh,25 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(SpecificJpegName,WM_SETFONT,(WPARAM)hf3,true);

    SendMessage(SpecificJpegName,EM_SETBKGNDCOLOR,0,RGB(215,215,215));
    ChooseJpeg = CreateWindow("BUTTON","Choose",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,225 hh,13 vv,90 hh,25 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ChooseJpeg,WM_SETFONT,(WPARAM)hf,true);

    RandomJpegOpt = CreateWindow("BUTTON","Random from folder:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,5 hh,85 vv,180 hh,30 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(RandomJpegOpt,WM_SETFONT,(WPARAM)hf,true);

    RandomJpegName=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_READONLY,5 hh,117 vv,310 hh,25 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(RandomJpegName,WM_SETFONT,(WPARAM)hf3,true);
    ChooseJFolder = CreateWindow("BUTTON","Choose",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,225 hh,85 vv,90 hh,25 vv,OptsHolder2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ChooseJFolder,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(SpecificJpegOpt,BM_SETCHECK,BST_CHECKED,0);

    OptsHolder4 = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,(5+hoff) hh,(120+voff+29) vv ,340 hh,155 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OptsHolder4,GWL_WNDPROC,(long)WindowProcedureC);
    SetWindowLong(OptsHolder4,GWL_STYLE,0);
    SetWindowLong(OptsHolder4,GWL_STYLE,WS_CHILD|WS_BORDER);
    SetWindowPos(OptsHolder4, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(OptsHolder4,SW_HIDE);

    HWND TextLabel1 = CreateWindow("STATIC","Output will be a stream of Base64-encoded cipher-text",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 hh, 43 vv,305 hh,55 vv,OptsHolder4,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(TextLabel1,WM_SETFONT,(WPARAM)hf,true);

    WaitWindow = CreateWindowEx (0,szClassName,"Quick Help",WS_DLGFRAME,0 hh,0 vv,760 hh,500 vv,hwndmain,NULL,hti,NULL);
    SetWindowLong(WaitWindow, GWL_STYLE, GetWindowLong(WaitWindow,GWL_STYLE)&~WS_SYSMENU );
    SetWindowLong(WaitWindow,GWL_WNDPROC,(long)WindowProcedureH);

    RECT re;
    GetClientRect(WaitWindow,&re);
    WaitLabel = CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE,30 hh, 30 vv,730 hh,re.bottom-(130 vv),WaitWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(WaitLabel,WM_SETFONT,(WPARAM)hf,true);

    QuickOkay = CreateWindow("BUTTON","OK",WS_CHILD | WS_VISIBLE,350 hh,re.bottom-(80 vv),60 hh,30 vv,WaitWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(QuickOkay,WM_SETFONT,(WPARAM)hf,true);

    OutPass=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_AUTOHSCROLL,(370-hoff) hh,(800-voff) vv,250 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OutPass,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(OutPass,EM_SETLIMITTEXT,255,0);
    emproc=SetWindowLong(OutPass,GWL_WNDPROC,(long)EnterClickProc);

    EncryptButton = CreateWindow("BUTTON","",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON|BS_BITMAP,(370-hoff) hh,(833-voff) vv,250 hh,35 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncryptButton,WM_SETFONT,(WPARAM)hf,true);

    EncProg=CreateWindowEx(WS_EX_CLIENTEDGE,PROGRESS_CLASS,"",WS_CHILD | PBS_SMOOTH,(15-hoff) hh,(717-voff) vv,960 hh,10 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

    AddFilesButton = CreateWindow("BUTTON","Add Files",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,1013 hh,805 vv,150 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(AddFilesButton,WM_SETFONT,(WPARAM)hf,true);
    RemoveFilesButton = CreateWindow("BUTTON","Remove All",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,1163 hh,805 vv,150 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(RemoveFilesButton ,WM_SETFONT,(WPARAM)hf,true);

    INITCOMMONCONTROLSEX icex;
    icex.dwICC = ICC_LISTVIEW_CLASSES;
    InitCommonControlsEx(&icex);

    OutList = CreateWindowEx(WS_EX_CLIENTEDGE,WC_LISTVIEW,"",WS_TABSTOP | WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_NOCOLUMNHEADER|LVS_SHOWSELALWAYS,1013 hh,100 vv,300 hh,650 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    ListView_SetExtendedListViewStyle(OutList,LVS_EX_FULLROWSELECT);
    DragAcceptFiles(OutList,true);
    lvproc=SetWindowLong(OutList,GWL_WNDPROC,(long)OutListProc);
    SendMessage(OutList,WM_SETFONT,(WPARAM)hft,true);

    SHFILEINFO shpre={};
    HIMAGELIST lvhil=(HIMAGELIST)SHGetFileInfo((LPCSTR)".txt",FILE_ATTRIBUTE_NORMAL,&shpre,sizeof(SHFILEINFO),SHGFI_SYSICONINDEX|SHGFI_SMALLICON|SHGFI_USEFILEATTRIBUTES|SHGFI_ICON|SHGFI_TYPENAME|SHGFI_DISPLAYNAME);
    SendMessage(OutList,LVM_SETIMAGELIST,LVSIL_SMALL,(LPARAM)lvhil);
    LVCOLUMN lvc={};
    lvc.mask = LVCF_WIDTH|LVCF_SUBITEM;lvc.cx=285 hh;lvc.iSubItem=0;
    SendMessage(OutList,LVM_INSERTCOLUMN,0,(LPARAM)&lvc);
    lvc.cx=0 hh;lvc.iSubItem=1;
    SendMessage(OutList,LVM_INSERTCOLUMN,1,(LPARAM)&lvc);
    lvc.cx=0 hh;lvc.iSubItem=2;
    SendMessage(OutList,LVM_INSERTCOLUMN,1,(LPARAM)&lvc);
    lvc.cx=0 hh;lvc.iSubItem=3;
    SendMessage(OutList,LVM_INSERTCOLUMN,1,(LPARAM)&lvc);

    HWND HelpHolder = CreateWindowEx (WS_EX_CONTROLPARENT|WS_EX_CLIENTEDGE,szClassName,NULL,WS_CHILD,203 hh,15 vv,125 hh,35 vv,hwnd2,NULL,hThisInstance,NULL);
    SetWindowLong(HelpHolder,GWL_WNDPROC,(long)WindowProcedure2);
    SetWindowLong(HelpHolder,GWL_STYLE,0);
    SetWindowLong(HelpHolder,GWL_STYLE,WS_CHILD);
    SetWindowPos(HelpHolder, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(HelpHolder,SW_SHOW);

    MoreHelpButton = CreateWindow("BUTTON","Help",WS_TABSTOP | WS_CHILD | WS_VISIBLE|BS_PUSHBUTTON,60 hh,0 vv,60 hh,31 vv,HelpHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(MoreHelpButton,WM_SETFONT,(WPARAM)hf,true);

    QuickHelpButton = CreateWindow("BUTTON","Quick",WS_TABSTOP | WS_CHILD | WS_VISIBLE|BS_PUSHBUTTON,0 hh,0 vv,60 hh,31 vv,HelpHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(QuickHelpButton,WM_SETFONT,(WPARAM)hf,true);

    PMaskE = CreateWindow("BUTTON","Hide",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,(565-hoff) hh,(768-voff) vv,70 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(PMaskE,WM_SETFONT,(WPARAM)hf,true);

    CFileHolder = CreateWindowEx(WS_EX_CLIENTEDGE,WC_LISTVIEW,"",WS_TABSTOP | WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_NOCOLUMNHEADER | LVS_SHOWSELALWAYS,370 hh,925 vv,250 hh,50 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(CFileHolder,WM_SETFONT,(WPARAM)hfab,true);
    SendMessage(CFileHolder,LVM_SETIMAGELIST,LVSIL_SMALL,(LPARAM)lvhil);
    sureset(&lvc,0,sizeof(lvc));
    lvc.mask = LVCF_WIDTH|LVCF_SUBITEM;lvc.cx=235 hh;lvc.iSubItem=0;
    SendMessage(CFileHolder,LVM_INSERTCOLUMN,0,(LPARAM)&lvc);
    lvc.cx=0 hh;lvc.iSubItem=1;
    SendMessage(CFileHolder,LVM_INSERTCOLUMN,1,(LPARAM)&lvc);
    SetWindowLong(CFileHolder,GWL_WNDPROC,(long)OutListProc);
    ShowWindow(CFileHolder,SW_HIDE);


    ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	if (Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) != Gdiplus::Ok){return 0;}

    InSectiontitle = CreateWindow("STATIC"," Outgoing Communication Options",WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_LEFT|SS_CENTERIMAGE ,0 hh,0 vv,400 hh,35 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(InSectiontitle,WM_SETFONT,(WPARAM)hfal,true);

    RevokeDragDrop(OutEdit);
    CDropTarget * pdroptarget = new CDropTarget(OutEdit);
    RegisterDragDrop(OutEdit,pdroptarget);

    EncFormatOpt1 = CreateWindow("STATIC","Normal File",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,(5+hoff) hh,(120+voff) vv,120 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncFormatOpt1,WM_SETFONT,(WPARAM)hf,true);
    EncFormatOpt2 = CreateWindow("STATIC","Modified Jpeg",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,redge(EncFormatOpt1),(120+voff) vv,140 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncFormatOpt2,WM_SETFONT,(WPARAM)hf,true);
    EncFormatOpt3 = CreateWindow("STATIC","Text",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,redge(EncFormatOpt2),(120+voff) vv,redge(OptsHolder)-redge(EncFormatOpt2),30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncFormatOpt3,WM_SETFONT,(WPARAM)hf,true);
    CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE | WS_GROUP,0 hh,0 vv,0 hh,0 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    EncDestOpt1 = CreateWindow("STATIC","Screen",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,(5+hoff) hh,(375+voff) vv,120 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncDestOpt1,WM_SETFONT,(WPARAM)hf,true);
    EncDestOpt3 = CreateWindow("STATIC","Clipboard",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,redge(EncDestOpt1),(375+voff) vv,120 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncDestOpt3,WM_SETFONT,(WPARAM)hf,true);
    EncDestOpt2 = CreateWindow("STATIC","Folder",WS_CHILD | WS_VISIBLE | WS_BORDER| WS_GROUP |SS_CENTERIMAGE | SS_CENTER |SS_NOTIFY,redge(EncDestOpt3),(375+voff) vv,redge(OptsHolder2)-redge(EncDestOpt3),30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(EncDestOpt2,WM_SETFONT,(WPARAM)hf,true);


    CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE | WS_GROUP,0 hh,0 vv,0 hh,0 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

    InOptsHolder = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_CHILD,(0+hoff) hh,(520+voff) vv,400 hh,250 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(InOptsHolder,GWL_WNDPROC,(long)WindowProcedureI);
    SetWindowLong(InOptsHolder,GWL_STYLE,0);
    SetWindowLong(InOptsHolder,GWL_STYLE,WS_CHILD);
    SetWindowPos(InOptsHolder, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(InOptsHolder,SW_SHOW);

    InSectiontitle2 = CreateWindow("STATIC"," Incoming Communication Options",WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_LEFT|SS_CENTERIMAGE,0 hh,0 vv,400 hh,35 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(InSectiontitle2,WM_SETFONT,(WPARAM)hfal,true);

    labelnum=7;
    labels[labelnum].h = InSectiontitle2;
    safescopy(labels[labelnum].ihelp,"  Step 1. Load some cipher-data    ");
    safescopy(labels[labelnum].nohelp,"  Incoming Communication Options");


    OthOptsHolder = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_CHILD,(0+hoff) hh,(770+voff) vv,400 hh,310 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OthOptsHolder,GWL_WNDPROC,(long)WindowProcedureO);
    SetWindowLong(OthOptsHolder,GWL_STYLE,0);
    SetWindowLong(OthOptsHolder,GWL_STYLE,WS_CHILD);
    SetWindowPos(OthOptsHolder, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(OthOptsHolder,SW_SHOW);

    OthTitle1 = CreateWindow("STATIC"," Other Options",WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_LEFT|SS_CENTERIMAGE,0 hh,0 vv,400 hh,35 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OthTitle1,WM_SETFONT,(WPARAM)hfal,true);

    SaveSettings = CreateWindow("BUTTON","Save program settings",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 hh,50 vv,200 hh,30 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(SaveSettings,WM_SETFONT,(WPARAM)hf,true);

    OptPrompt = CreateWindow("BUTTON","Prompt",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP,10 hh,122 vv,220 hh,20 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OptPrompt,WM_SETFONT,(WPARAM)hf,true);

    OptUseDefault = CreateWindow("BUTTON","Use default location",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,10 hh,150 vv,220 hh,20 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OptUseDefault,WM_SETFONT,(WPARAM)hf,true);

    OptUseSpecified = CreateWindow("BUTTON","Use specified location",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,10 hh,179 vv,220 hh,20 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OptUseSpecified,WM_SETFONT,(WPARAM)hf,true);

    HWND unwriteopt=CreateWindow("STATIC","On decrypted disk-write attempt:",WS_CHILD | WS_VISIBLE | WS_GROUP,10 hh,97 vv,280 hh,20 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(unwriteopt,WM_SETFONT,(WPARAM)arial12u,true);

    SpecFolder=CreateWindowEx(WS_EX_CLIENTEDGE,RICHEDIT_CLASS,"",WS_CHILD | WS_VISIBLE|WS_TABSTOP,30 hh,200 vv,300 hh,25 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(SpecFolder,WM_SETFONT,(WPARAM)hf3,true);

    ChooseSpecFolder= CreateWindow("BUTTON","Choose",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,240 hh,177 vv,90 hh,23 vv,OthOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ChooseSpecFolder,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(OptPrompt,BM_SETCHECK,BST_CHECKED,0);


    labelnum=1;
    labels[labelnum].h = CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE | SS_LEFT,5 hh,55 vv,300 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(labels[labelnum].h,WM_SETFONT,(WPARAM)arial14,true);
    safescopy(labels[labelnum].help,"Step 1. Choose an output format");
    safescopy(labels[labelnum].nohelp,"Output Format");

    labelnum=2;
    labels[labelnum].h = CreateWindow("STATIC","label",WS_CHILD | WS_VISIBLE | SS_LEFT,5 hh,(345 + voff) vv,340 hh,30 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(labels[labelnum].h,WM_SETFONT,(WPARAM)arial14,true);
    safescopy(labels[labelnum].help,"Step 2. Choose an Output Destination");
    safescopy(labels[labelnum].nohelp,"Output Destination");

    OptsHolder3 = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD,(5+hoff) hh,(404+voff) vv,340 hh,90 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OptsHolder3,GWL_WNDPROC,(long)WindowProcedureC);
    SetWindowLong(OptsHolder3,GWL_STYLE,0);
    SetWindowLong(OptsHolder3,GWL_STYLE,WS_CHILD|WS_BORDER);
    SetWindowPos(OptsHolder3, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    ShowWindow(OptsHolder3,SW_HIDE);

    HWND FolderLabel2 = CreateWindow("STATIC","Folder Location:",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 hh, 17 vv,305 hh,25 vv,OptsHolder3,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(FolderLabel2,WM_SETFONT,(WPARAM)hf,true);

    FolderOutput=CreateWindowEx(WS_EX_CLIENTEDGE,RICHEDIT_CLASS,"",WS_CHILD | WS_VISIBLE|WS_TABSTOP,5 hh,45 vv,310 hh,25 vv,OptsHolder3,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(FolderOutput,WM_SETFONT,(WPARAM)hf3,true);

    ChooseFolderOutput = CreateWindow("BUTTON","Choose",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,225 hh,13 vv,90 hh,25 vv,OptsHolder3,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ChooseFolderOutput,WM_SETFONT,(WPARAM)hf,true);
    ShowWindow(OptsHolder3,SW_SHOW);

    OptsHolder5 = CreateWindowEx (WS_EX_CONTROLPARENT,szClassName,NULL,WS_DLGFRAME|WS_CHILD|WS_VISIBLE,(5+hoff) hh,(404+voff) vv,340 hh,90 vv,DWindow,NULL,hThisInstance,NULL);
    SetWindowLong(OptsHolder5,GWL_WNDPROC,(long)WindowProcedureC);
    SetWindowLong(OptsHolder5,GWL_STYLE,0);
    SetWindowLong(OptsHolder5,GWL_STYLE,WS_CHILD|WS_BORDER|WS_VISIBLE);
    SetWindowPos(OptsHolder5, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);

    TextLabel2 = CreateWindow("STATIC","",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 hh, 15 vv,320 hh,55 vv,OptsHolder5,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(TextLabel2,WM_SETFONT,(WPARAM)hf,true);

    HWND ddlabel1=CreateWindow("STATIC","Drag-drop file or text ",WS_CHILD | WS_VISIBLE | WS_BORDER| SS_CENTERIMAGE|SS_CENTER,5 hh,50 vv,340 hh,35 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ddlabel1,WM_SETFONT,(WPARAM)arial14,true);

    HWND ddlabel2=CreateWindow("STATIC","\xe0 \xe0",WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE|SS_CENTER,270 hh,57 vv,60 hh,24 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ddlabel2,WM_SETFONT,(WPARAM)hfwd,true);

    DecryptButton1 = CreateWindow("BUTTON","Load file from folder",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,5 hh,130 vv,340 hh,35 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(DecryptButton1,WM_SETFONT,(WPARAM)hf,true);

    DecryptButton2 = CreateWindow("BUTTON","Load clipboard contents",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,5 hh,90 vv,340 hh,35 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(DecryptButton2,WM_SETFONT,(WPARAM)hf,true);

    ShowPrompt();

    ReplyButton = CreateWindow("BUTTON","Reply",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,825 hh,805 vv,150 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(ReplyButton,WM_SETFONT,(WPARAM)hf,true);

    WithQuote = CreateWindow("BUTTON","With Quote",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,825 hh,840 vv,110 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(WithQuote,WM_SETFONT,(WPARAM)hf,true);

    IncludeFiles = CreateWindow("BUTTON","Re-include files",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,825 hh,870 vv,140 hh,30 vv,hwnd2,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(IncludeFiles,WM_SETFONT,(WPARAM)hf,true);

    AutoDecrypt = CreateWindow("BUTTON","Auto-Decrypt on load",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,80 hh,190 vv,200 hh,30 vv,InOptsHolder,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(AutoDecrypt,WM_SETFONT,(WPARAM)hf,true);

    OutBlocker = CreateWindow("STATIC","label",WS_CHILD|SS_CENTER,0 hh,0 vv,400 hh,500 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    OutBlocker2 = CreateWindow("STATIC","",WS_CHILD | SS_CENTER,0 hh,500 vv,400 hh,800 vv,DWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    SendMessage(OutBlocker,WM_SETFONT,(WPARAM)webdings108,true);
    SendMessage(OutBlocker2,WM_SETFONT,(WPARAM)webdings108,true);
    SendMessage(OutBlocker,WM_SETTEXT,0,(LPARAM)"\xe1\0\n\xe1\n\xe1");
    SendMessage(OutBlocker2,WM_SETTEXT,0,(LPARAM)"\xe2");


    //Internally created bitmaps for buttons
    HDC ddc = GetDC(GetDesktopWindow());
    HDC memDC=CreateCompatibleDC(ddc);
    ebuttonpic0=CreateCompatibleBitmap(ddc,250 hh, 35 vv);
    HBITMAP oldbmp=(HBITMAP)SelectObject(memDC,ebuttonpic0);
    HBRUSH buttonbrush=CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
    SelectObject(memDC,buttonbrush);
    RECT trect={0,0,(long)(250 hh),(long)(32 vv)};
    SelectObject(memDC,arial14);
    SetBkColor(memDC,GetSysColor(COLOR_BTNFACE));
    SetTextColor(memDC,RGB(0,0,0));
    Rectangle(memDC,0,0,250 hh,35 vv);
    DrawText(memDC,"Encrypt",7,&trect,DT_CENTER|DT_SINGLELINE|DT_VCENTER);
    ebuttonpic1=CreateCompatibleBitmap(ddc,250 hh, 35 vv);
    SelectObject(memDC,ebuttonpic1);
    Rectangle(memDC,0,0,250 hh,35 vv);
    SetTextColor(memDC,RGB(0,0,255));
    SetBkColor(memDC,RGB(255,255,100));
    DrawText(memDC,"Step 6. Click-to-Encrypt ",25,&trect,DT_CENTER|DT_SINGLELINE|DT_VCENTER);
    ebuttonpic2=CreateCompatibleBitmap(ddc,250 hh, 35 vv);
    SelectObject(memDC,ebuttonpic2);
    Rectangle(memDC,0,0,250 hh,35 vv);
    SetBkColor(memDC,GetSysColor(COLOR_BTNFACE));
    SetTextColor(memDC,RGB(0,0,0));
    DrawText(memDC,"Cancel",6,&trect,DT_CENTER|DT_SINGLELINE|DT_VCENTER);
    dbuttonpic0=CreateCompatibleBitmap(ddc,250 hh, 35 vv);
    SelectObject(memDC,dbuttonpic0);
    Rectangle(memDC,0,0,250 hh,35 vv);
    SetTextColor(memDC,RGB(0,0,0));
    DrawText(memDC,"Decrypt",7,&trect,DT_CENTER|DT_SINGLELINE|DT_VCENTER);
    dbuttonpic1=CreateCompatibleBitmap(ddc,250 hh, 35 vv);
    SelectObject(memDC,dbuttonpic1);
    Rectangle(memDC,0,0,250 hh,35 vv);
    SetTextColor(memDC,RGB(255,255,50));
    SetBkColor(memDC,RGB(255,0,255));
    DrawText(memDC,"Step 3. Click-to-Decrypt ",25,&trect,DT_CENTER|DT_SINGLELINE|DT_VCENTER);
    SelectObject(memDC,oldbmp);

    DeleteDC(memDC);
    DeleteObject(oldbmp);
    DeleteObject(buttonbrush);



    insmode=-1;
    InsToggle();

    CheckPrefs();
    insmode=0;
    InsToggle();

    SetFocus(OutEdit);
    hpos=0;vpos=0;

    HANDLE hIcon=LoadIcon(GetModuleHandle(NULL),"MY_ICON");
    SendMessage(hwndmain, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
    SendMessage(hwndmain, WM_SETICON, ICON_BIG, (LPARAM)hIcon);

    Sleep(500);
    ShowWindow(hwnd,SW_MAXIMIZE);
    ShowScrollBar(hwnd,SB_BOTH,true);
    GetClientRect(hwnd,&viewrect);
    ShowScrollBar(hwnd,SB_BOTH,false);

    if (strlen(lpszArgument)>0)
    {
        strrchr(lpszArgument,'"')[0]='\0';
        safescopy(DecDetails.cfile,strchr(lpszArgument,'"')+1);
        DecDetails.cformat=1;commode=1;
        ContAck(1,2,strrchr(DecDetails.cfile,'\\')+1);
        InsToggle();
    }


    //Process window messages in loop
    while (GetMessage (&messages, NULL, 0, 0))
    {
        if(!IsDialogMessage(hwnd, &messages))
        {
         TranslateMessage(&messages);
         DispatchMessage(&messages);
        }
    }


    //Program over, delete objects
    DeleteObject(greenbrush);
    DeleteObject(sagebrush);
    DeleteObject(beigebrush);
    DeleteObject(greenpen);
    DeleteObject(sagepen);
    DeleteObject(greypen);
    DeleteObject(greybrush);
    DeleteObject(othebrush[0][0]);
    DeleteObject(othebrush[1][0]);
    DeleteObject(othebrush[0][1]);
    DeleteObject(outbrush[0][0]);
    DeleteObject(outbrush[1][0]);
    DeleteObject(outbrush[2][0]);
    DeleteObject(outbrush[3][0]);
    DeleteObject(outbrush[3][2]);
    DeleteObject(inbrush[0][0]);
    DeleteObject(inbrush[1][0]);
    DeleteObject(inbrush[0][1]);
    DeleteObject(hf);
    DeleteObject(hf2);
    DeleteObject(hf3);
    DeleteObject(hft);
    DeleteObject(hfab);
    DeleteObject(hfb);
    DeleteObject(hfi);
    DeleteObject(hfu);
    DeleteObject(hfsu);
    DeleteObject(hff);
    DeleteObject(hfal);
    DeleteObject(hfwd);
    DeleteObject(arial14);
    DeleteObject(arial14b);
    DeleteObject(arial12u);
    DeleteObject(webdings108);
    DeleteObject(arial100);
    DeleteObject(ebuttonpic0);
    DeleteObject(ebuttonpic1);
    DeleteObject(ebuttonpic2);
    DeleteObject(dbuttonpic0);
    DeleteObject(dbuttonpic1);
    DeleteObject(helpimage1);
    DeleteObject(helpimage2);

    if (DecDetails.csp){DecDetails.csp->Release();DecDetails.csp=NULL;}
    if (DecDetails.cstgmed){ReleaseStgMedium(DecDetails.cstgmed);}

    if (DecDetails.dbuffer)
        {
            sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
            VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
            SetProcessWorkingSetSize(hProcess,defmin,defmax);
            delete[] DecDetails.dbuffer;
            DecDetails.dbuffer=NULL;
        }

    sureset(&DecDetails,0,sizeof(DecDetails));
    sureset(labels,0,sizeof(labels));
    sureset(SaveFilePath,0,sizeof(SaveFilePath));
    VirtualUnlock(&labels,sizeof(labels));
    VirtualUnlock(&DecDetails,sizeof(DecDetails));
    VirtualUnlock(SaveFilePath,sizeof(SaveFilePath));
    GdiplusShutdown(gdiplusToken);
    return messages.wParam;
}


//Main window callback
LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static HWND lastcontrol;
    switch (message)
    {

        case WM_COPYDATA:
            {
                COPYDATASTRUCT * cds=(COPYDATASTRUCT*)lParam;
                printf("Cdata: %s \n",(char*)cds->lpData);
                sureset(DecDetails.cfile,0,sizeof(DecDetails.cfile));
                strncpy(DecDetails.cfile,(char*)cds->lpData,cds->cbData);
                DecDetails.cformat=1;commode=1;
                ContAck(1,2,DecDetails.cfile);
                InsToggle();
                SetForegroundWindow(hwndmain);
                printf("Cfile: %s \n",DecDetails.cfile);
                return true;
            }


        case WM_SYSCOMMAND:
            if (wParam==SC_CLOSE)
            {SavePrefs();}
            break;


        case WM_ACTIVATE:
            if (LOWORD(wParam)==0)
            {
                lastcontrol=GetFocus();
                return 0;
            }
            else
            {
                SetFocus(lastcontrol);
                return 0;
            }
            break;


        case WM_CREATE:
            break;

        case WM_SIZE:

            SizeWindow(hwnd,message,wParam,lParam);
            break;

        case WM_DESTROY:
            PostQuitMessage (0);
            break;

        case WM_COMMAND:

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==0)
            {return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((QuitButton==(HWND)lParam)))
            {SavePrefs();PostQuitMessage (0);return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((FontButton==(HWND)lParam))and commode==0)
            {FontBox(hwnd);return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((BoldButton==(HWND)lParam)) and commode==0)
            {BoldToggle();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((ItalicButton==(HWND)lParam))and commode==0)
            {ItalicToggle();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((UlineButton==(HWND)lParam))and commode==0)
            {UlineToggle();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((NewTextButton==(HWND)lParam)))
            {NewMessage();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==AddFilesButton)
            {
                if (commode==0){FileRequestor();return 0;}
                else {OpenInListFiles();return 0;}
            }

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==RemoveFilesButton)
            {
                if (CheckStatus()){return 0;}
                if (commode==0)
                {
                    if (ListView_GetNextItem(OutList,-1,LVNI_SELECTED)==-1){ListView_DeleteAllItems(OutList);}
                    else while (ListView_DeleteItem(OutList,ListView_GetNextItem(OutList,-1,LVNI_SELECTED)));
                    return 0;
                }
                else
                {OpenInListFiles(1);}
            }

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==PMaskE)
            {PMaskEToggle();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==EncryptButton)
            {
                if (commode==0){CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)EncryptComm,NULL,0,NULL);}
                else {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)DecryptComm,NULL,0,NULL);SetFocus(OutPass);}
            }


            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==DecryptButton1)
            {FileLoader();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==DecryptButton2)
            {ClipLoader();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==DecryptButton)
            {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)DecryptComm,NULL,0,NULL);SetFocus(InPass);}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==OpenFilesButton)
            {OpenInListFiles();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==SaveFilesButton)
            {OpenInListFiles(1);}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==ReplyButton)
            {ReplyWithQuote();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==MoreHelpButton)
            {ShowHelp();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==QuickHelpButton)
            {QuickHelp();}

            if (HIWORD(wParam)==STN_CLICKED and ((HWND)lParam==EncFormatOpt1 or (HWND)lParam==EncFormatOpt2 or (HWND)lParam==EncFormatOpt3 or (HWND)lParam==EncDestOpt1 or (HWND)lParam==EncDestOpt2 or (HWND)lParam==EncDestOpt3))
            {
                if (CheckStatus()){return 0;}
                if ((HWND)lParam==EncFormatOpt1){formode=1;}
                if ((HWND)lParam==EncFormatOpt2){formode=2;}
                if ((HWND)lParam==EncFormatOpt3){formode=3;}
                if ((HWND)lParam==EncDestOpt1){desmode=1;}
                if ((HWND)lParam==EncDestOpt2){desmode=2;}
                if ((HWND)lParam==EncDestOpt3){desmode=3;}
                InsToggle();
            }

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==ChooseJpeg)
            {JpegRequestor();}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==ChooseJFolder)
            {FolderOutputRequestor(RandomJpegName);}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==ChooseFolderOutput)
            {FolderOutputRequestor(FolderOutput);}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==ChooseSpecFolder)
            {FolderOutputRequestor(SpecFolder);}


            break; //case WM_Command




        case WM_HSCROLL: case WM_VSCROLL:
        if (hwnd==hwndmain)
        {ScrollWindowFunc(hwnd,message,wParam,lParam);return 0;}
        return 0;


        case WM_NOTIFY:
        {
            NMHDR * nh = (NMHDR*) lParam;

            if ((nh->hwndFrom==OutEdit or nh->hwndFrom==HelpEdit1 or nh->hwndFrom==Linkbox) and (nh->code==EN_LINK))
            {
                LinkCatcher(hwnd,message,wParam,lParam);
                return 0;
            }

            if (nh->hwndFrom==OutEdit and nh->code==EN_SELCHANGE)
            {
              CHARFORMAT cfor;
              cfor.cbSize=sizeof(CHARFORMAT);
              SendMessage(OutEdit,EM_GETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
              cfor.dwEffects & CFE_BOLD ? SendMessage(BoldButton,BM_SETCHECK,BST_CHECKED,0) : SendMessage(BoldButton,BM_SETCHECK,BST_UNCHECKED,0);
              cfor.dwEffects & CFE_ITALIC ? SendMessage(ItalicButton,BM_SETCHECK,BST_CHECKED,0) : SendMessage(ItalicButton,BM_SETCHECK,BST_UNCHECKED,0);
              cfor.dwEffects & CFE_UNDERLINE ? SendMessage(UlineButton,BM_SETCHECK,BST_CHECKED,0) : SendMessage(UlineButton,BM_SETCHECK,BST_UNCHECKED,0);
            }

            if (nh->hwndFrom==OutList and (nh->code==(UINT)NM_CLICK or nh->code==(UINT)LVN_KEYDOWN or nh->code==(UINT)LVN_ITEMCHANGED))
            {InsToggle();}

            if (nh->hwndFrom==OutList and nh->code==LVN_ITEMACTIVATE)
            {
                if (commode==0)
                {
                    char tex[MAX_PATH];
                    int i=ListView_GetNextItem(OutList,-1,LVNI_SELECTED);
                    ListView_GetItemText(OutList,i,1,(LPTSTR)tex,MAX_PATH);
                    ShellExecute(NULL,"open",tex,"","",SW_SHOW);
                }
                else
                {OpenInListFiles(0);}

            }


            if (nh->hwndFrom==CFileHolder and nh->code==LVN_ITEMACTIVATE)
            {
                char tex[MAX_PATH];
                ListView_GetItemText(CFileHolder,0,1,(LPTSTR)tex,MAX_PATH);
                ShellExecute(NULL,"open",tex,"","",SW_SHOW);
            }



            if (nh->hwndFrom==CFileHolder and nh->code==LVN_BEGINDRAG)
                {
                    IDataObject *pDataObject;
                    IDropSource *pDropSource;
                    DWORD		 dwEffect;

                    FORMATETC fmtetc = { CF_HDROP, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
                    STGMEDIUM stgmed = { TYMED_HGLOBAL, { 0 }, 0 };

                    char tex[MAX_PATH*2+1]={};
                    VirtualLock(tex,sizeof(tex));
                    ListView_GetItemText(CFileHolder,0,1,tex,MAX_PATH);
                    HGLOBAL hMem = GlobalAlloc(GHND, sizeof(DROPFILES) + strlen(tex) + 2);
                    DROPFILES *dfiles = (DROPFILES*) GlobalLock(hMem);
                    dfiles->pFiles = sizeof(DROPFILES);
                    GetCursorPos(&(dfiles->pt));
                    dfiles->fNC = TRUE;
                    dfiles->fWide = FALSE;
                    memcpy(&dfiles[1], tex, strlen(tex));
                    GlobalUnlock(hMem);
                    stgmed.hGlobal=hMem;

                    CreateDropSource(&pDropSource);
                    CreateDataObject(&fmtetc, &stgmed, 1, &pDataObject);
                    DoDragDrop(pDataObject, pDropSource, DROPEFFECT_COPY, &dwEffect);
                    pDataObject->Release();
                    pDropSource->Release();
                    ReleaseStgMedium(&stgmed);
                    sureset(tex,0,sizeof(tex));
                    VirtualUnlock(tex,sizeof(tex));
                }



            break;
        }


        default:
            return DefWindowProc (hwnd, message, wParam, lParam);
    }


    return DefWindowProc (hwnd, message, wParam, lParam);

}

//Callback for top section container
LRESULT CALLBACK WindowProcedure2 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_CTLCOLORSTATIC:
            {
                SetBkColor((HDC)wParam,beigecolor);
                for (int x=3;x<=4;x++)
                {if ((HWND)lParam==labels[x].h){SetTextColor((HDC)wParam,maincolor[3][insmode]);SetBkColor((HDC)wParam,maincolor[1][insmode]);}}
                if ((HWND)lParam==labels[5].h){SetTextColor((HDC)wParam,maincolor[4][insmode]);SetBkColor((HDC)wParam,maincolor[2][insmode]);}
                return (LRESULT)beigebrush;
            }


        case WM_COMMAND: case WM_NOTIFY:

            CallWindowProc(WindowProcedure,hwnd,message,wParam,lParam);
            return 0;

    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

//Callback for side menu container
LRESULT CALLBACK WindowProcedureD (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_ERASEBKGND:

                RECT rect;
                SelectObject((HDC)wParam, greenpen);
                SelectObject((HDC)wParam, outbrush[1][insmode]);
                GetClientRect(hwnd, &rect);
                Rectangle((HDC)wParam, rect.left, rect.top, rect.right, rect.bottom);
                return 0;


        case WM_CTLCOLORSTATIC:
            {
                if ((HWND)lParam==InSectiontitle)
                {
                    SetBkColor((HDC)wParam,outcolor[0][insmode]);
                    return (LRESULT)outbrush[0][insmode];
                }

                if ((HWND)lParam==OutBlocker or (HWND)lParam==OutBlocker2)
                {
                    SetBkColor((HDC)wParam,greycolor);
                    SetTextColor((HDC)wParam,RGB(120,120,120));
                    return (LRESULT)greybrush;
                }


                if (((HWND)lParam==EncFormatOpt1 and formode==1) or ((HWND)lParam==EncFormatOpt2 and formode==2) or ((HWND)lParam==EncFormatOpt3 and formode==3) )
                {
                    SetBkColor((HDC)wParam,outcolor[0][insmode]);
                    return (LRESULT)outbrush[0][insmode];
                }

                if (((HWND)lParam==EncDestOpt1 and desmode==1) or ((HWND)lParam==EncDestOpt2 and desmode==2) or ((HWND)lParam==EncDestOpt3 and desmode==3) )
                {
                    SetBkColor((HDC)wParam,outcolor[0][insmode]);
                    return (LRESULT)outbrush[0][insmode];
                }

                SetBkColor((HDC)wParam,outcolor[1][insmode]);

                for (int x=1;x<15;x++)
                {
                    if ((HWND)lParam==labels[x].h){SetTextColor((HDC)wParam,outcolor[4][insmode]);SetBkColor((HDC)wParam,outcolor[2][insmode]);}
                }

                return (LRESULT)outbrush[1][insmode];
            }


        case WM_COMMAND: case WM_NOTIFY:

            CallWindowProc(WindowProcedure,hwnd,message,wParam,lParam);
            return 0;

    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}




LRESULT CALLBACK WindowProcedureI (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_ERASEBKGND:

                RECT rect;
                SelectObject((HDC)wParam, inpen);
                SelectObject((HDC)wParam, inbrush[1][insmode]);
                GetClientRect(hwnd, &rect);
                Rectangle((HDC)wParam, rect.left, rect.top, rect.right, rect.bottom);
                return 0;


        case WM_CTLCOLORSTATIC:
            {
                if ((HWND)lParam==InSectiontitle2)
                {
                    SetBkColor((HDC)wParam,incolor[0][insmode]);
                    SetTextColor((HDC)wParam,incolor[2][insmode]);
                    return (LRESULT)inbrush[0][insmode];
                }

                SetBkColor((HDC)wParam,incolor[1][insmode]);

                for (int x=7;x<8;x++)
                {if ((HWND)lParam==labels[x].h){SetTextColor((HDC)wParam,incolor[2][insmode]);}}
                return (LRESULT)inbrush[1][insmode];
            }


        case WM_COMMAND: case WM_NOTIFY:

            CallWindowProc(WindowProcedure,hwnd,message,wParam,lParam);
            return 0;

    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}


LRESULT CALLBACK WindowProcedureO(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_ERASEBKGND:

                RECT rect;
                SelectObject((HDC)wParam, othpen);
                SelectObject((HDC)wParam, othebrush[1][insmode]);
                GetClientRect(hwnd, &rect);
                Rectangle((HDC)wParam, rect.left, rect.top, rect.right, rect.bottom);
                return 0;


        case WM_CTLCOLORSTATIC:
            {
                if ((HWND)lParam==OthTitle1)
                {SetBkColor((HDC)wParam,othecolor[0][insmode]);return (LRESULT)othebrush[0][insmode];}
                SetBkColor((HDC)wParam,othecolor[1][insmode]);
                return (LRESULT)othebrush[1][insmode];
            }


        case WM_COMMAND: case WM_NOTIFY:

            CallWindowProc(WindowProcedure,hwnd,message,wParam,lParam);
            return 0;

    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}


LRESULT CALLBACK WindowProcedureC(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_ERASEBKGND:

                RECT rect;
                SelectObject((HDC)wParam, greypen);
                SelectObject((HDC)wParam, outbrush[3][insmode]);
                GetClientRect(hwnd, &rect);
                Rectangle((HDC)wParam, rect.left, rect.top, rect.right, rect.bottom);
                return 0;


        case WM_CTLCOLORSTATIC:
            {SetBkColor((HDC)wParam,outcolor[3][insmode]);return (LRESULT)outbrush[3][insmode];}

        case WM_COMMAND: case WM_NOTIFY:

            CallWindowProc(WindowProcedure,hwnd,message,wParam,lParam);
            return 0;

    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}





//Callback for message composition editor
LRESULT CALLBACK WindowProcedure3 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {

    case WM_SETFOCUS: case WM_ACTIVATE:
    if (commode==1 and SendMessage(OutEdit,WM_GETTEXTLENGTH,0,0)<7){return 0;}
    break;


    case WM_KEYDOWN:
    if (wParam==27){return 0;}
    if (commode!=0 and wParam==0x58 and GetAsyncKeyState(VK_CONTROL)& 0x8000){return 0;}
    if (commode!=0 and wParam==0x5A and GetAsyncKeyState(VK_CONTROL)& 0x8000){return 0;}
    if (commode!=0 and wParam==0x2E){return 0;}
    if (commode!=0 and wParam==0x08){return 0;}
    if (commode!=0 and wParam==0x56 and GetAsyncKeyState(VK_CONTROL)& 0x8000){return 0;}

    break;

    case WM_CHAR:
    if (wParam==9 and GetAsyncKeyState(VK_CONTROL)){return 0;}
    if (commode!=0){return 0;}
    break;

    case WM_KEYUP:
        {
          if (wParam==0x42 and (GetAsyncKeyState(VK_CONTROL) & 0x8000))
          {BoldToggle();return 0;}

          if (wParam==0x55 and (GetAsyncKeyState(VK_CONTROL) & 0x8000))
          {UlineToggle();return 0;}

          if (wParam==0x49 and (GetAsyncKeyState(VK_CONTROL) & 0x8000))
          {ItalicToggle();return 0;}

          break;
        }

    case WM_CONTEXTMENU:
        {
            HMENU menu=CreatePopupMenu();
            if(commode==0){InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION, 1, "Undo");}
            if(commode==0){InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION | MF_SEPARATOR, 0, "");}
            if(commode==0){InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION, 2, "Cut");}
            InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION, 3, "Copy");
            if(commode==0){InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION, 4, "Paste");}
            if(commode==0){InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION | MF_SEPARATOR, 0, "");}
            InsertMenu(menu, -1, MF_STRING | MF_BYPOSITION, 5, "Select All");
            POINT pt;
            GetCursorPos(&pt);
            int menuresult=TrackPopupMenu(menu, TPM_BOTTOMALIGN | TPM_BOTTOMALIGN | TPM_RETURNCMD, pt.x, pt.y + 130, 0, hwndmain, NULL);
            if (menuresult==1){SendMessage(hwnd,WM_UNDO,0,0);}
            if (menuresult==2){SendMessage(hwnd,WM_CUT,0,0);}
            if (menuresult==3){SendMessage(hwnd,WM_COPY,0,0);}
            if (menuresult==4){SendMessage(hwnd,WM_PASTE,0,0);}
            if (menuresult==5){SendMessage(hwnd,EM_SETSEL,0,-1);}

            DestroyMenu(menu);
            return 0;
            break;

        }



    default:
        return CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);
    }

        return CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);

}

//Callback for Help window
LRESULT CALLBACK WindowProcedureH (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_SYSCOMMAND:
        if (wParam==SC_CLOSE)
        {ShowWindow(hwnd,SW_HIDE);return 0;}
        break;


    case WM_CTLCOLORSTATIC:
    {
        SetBkColor((HDC)wParam,RGB(233,236,216));
        return (LRESULT)beigebrush;
    }


    case WM_NOTIFY:
            {
                NMHDR * nh = (NMHDR*) lParam;

                if ((nh->hwndFrom==HelpEdit1) and (nh->code==EN_LINK))
                {
                    LinkCatcher(hwnd,message,wParam,lParam);
                    return 0;
                }

            }

    case WM_COMMAND:


            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==QuickOkay)
            {
                if (insmode==1)
                {
                    insmode=2;InsToggle();
                    SendMessage(WaitLabel,WM_SETTEXT,0,(LPARAM)"TO PROCESS AN INCOMING COMMUNICATION: \n(See onscreen highlighted guide)\n\n\nStep 1. Load incoming cipher-data by any preferred method                                 \n\nStep 2. Enter passphrase into box \n\nStep 3. Click button (or hit Enter) to decrypt \n\n\nSee extended Help screen for more detailed instructions.\n\nClick OK below to close quick help.");
                }
                else
                {
                    SetForegroundWindow(hwndmain);
                    EnableWindow(hwndmain,true);
                    ShowWindow(WaitWindow,SW_HIDE);
                    insmode=0;InsToggle();
                }
                return 0;
            }


            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==PromptChooseSpecFolder)
            {FolderOutputRequestor(PromptSpecFolder);}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==PromptOkay)
            {PromptAction(1);return 0;}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==PromptCancel)
            {PromptAction(0);return 0;}

            break;


    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return DefWindowProc (hwnd, message, wParam, lParam);

}



//Callback for password entry fields
LRESULT CALLBACK EnterClickProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {

        case WM_KEYUP:
            if (wParam==VK_RETURN and hwnd==OutPass and commode==0)
            {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)EncryptComm,NULL,0,NULL);}
            if (wParam==VK_RETURN and hwnd==OutPass and commode!=0)
            {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)DecryptComm,NULL,0,NULL);}
            break;

        default:

            return CallWindowProc((WNDPROC)emproc,hwnd,message,wParam,lParam);

    }

        return CallWindowProc((WNDPROC)emproc,hwnd,message,wParam,lParam);

}




//Callback for outgoing file list
LRESULT CALLBACK OutListProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (hwnd==CFileHolder and message==WM_KILLFOCUS){return 0;}
    if (hwnd==CFileHolder and message!=WM_KILLFOCUS){return CallWindowProc((WNDPROC)lvproc,hwnd,message,wParam,lParam);}

    switch (message)
    {

    case WM_KILLFOCUS:
        {
            return 0;
        }
        break;


    case WM_DROPFILES:
    {

        if(CheckStatus()){return 0;};

        if (commode!=0){syme(7,"Swtich to outgoing message (New or Reply) before adding outgoing files.");return 0;}

        char flist[MAX_PATH]={};
        VirtualLock(flist,MAX_PATH);

        int numfiles=DragQueryFile((HDROP)wParam,0xFFFFFFFF,(LPSTR)flist,MAX_PATH);

        for (int x=0;x<numfiles;x++)
        {
            DragQueryFile((HDROP)wParam,x,(LPSTR)flist,MAX_PATH);
            char * fname=strrchr(flist,'\\')+1;
            LVITEM lvi={};
            lvi.iItem=ListView_GetItemCount(OutList)+2;
            int ni=ListView_InsertItem(OutList,&lvi);
            ListView_SetItemText(OutList,ni,0,(LPSTR)fname);
            ListView_SetItemText(OutList,ni,1,(LPSTR)flist);
        }
        DragFinish((HDROP)wParam);
        Addicons(OutList);
        sureset(flist,0,sizeof(flist));
        VirtualUnlock(flist,sizeof(flist));

        break;
    }

    case WM_KEYUP:
        if (wParam==VK_DELETE and commode==0)
        {
            if (CheckStatus()){return 0;}
            while (ListView_DeleteItem(OutList,ListView_GetNextItem(OutList,-1,LVNI_SELECTED)));
        }
        break;



    default:

        return CallWindowProc((WNDPROC)lvproc,hwnd,message,wParam,lParam);
    }

        return CallWindowProc((WNDPROC)lvproc,hwnd,message,wParam,lParam);

}



//Function to produce a quick MessageBox popup
VOID Mbox(string a)
{MessageBox(hwndmain,(LPCSTR)a.c_str(),"",MB_OK|MB_APPLMODAL);}

//Function to quickly convert an integer to a string for display
string Str(int i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

//Function to quickly convert a long long to a string for display
string QuadToString(long long q)
{
    stringstream ss;
    ss << q;
    return ss.str();
}


//Function to call a font requester box for text formatting
VOID FontBox(HWND hwnd)
{
    if (commode!=0){return;}
    CHARFORMAT cfor={};
    cfor.cbSize=sizeof(CHARFORMAT);
    SendMessage(OutEdit,EM_GETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    CHOOSEFONT cf={};
    LOGFONT lf={};

    safescopy(lf.lfFaceName,cfor.szFaceName);
    if (cfor.dwEffects & CFE_ITALIC){lf.lfItalic=true;}
    cfor.dwEffects & CFE_BOLD ? lf.lfWeight=FW_BOLD : lf.lfWeight=FW_NORMAL ;
    if (cfor.dwEffects & CFE_UNDERLINE){lf.lfUnderline=true;}
    if (cfor.dwEffects & CFE_STRIKEOUT){lf.lfStrikeOut=true;}
    cf.rgbColors=cfor.crTextColor;

    int PointSize=(((double)cfor.yHeight/20)+0.5);
    HDC hdc=GetDC(hwnd);
    lf.lfHeight = -MulDiv(PointSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    ReleaseDC(hwnd,hdc);
    cf.lStructSize = sizeof (cf);
    cf.hwndOwner = hwnd;
    cf.lpLogFont = &lf;
    cf.Flags = CF_SCREENFONTS | CF_EFFECTS|CF_INITTOLOGFONTSTRUCT;

    if (ChooseFont(&cf))
    {
        safescopy(cfor.szFaceName,lf.lfFaceName);
        cfor.yHeight=cf.iPointSize*2;
        cfor.crTextColor=cf.rgbColors;
        cfor.dwMask=CFM_FACE|CFM_SIZE|CFM_BOLD|CFM_COLOR|CFM_ITALIC|CFM_UNDERLINE|CFM_STRIKEOUT;
        cfor.dwEffects=0;
        if (lf.lfItalic){cfor.dwEffects |= CFE_ITALIC;}
        if (lf.lfStrikeOut){cfor.dwEffects |= CFE_STRIKEOUT;}
        if (lf.lfUnderline){cfor.dwEffects |= CFE_UNDERLINE;}
        if (lf.lfWeight>400){cfor.dwEffects |= CFE_BOLD;}
        SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);

    }
    SetFocus(OutEdit);

}


//Function to process and handle clicks on URL links within edit controls
void LinkCatcher(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    NMHDR * nh = (NMHDR*) lParam;
    if ((nh->hwndFrom==OutEdit or nh->hwndFrom==HelpEdit1 or nh->hwndFrom==Linkbox) and (nh->code==EN_LINK))
    {
        ENLINK * enl=(ENLINK*)lParam;
          if (enl->msg==WM_LBUTTONDOWN)
            {
                long epos1, epos2;
                SendMessage(nh->hwndFrom,EM_GETSEL,(WPARAM)&epos1,(LPARAM)&epos2);
                SendMessage(nh->hwndFrom,EM_SETSEL,enl->chrg.cpMin,enl->chrg.cpMax);
                char * gst = new char[1024];
                VirtualLock(gst,1024);
                SendMessage(nh->hwndFrom,EM_GETSELTEXT,0,(LPARAM)gst);
                SendMessage(nh->hwndFrom,EM_SETSEL,(WPARAM)&epos1,(LPARAM)&epos2);
                ShellExecute(NULL,"open",gst,"","",SW_SHOW);
                sureset(gst,0,1024);
                VirtualUnlock(gst,1024);
                delete[] gst;
                return;
            }
    }

}


//Function to toggle bold text in message editor
void BoldToggle()
{
    if (commode!=0){return;}
    CHARFORMAT cfor;
    cfor.cbSize=sizeof(CHARFORMAT);
    SendMessage(OutEdit,EM_GETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    cfor.dwMask=CFM_BOLD;
    cfor.dwEffects & CFE_BOLD ?  (cfor.dwEffects=0,SendMessage(BoldButton,BM_SETCHECK,BST_UNCHECKED,0)) : (cfor.dwEffects=CFE_BOLD,SendMessage(BoldButton,BM_SETCHECK,BST_CHECKED,0));
    SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    SetFocus(OutEdit);
}

//Function to toggle italic text in message editor
void ItalicToggle()
{
    if (commode!=0){return;}
    CHARFORMAT cfor;
    cfor.cbSize=sizeof(CHARFORMAT);
    SendMessage(OutEdit,EM_GETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    cfor.dwMask=CFM_ITALIC;
    cfor.dwEffects & CFE_ITALIC ?  (cfor.dwEffects=0,SendMessage(ItalicButton,BM_SETCHECK,BST_UNCHECKED,0)) : (cfor.dwEffects=CFE_ITALIC,SendMessage(ItalicButton,BM_SETCHECK,BST_CHECKED,0));
    SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    SetFocus(OutEdit);
}

//Function to toggle underline text in message editor
void UlineToggle()
{
    if (commode!=0){return;}
    CHARFORMAT cfor;
    cfor.cbSize=sizeof(CHARFORMAT);
    SendMessage(OutEdit,EM_GETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    cfor.dwMask=CFM_UNDERLINE;
    cfor.dwEffects & CFE_UNDERLINE ?  (cfor.dwEffects=0,SendMessage(UlineButton,BM_SETCHECK,BST_UNCHECKED,0)) : (cfor.dwEffects=CFE_UNDERLINE,SendMessage(UlineButton,BM_SETCHECK,BST_CHECKED,0));
    SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
    SetFocus(OutEdit);
}

//Function to handle resizing of main program window
void SizeWindow(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static int lastwidth;
    static int lastheight;

    SCROLLINFO si;
    si.cbSize=sizeof(SCROLLINFO);
    si.fMask=SIF_ALL;
    LPSCROLLINFO lpsi=&si;
    LPCSCROLLINFO lpcsi=&si;

    GetScrollInfo(hwnd,SB_HORZ,lpsi);
    si.nMin=1;
    si.nMax=(maxrect.right-maxrect.left);
    si.nPage=LOWORD(lParam);
    SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
    GetScrollInfo(hwnd,SB_HORZ,lpsi);

    RECT newrect;
    GetClientRect(hwnd,&newrect);
    int newwidth=(newrect.right-newrect.left);

    if (newwidth>lastwidth and lastwidth> 0 and (si.nPos>=si.nMax-(int)(si.nPage-1)))
    {
        int samount=newwidth-lastwidth;
        int maxhpos=((maxrect.right-maxrect.left)-newwidth)*-1;
        if ((int)(samount+hpos)>maxhpos){samount=maxhpos-hpos;}
        if (hpos+samount>0){samount=0-hpos;}
        ScrollWindowEx(hwnd,+samount,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
        hpos+=samount;
    }

    lastwidth=newwidth;

    GetScrollInfo(hwnd,SB_VERT,lpsi);
    si.nMin=1;
    si.nMax=(maxrect.bottom-maxrect.top);
    si.nPage=HIWORD(lParam);

    GetClientRect(hwnd,&newrect);
    int newheight=(newrect.bottom-newrect.top);
    si.nPage=newrect.bottom-newrect.top;
    SetScrollInfo(hwnd,SB_VERT,lpcsi,true);
    GetScrollInfo(hwnd,SB_VERT,lpsi);


    if (newheight>lastheight and lastheight>0 and (si.nPos>=si.nMax-(int)(si.nPage-1)))
    {
        int samount=newheight-lastheight;
        int maxvpos=((maxrect.bottom-maxrect.top)-newheight)*-1;
        if (samount+vpos>maxvpos){samount=maxvpos-vpos;}
        if (vpos+samount>0){samount=0-vpos;}
        ScrollWindowEx(hwnd,0,+samount,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
        vpos+=samount;
    }


    lastheight=newheight;

    if (newheight>=(maxrect.bottom-maxrect.top-(maxrect.bottom-viewrect.bottom)) and newwidth>=(maxrect.right-maxrect.left-(maxrect.right-viewrect.right)))
    {
        ScrollWindowEx(hwnd,-hpos,-vpos,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
        hpos=0;vpos=0;
        ShowScrollBar(hwnd,SB_BOTH,false);

    }

    if (wParam==SIZE_MAXIMIZED)
    {
        ShowScrollBar(hwnd,SB_BOTH,false);
    }


    if (initwait==1){hpos=0;vpos=0;ShowScrollBar(hwnd,SB_BOTH,false);}

    return;

}

//Function to handle scrolling of main program window
int ScrollWindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {

        case WM_HSCROLL:
           {

            SCROLLINFO si;
            si.cbSize=sizeof(SCROLLINFO);
            si.fMask=SIF_ALL;
            LPSCROLLINFO lpsi=&si;
            GetScrollInfo(hwnd,SB_HORZ,lpsi);

            if (LOWORD(wParam)==SB_LINERIGHT)
            {
                if (si.nPos==(si.nMax-(int)(si.nPage-1))) {break;}
                si.nPos+=1;
                hpos-=1;
                SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
                ScrollWindowEx(hwnd,-1,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                return 0;
            }

            if (LOWORD(wParam)==SB_LINELEFT)
            {
                if (si.nPos==si.nMin) {break;}
                si.nPos-=1;
                hpos+=1;
                SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
                ScrollWindowEx(hwnd,+1,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                return 0;
            }

            if (LOWORD(wParam)==SB_PAGERIGHT)
            {
                int samount=si.nPage;
                if (samount>si.nMax-si.nPos-(int)si.nPage+1){samount=si.nMax-si.nPos-si.nPage+1;}
                si.nPos+=si.nPage;
                hpos-=samount;
                SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
                ScrollWindowEx(hwnd,-1*samount,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                return 0;
            }

            if (LOWORD(wParam)==SB_PAGELEFT)
            {
                int samount=si.nPage;
                if (samount>si.nPos-si.nMin){samount=si.nPos-si.nMin;}
                si.nPos-=si.nPage;
                hpos+=samount;
                SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
                ScrollWindowEx(hwnd,1*samount,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                return 0;

            }

            if (LOWORD(wParam)==SB_THUMBTRACK)
            {
                int samount=(HIWORD(wParam)-1)-hpos*-1;
                ScrollWindowEx(hwnd,-1*samount,0,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                hpos-=samount;
                return 0;
            }

            if (LOWORD(wParam)==SB_THUMBPOSITION)
            {
                si.nPos=HIWORD(wParam);
                SetScrollInfo(hwnd,SB_HORZ,lpsi,true);
                return 0;
            }


            break;
            } //case Hscroll



        case WM_VSCROLL:
            {
                SCROLLINFO si;
                si.cbSize=sizeof(SCROLLINFO);
                si.fMask=SIF_ALL;
                LPSCROLLINFO lpsi=&si;
                GetScrollInfo(hwnd,SB_VERT,lpsi);

                if (LOWORD(wParam)==SB_LINEDOWN)
                {
                    if (si.nPos==(si.nMax-(int)(si.nPage-1))) {break;}
                    si.nPos+=1;
                    vpos-=1;
                    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
                    ScrollWindowEx(hwnd,0,-1,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                    return 0;
                }

                if (LOWORD(wParam)==SB_LINEUP)
                {
                    if (si.nPos==si.nMin) {break;}
                    si.nPos-=1;
                    vpos+=1;
                    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
                    ScrollWindowEx(hwnd,0,+1,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                    return 0;
                }

                if (LOWORD(wParam)==SB_PAGEDOWN)
                {
                    int samount=si.nPage;
                    if (samount>si.nMax-si.nPos-(int)si.nPage+1){samount=si.nMax-si.nPos-si.nPage+1;}
                    si.nPos+=si.nPage;
                    vpos-=samount;
                    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
                    ScrollWindowEx(hwnd,0,-1*samount,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                    return 0;
                }

                if (LOWORD(wParam)==SB_PAGEUP)
                {
                    int samount=si.nPage;
                    if (samount>si.nPos-si.nMin){samount=si.nPos-si.nMin;}
                    si.nPos-=si.nPage;
                    vpos+=samount;
                    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
                    ScrollWindowEx(hwnd,0,1*samount,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                    return 0;

                }

                if (LOWORD(wParam)==SB_THUMBTRACK)
                {
                    int samount=(HIWORD(wParam)-1)-vpos*-1;
                    ScrollWindowEx(hwnd,0,-1*samount,NULL,NULL,NULL,NULL,SW_ERASE|SW_INVALIDATE|SW_SCROLLCHILDREN);
                    vpos-=samount;
                    return 0;

                }

                if (LOWORD(wParam)==SB_THUMBPOSITION)
                {
                    si.nPos=HIWORD(wParam);
                    SetScrollInfo(hwnd,SB_VERT,lpsi,true);
                    return 0;
                }

            break;
            } //case vscroll

    } // switch

    return 0;
}

//Function to stream rich-text content out from editor control
void OEditStreamOut()
{
 EDITSTREAM es;
 es.dwCookie=0;
 es.pfnCallback=EditStreamCallback;
 SendMessage(OutEdit,EM_STREAMOUT,SF_RTF,(LPARAM)&es);
}

//Partner function to above, to handle actual streaming out of text
DWORD CALLBACK EditStreamCallback(DWORD_PTR dwCookie,LPBYTE pbBuff,LONG cb,LONG *pcb)
{
    strncat(messbuff,(char*)pbBuff,cb);
    *pcb=cb;
    return 0;
}

//Function to determine number of bytes to be streamed out first
long long OEditStreamOutCheck()
{
 EDITSTREAM es;
 es.dwCookie=0;
 es.pfnCallback=EditStreamCallbackCheck;
 messbytes=0;
 SendMessage(OutEdit,EM_STREAMOUT,SF_RTF,(LPARAM)&es);
 return messbytes;
}

//Partner function above, to count actual bytes needed
DWORD CALLBACK EditStreamCallbackCheck(DWORD_PTR dwCookie,LPBYTE pbBuff,LONG cb,LONG *pcb)
{
    messbytes+=cb;
    sureset(pbBuff,0,cb);
    *pcb=cb;
    return 0;
}


//Function to call file requester dialog for attaching outgoing files
void FileRequestor()
{
    if (CheckStatus()){return;}

    OPENFILENAME ofn={};
    char szFile[65536]={};
    VirtualLock(&ofn,sizeof(ofn));
    VirtualLock(szFile,sizeof(szFile));

    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwndmain;
    ofn.lpstrFile = szFile;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST|OFN_ALLOWMULTISELECT|OFN_EXPLORER;


    if (GetOpenFileName(&ofn)==TRUE)
        {
            char* first=szFile;
            char* last=szFile+sizeof(szFile)-1;
            char folder[MAX_PATH]={};
            VirtualLock(folder,sizeof(folder));
            char fname[MAX_PATH]={};
            VirtualLock(fname,sizeof(fname));

            do
                {
                    char* f=find(first,last,'\0');
                    if (f==last){break;}
                    char* fsel=first;
                    first=f+1;
                    if (strchr(fsel,':') and *first!='\0')
                        {safescopy(folder,fsel);safescat(folder,"\\");}
                    else
                        {
                            safescopy(fname,folder);
                            safescat(fname,fsel);
                            fsel=strrchr(fname,'\\')+1;
                            LVITEM lvi={};
                            lvi.iItem=ListView_GetItemCount(OutList)+2;
                            int ni=ListView_InsertItem(OutList,&lvi);
                            ListView_SetItemText(OutList,ni,0,(LPSTR)fsel);
                            ListView_SetItemText(OutList,ni,1,(LPSTR)fname);
                        }
                } while (*first !='\0');

                Addicons(OutList);
                sureset(folder,0,sizeof(folder));
                sureset(fname,0,sizeof(fname));
                VirtualUnlock(folder,sizeof(folder));
                VirtualUnlock(fname,sizeof(fname));

        }

    sureset(&ofn,0,sizeof(ofn));
    sureset(szFile,0,sizeof(szFile));
    VirtualUnlock(&ofn,sizeof(ofn));
    VirtualUnlock(szFile,sizeof(szFile));

}

//Function to call file requester for loading incoming cipher-files
void FileLoader()
{
    if (CheckStatus()){return;}

    OPENFILENAME ofn={};
    char szFile[MAX_PATH*2+1]={};
    VirtualLock(szFile,sizeof(szFile));

    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwndmain;
    ofn.lpstrFile = szFile;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST|OFN_ALLOWMULTISELECT|OFN_EXPLORER;


if (GetOpenFileName(&ofn)==TRUE)
    {

        if (DecDetails.csp){DecDetails.csp->Release();DecDetails.csp=NULL;}
        if (DecDetails.cstgmed){ReleaseStgMedium(DecDetails.cstgmed);}
        if (DecDetails.dbuffer)
        {
            sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
            VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
            SetProcessWorkingSetSize(hProcess,defmin,defmax);
            delete[] DecDetails.dbuffer;
            DecDetails.dbuffer=NULL;
        }
        sureset(&DecDetails,0,sizeof(DecDetails));

        safescopy(DecDetails.cfile,szFile);
        if (szFile[strlen(szFile)+1]!='\0')
        {
            safescat(DecDetails.cfile,"\\");
            safescat(DecDetails.cfile,strchr(szFile,'\0')+1);
        }

        DecDetails.cformat=1;
        ContAck(1,2,strrchr(DecDetails.cfile,'\\')+1);
        SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,255));
        SetFocus(OutPass);
        ListView_DeleteAllItems(OutList);
        commode=1;InsToggle();
    }

sureset(szFile,0,sizeof(szFile));
VirtualUnlock(szFile,sizeof(szFile));

}


//Function to call file requester dialog for specifying source jpeg
void JpegRequestor(int mode)
{
    if (mode==0 and CheckStatus()){return;}

    OPENFILENAME ofn={};
    char szFile[MAX_PATH*2+1]={};
    char datapath[MAX_PATH*2+1]={};
    VirtualLock(&ofn,sizeof(ofn));
    VirtualLock(szFile,sizeof(szFile));
    VirtualLock(datapath,sizeof(datapath));

    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwndmain;
    ofn.lpstrFile = szFile;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = "Jpeg Images\0*.jpg;*.jpeg\0\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = datapath;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST|OFN_EXPLORER;

    SendMessage(SpecificJpegName,WM_GETTEXT,sizeof(datapath),(LPARAM)datapath);
    if ((strrchr(datapath,'\\'))!=NULL){*(char*)(strrchr(datapath,'\\'))='\0';}

    if (GetOpenFileName(&ofn)==TRUE)
    {
        SendMessage(SpecificJpegName,WM_SETTEXT,0,(LPARAM)szFile);
        SendMessage(RandomJpegOpt,BM_SETCHECK,0,0);SendMessage(SpecificJpegOpt,BM_SETCHECK,1,0);
    }

    sureset(&ofn,0,sizeof(ofn));
    sureset(szFile,0,sizeof(szFile));
    sureset(datapath,0,sizeof(datapath));
    VirtualUnlock(&ofn,sizeof(ofn));
    VirtualUnlock(szFile,sizeof(szFile));
    VirtualUnlock(datapath,sizeof(datapath));
}


void FolderOutputRequestor(HWND afolder,int mode)
{

if (mode==0 and CheckStatus()){return;}
AffectFolder=afolder;

OPENFILENAME ofn={};
char szFile[MAX_PATH*2+1]={};
char datapath[MAX_PATH*2+1]="C:\\\0";
VirtualLock(&ofn,sizeof(ofn));
VirtualLock(szFile,sizeof(szFile));
VirtualLock(datapath,sizeof(datapath));

ofn.lStructSize = sizeof(ofn);
ofn.hwndOwner = hwndmain;
ofn.lpstrFile = szFile;
ofn.lpstrFile[0] = '\0';
ofn.nMaxFile = sizeof(szFile);
ofn.lpstrFilter = "Folders\0zzzzzzzzzzzz.zzzzzzzzzz\0\0";
if (AffectFolder==RandomJpegName){ofn.lpstrFilter ="Jpeg Images\0*.jpg;*.jpeg\0\0";}
ofn.nFilterIndex = 1;
ofn.lpstrFileTitle = NULL;
ofn.lpstrTitle="Choose a Folder";
ofn.nMaxFileTitle = 0;
ofn.lpstrInitialDir = datapath;
ofn.lpfnHook=FolderSelect3;
ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST|OFN_EXPLORER|OFN_ENABLEHOOK;

if (strlen(SaveFilePath)>0){strncpy(datapath,SaveFilePath,MAX_PATH);}
if (afolder){SendMessage(AffectFolder,WM_GETTEXT,sizeof(datapath),(LPARAM)datapath);}
if (strlen(datapath)<1){strncpy(datapath,"C:\\\0",MAX_PATH);}

if (GetOpenFileName(&ofn)==TRUE)
{
    if (afolder==RandomJpegName){SendMessage(RandomJpegOpt,BM_SETCHECK,1,0);SendMessage(SpecificJpegOpt,BM_SETCHECK,0,0);}
    if (afolder==SpecFolder){SendMessage(OptUseSpecified,BM_SETCHECK,1,0);SendMessage(OptUseDefault,BM_SETCHECK,0,0);SendMessage(OptPrompt,BM_SETCHECK,0,0);}
}

sureset(&ofn,0,sizeof(ofn));
sureset(szFile,0,sizeof(szFile));
sureset(datapath,0,sizeof(datapath));
VirtualUnlock(&ofn,sizeof(ofn));
VirtualUnlock(szFile,sizeof(szFile));
VirtualUnlock(datapath,sizeof(datapath));
}


UINT_PTR CALLBACK FolderSelect3 (HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_INITDIALOG:
                {
                HWND hparent=GetParent(hDlg);
                SendMessage(hparent,CDM_HIDECONTROL,stc1,0);
                SendMessage(hparent,CDM_HIDECONTROL,cmb1,0);
                SendMessage(hparent,CDM_HIDECONTROL,stc2,0);
                SendMessage(hparent,CDM_HIDECONTROL,chx1,0);
                SendMessage(hparent,CDM_HIDECONTROL,cmb13,0);
                SendMessage(hparent,CDM_HIDECONTROL,edt1,0);
                SendMessage(hparent,CDM_SETCONTROLTEXT,IDOK,(LPARAM)"Select");
                SendMessage(hparent,CDM_SETCONTROLTEXT,stc3,(LPARAM)"Select a folder");
                ofnproc=SetWindowLong(hparent,GWL_WNDPROC,(long)FolderDialogProc3);
                return TRUE;
                }
    }
    return TRUE;
}


LRESULT CALLBACK FolderDialogProc3 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (message==WM_COMMAND && HIWORD(wParam)==BN_CLICKED && LOWORD(wParam)==IDOK)
    {
        char datapath[MAX_PATH*2+1]={};
        VirtualLock(datapath,sizeof(datapath));
        SendMessage(hwnd,CDM_GETFILEPATH,sizeof(datapath),(LPARAM)datapath);
        ifstream testfile(datapath);
        if (testfile.is_open()){strrchr(datapath,'\\')[0]='\0';}
        testfile.close();

        if (strlen(datapath)<1){return CallWindowProc((WNDPROC)ofnproc,hwnd,message,wParam,lParam);}
        if(AffectFolder){SendMessage(AffectFolder,WM_SETTEXT,0,(LPARAM)datapath);}
        else {safescopy(SaveFilePath,datapath);safescat(SaveFilePath,"\\");}
        sureset(datapath,0,sizeof(datapath));
        VirtualUnlock(datapath,sizeof(datapath));
        EndDialog(hwnd,TRUE);
    }
    return CallWindowProc((WNDPROC)ofnproc,hwnd,message,wParam,lParam);
}


//Function to toggle on-screen display of helper text
void InsToggle()
{
    static int oldcommode,oldinsmode,oldformode,olddesmode;

    int ns=ListView_GetSelectedCount(OutList);
    SendMessage(RemoveFilesButton,WM_SETTEXT,0,(LPARAM)(commode==0 ? (ns ? "Remove Selected" : "Remove All") : (ns ? "Save Selected" : "Save All")));
    SendMessage(AddFilesButton,WM_SETTEXT,0,(LPARAM)(commode==0 ? ("Add Files") : (ns ? "Open Selected" : "Open All")));

    if (commode!=oldcommode)
    {
        if (commode==0)
        {
            SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(255,255,255));
            ListView_SetBkColor(OutList,RGB(255,255,255));
            ListView_SetTextBkColor(OutList,RGB(255,255,255));
            InvalidateRect(OutEdit,NULL,true);
            InvalidateRect(OutList,NULL,true);
        }

        if (commode>0)
        {
            ListView_SetBkColor(OutList,RGB(215,215,215));
            ListView_SetTextBkColor(OutList,RGB(215,215,215));
            InvalidateRect(OutList,NULL,true);
        }

        if (commode==1)
        {
            SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,255));
            if (DecDetails.cformat!=3){ShowWindow(EncProg,SW_HIDE);}
        }

        if (commode==2)
        {SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,215));}

        if (commode!=0)
        {ShowWindow(CFileHolder,SW_HIDE);}

    }


    if (WaitForSingleObject(mtx,10000)==WAIT_OBJECT_0 and enctraffic==0 and dectraffic==0)
        {SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)(commode==0 ? (ebuttonpic0) : (dbuttonpic0)));}
        ReleaseMutex(mtx);


    if (formode!=oldformode or desmode!=olddesmode)
    {
        if (formode==3 and desmode==2){desmode=3;}
        if (formode==3){EnableWindow(EncDestOpt2,false);SendMessage(EncDestOpt2,WM_SETTEXT,0,(LPARAM)"-----");}
        else {EnableWindow(EncDestOpt2,true);SendMessage(EncDestOpt2,WM_SETTEXT,0,(LPARAM)"Folder");}

        if (formode==1){ShowWindow(OptsHolder,SW_SHOW);ShowWindow(OptsHolder2,SW_HIDE);ShowWindow(OptsHolder4,SW_HIDE);}
        else if (formode==2){ShowWindow(OptsHolder2,SW_SHOW);ShowWindow(OptsHolder,SW_HIDE);ShowWindow(OptsHolder4,SW_HIDE);}
        else if (formode==3){ShowWindow(OptsHolder2,SW_HIDE);ShowWindow(OptsHolder,SW_HIDE);ShowWindow(OptsHolder4,SW_SHOW);}

        InvalidateRect(EncFormatOpt1,NULL,true);
        InvalidateRect(EncFormatOpt2,NULL,true);
        InvalidateRect(EncFormatOpt3,NULL,true);

        if (desmode==2){ShowWindow(OptsHolder3,SW_SHOW);ShowWindow(OptsHolder5,SW_HIDE);}
        else {ShowWindow(OptsHolder3,SW_HIDE);ShowWindow(OptsHolder5,SW_SHOW);}

        if (formode==1 and desmode==1)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Encrypted cipher-file will be placed on-screen for easy drag-and-drop operations");}

        if (formode==1 and desmode==3)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Encrypted cipher-file will be copied to system clipboard for pasting elsewhere");}

        if (formode==2 and desmode==1)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Modified Jpeg file will be placed on-screen for easy drag-and-drop operations");}

        if (formode==2 and desmode==3)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Modified Jpeg file will be copied to system clipboard for pasting elsewhere");}

        if (formode==3 and desmode==1)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Encrypted cipher-text will be displayed in the main message window");}

        if (formode==3 and desmode==3)
        {SendMessage(TextLabel2,WM_SETTEXT,0,(LPARAM)"Encrypted cipher-text will be copied to system clipboard for pasting elsewhere");}

        InvalidateRect(EncDestOpt1,NULL,true);
        InvalidateRect(EncDestOpt2,NULL,true);
        InvalidateRect(EncDestOpt3,NULL,true);

    }




    if (insmode!=oldinsmode)
    {

        if (insmode==1)
        {
            for (int x=1;x<=5;x++)
            {SendMessage(labels[x].h,WM_SETTEXT,0,(LPARAM)labels[x].help);}
            SendMessage(labels[7].h,WM_SETTEXT,0,(LPARAM)labels[7].nohelp);
            SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)ebuttonpic1);
            MoveWindow(PMaskE,625 hh, 802 vv, 70 hh, 30 vv,true);
            MoveWindow(labels[5].h,375 hh, 805 vv, 230 hh, 30 vv,true);
            InvalidateRect(labels[5].h,NULL,true);
            SetWindowPos(OutBlocker, HWND_BOTTOM, 0 hh,485 vv,350 hh,700 vv, SWP_FRAMECHANGED | SWP_NOOWNERZORDER);
            SendMessage(OutBlocker,WM_SETTEXT,0,(LPARAM)"\xe1\0\n\xe1\n\xe1");
            SendMessage(OutBlocker2,WM_SETTEXT,0,(LPARAM)"\xe2");
            ShowWindow(OutBlocker,SW_SHOW);ShowWindow(OutBlocker2,SW_HIDE);
        }

        else if (insmode==2)
        {
            for (int x=1;x<5;x++)
            {SendMessage(labels[x].h,WM_SETTEXT,0,(LPARAM)labels[x].nohelp);}
            for (int x=5;x<8;x++)
            {SendMessage(labels[x].h,WM_SETTEXT,0,(LPARAM)labels[x].ihelp);}
            SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)dbuttonpic1);
            MoveWindow(PMaskE,625 hh, 802 vv, 70 hh, 30 vv,true);
            MoveWindow(labels[5].h,375 hh, 805 vv, 230 hh, 30 vv,true);
            InvalidateRect(labels[5].h,NULL,true);
            SetWindowPos(OutBlocker, HWND_BOTTOM, 0 hh,0 vv,350 hh,485 vv, SWP_FRAMECHANGED | SWP_NOOWNERZORDER);
            SetWindowPos(OutBlocker2, HWND_BOTTOM, 0 hh,730 vv,350 hh,500 vv, SWP_FRAMECHANGED | SWP_NOOWNERZORDER);
            SendMessage(OutBlocker,WM_SETTEXT,0,(LPARAM)"\n\n\xe2\0\n\xe1\n\xe1");
            SendMessage(OutBlocker2,WM_SETTEXT,0,(LPARAM)"\xe1");
            ShowWindow(OutBlocker,SW_SHOW);ShowWindow(OutBlocker2,SW_SHOW);
        }

        else
        {
            for (int x=1;x<8;x++)
            {
                labelinstext=RGB(0,0,0);
                labelinsback=RGB(233,236,216);
                SendMessage(labels[x].h,WM_SETTEXT,0,(LPARAM)labels[x].nohelp);
                MoveWindow(labels[5].h,375 hh, 805 vv, 170 hh, 30 vv,true);
                InvalidateRect(labels[5].h,NULL,true);
                MoveWindow(PMaskE,565 hh, 802 vv, 70 hh, 30 vv,true);
            }
            ShowWindow(OutBlocker,SW_HIDE);ShowWindow(OutBlocker2,SW_HIDE);
        }

    InvalidateRect(OthOptsHolder,NULL,true);
    InvalidateRect(InOptsHolder,NULL,true);
    InvalidateRect(DWindow,NULL,true);
    InvalidateRect(OptsHolder2,NULL,true);

    }

    oldcommode=commode;
    oldinsmode=insmode;
    oldformode=formode;
    olddesmode=desmode;


}

//Function to load non-bitmap type image files and convert them
HBITMAP LoadPictResource(LPCTSTR resId, LPCTSTR resType)
{
	HRSRC hRes = FindResource( NULL, resId, resType);
	DWORD dwFileSize = SizeofResource( NULL, hRes );
	HGLOBAL hSplash = LoadResource( NULL, hRes );
	LPVOID pSplash = LockResource( hSplash );
	LPVOID pvData = NULL;
	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, dwFileSize);
	pvData = GlobalLock(hGlobal);
	CopyMemory( pvData, pSplash, dwFileSize );
	GlobalUnlock(hGlobal);
	LPSTREAM pstm = NULL;
	CreateStreamOnHGlobal(hGlobal, FALSE, &pstm);
	HBITMAP hbm = NULL;
	Gdiplus::Bitmap* pBitmap = Gdiplus::Bitmap::FromStream(pstm);
    if (pBitmap)
    {
        pBitmap->GetHBITMAP(Gdiplus::Color(0, 0, 0), &hbm);
    }
    pstm->Release();
    GlobalFree(pvData);
    return hbm;
}


//Function to delete all files and sub-folders within parent folder
int DeleteDirectory(char refcstrRootDirectory[MAX_PATH],bool bDeleteSubdirectories)
{
  bool bSubdirectory = false;
  HANDLE hFile;
  char strFilePath[MAX_PATH*2+1]={};
  char strPattern[MAX_PATH*2+1]={};
  VirtualLock(strFilePath,sizeof(strFilePath));
  VirtualLock(strPattern,sizeof(strPattern));
  WIN32_FIND_DATA FileInformation;
  safescopy(strPattern,refcstrRootDirectory);
  safescat(strPattern,"\\*.*");

  hFile = ::FindFirstFile(strPattern, &FileInformation);
  if(hFile != INVALID_HANDLE_VALUE)
  {
    do
    {
      if(FileInformation.cFileName[0] != '.')
      {
        sureset(strFilePath,0,sizeof(strFilePath));
        safescopy(strFilePath,refcstrRootDirectory);
        safescat(strFilePath,"\\");
        safescat(strFilePath,FileInformation.cFileName);

        if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
          if(bDeleteSubdirectories)
          {
            // Delete subdirectory
            int iRC = DeleteDirectory(strFilePath, bDeleteSubdirectories);
            RemoveDirectory(strFilePath);
            if(iRC)
              return iRC;
          }
          else
            bSubdirectory = true;
        }
        else
        {
          // Set file attributes
          SetFileAttributes(strFilePath,FILE_ATTRIBUTE_NORMAL);
          // Zero-force binary contents
          ZeroFile(strFilePath);
          // Delete file
          DeleteFile(strFilePath);
        }
      }
    } while(FindNextFile(hFile, &FileInformation) == TRUE);

    // Close handle
    FindClose(hFile);

    DWORD dwError = GetLastError();
    if(dwError != ERROR_NO_MORE_FILES)
      {return dwError;}
    else
    {
      if(!bSubdirectory)
      {
        // Set directory attributes
        SetFileAttributes(refcstrRootDirectory,FILE_ATTRIBUTE_NORMAL);

      }
    }
  }

    sureset(strFilePath,0,sizeof(strFilePath));
    sureset(strPattern,0,sizeof(strPattern));
    VirtualUnlock(strFilePath,sizeof(strFilePath));
    VirtualUnlock(strPattern,sizeof(strPattern));

  return 0;
}


int ListJFiles(char refcstrRootDirectory[MAX_PATH*2+1],vector<char*>& jpegs,long long neededsize)
{
  HANDLE hFile;
  char strFilePath[MAX_PATH*2+1]={};
  char strPattern[MAX_PATH*2+1]={};
  VirtualLock(strFilePath,sizeof(strFilePath));
  VirtualLock(strPattern,sizeof(strPattern));
  WIN32_FIND_DATA FileInformation;
  safescopy(strPattern,refcstrRootDirectory);
  safescat(strPattern,"\\*");

  hFile = ::FindFirstFile(strPattern, &FileInformation);
  if(hFile != INVALID_HANDLE_VALUE)
  {
    do
    {
      if(FileInformation.cFileName[0] != '.')
      {
        sureset(strFilePath,0,sizeof(strFilePath));
        safescopy(strFilePath,refcstrRootDirectory);
        safescat(strFilePath,"\\");
        safescat(strFilePath,FileInformation.cFileName);

        if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            ListJFiles(strFilePath,jpegs,neededsize);
        }
        else
        {
            long long fsize =  (FileInformation.nFileSizeHigh * (MAXDWORD+1)) + FileInformation.nFileSizeLow;
            char* fsel=strrchr(strFilePath,'.');
            char* forig=fsel;
            while (fsel[0])
            {
                fsel[0]=tolower(fsel[0]);
                fsel++;
            }
            if ((strcmp(forig,".jpg")==0 or strcmp(forig,".jpeg")==0) and fsize>=neededsize)
            {
                char * c = new char[MAX_PATH*2+1];
                VirtualLock(c,MAX_PATH*2+1);
                strncpy(c,strFilePath,MAX_PATH);
                jpegs.push_back(c);
            }
        }
      }
    } while(FindNextFile(hFile, &FileInformation) == TRUE);

    FindClose(hFile);

  }


    sureset(strFilePath,0,sizeof(strFilePath));
    sureset(strPattern,0,sizeof(strPattern));
    VirtualUnlock(strFilePath,sizeof(strFilePath));
    VirtualUnlock(strPattern,sizeof(strPattern));
    return 0;

}



//Function to fill file contents with all zero bytes
void ZeroFile(char* dfile)
{
        long long buffsize=segmentsize;
        char * buffer = new char[buffsize];
        sureset(buffer,0,buffsize);

        ofstream outfile;
        outfile.clear();
        outfile.rdbuf()->pubsetbuf(0,0);

        outfile.open(dfile,outfile.out|outfile.binary|outfile.in);
        if (outfile.fail()){return;}

        long long fsize=0;
        long long written=0;

        outfile.seekp(0,outfile.end);
        fsize=outfile.tellp();
        outfile.seekp(0,outfile.beg);
        if(fsize<0){fsize=0;}

        while (written < fsize)
        {
            long wam=buffsize < fsize-written ? buffsize : fsize-written;
            outfile.write(buffer, wam);
            written+=wam;
        }
        outfile.close();
        delete[] buffer;
        return;
}

//Function to toggle password mask
void PMaskEToggle()
{
    if (SendMessage(PMaskE,BM_GETCHECK,0,0)==BST_CHECKED)
    {SendMessage(OutPass,EM_SETPASSWORDCHAR,42,0);}
    else {SendMessage(OutPass,EM_SETPASSWORDCHAR,0,0);}
    InvalidateRect(OutPass,0,0);
}


//Function to save all user preferences upon program exit
void SavePrefs()
{
    ShowWindow(hwndmain,SW_HIDE);
    char buffer[1000]={};
    VirtualLock(buffer,sizeof(buffer));
    SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,buffer);
    GetModuleFileNameA(NULL, buffer, MAX_PATH); strrchr(buffer,'\\')[0]=0;
    safescat(buffer,"\\CenoCipherTempFiles");
    //safescat(buffer,"\\cenocipher");
    DeleteDirectory(buffer);
    RemoveDirectory(buffer);
    if (!SendMessage(SaveSettings,BM_GETCHECK,0,0)){sureset(buffer,0,sizeof(buffer));return;}
    strrchr(buffer,'\\')[0]=0;
    safescat(buffer,"\\cenocipher.dat");
    FILE * fp;
    fp=fopen(buffer,"w+");
    sureset(buffer,0,sizeof(buffer));

    safescat(buffer,"HidePassE");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(PMaskE,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"USECUST");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(CustNameOpt,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"IncludeFiles");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(IncludeFiles,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"WithQuote");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(WithQuote,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"ClearPassword");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(ClearPassword,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"EXTNAME");
    safescat(buffer,"\x1b");
    SendMessage(ExtName,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    safescat(buffer,"CUSTNAME");
    safescat(buffer,"\x1b");
    SendMessage(CustName,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    safescat(buffer,"USEJFOLDER");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(RandomJpegOpt,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"SPECJPEG");
    safescat(buffer,"\x1b");
    SendMessage(SpecificJpegName,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    safescat(buffer,"JFOLDER");
    safescat(buffer,"\x1b");
    SendMessage(RandomJpegName,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    safescat(buffer,"OFOLDER");
    safescat(buffer,"\x1b");
    SendMessage(FolderOutput,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    safescat(buffer,"FORMODE");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(formode).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"DESMODE");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(desmode).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"AUTODECRYPT");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(AutoDecrypt,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"SAVESETTINGS");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(SaveSettings,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"OPTPROMPT");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(OptPrompt,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"OPTUSEDEFAULT");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(OptUseDefault,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"OPTUSESPECIFIED");
    safescat(buffer,"\x1b");
    safescat(buffer,Str(SendMessage(OptUseSpecified,BM_GETCHECK,0,0)).c_str());
    safescat(buffer,"\n");

    safescat(buffer,"SPECFOLDER");
    safescat(buffer,"\x1b");
    SendMessage(SpecFolder,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
    safescat(buffer,"\n");

    if (IsWindow(PromptWindow))
    {
        safescat(buffer,"PROMPTABORT");
        safescat(buffer,"\x1b");
        safescat(buffer,Str(SendMessage(PromptAbort,BM_GETCHECK,0,0)).c_str());
        safescat(buffer,"\n");

        safescat(buffer,"PROMPTUSEDEFAULT");
        safescat(buffer,"\x1b");
        safescat(buffer,Str(SendMessage(PromptUseDefault,BM_GETCHECK,0,0)).c_str());
        safescat(buffer,"\n");

        safescat(buffer,"PROMPTUSESPECIFIED");
        safescat(buffer,"\x1b");
        safescat(buffer,Str(SendMessage(PromptUseSpecified,BM_GETCHECK,0,0)).c_str());
        safescat(buffer,"\n");

        safescat(buffer,"PROMPTSPECFOLDER");
        safescat(buffer,"\x1b");
        SendMessage(PromptSpecFolder,WM_GETTEXT,255,(LPARAM)strchr(buffer,'\0'));
        safescat(buffer,"\n");
    }

    fputs(buffer,fp);
    fclose(fp);
    sureset(buffer,0,sizeof(buffer));
    VirtualUnlock(buffer,sizeof(buffer));
}


//Function to load all user preferences upon program start
void CheckPrefs()
{
    char buffer[1000]={};
    VirtualLock(buffer,sizeof(buffer));

    SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,buffer);
    GetModuleFileNameA(NULL, buffer, MAX_PATH); strrchr(buffer,'\\')[0]=0;
    //safescat(buffer,"\\CenoCipherTempFiles");
    //CreateDirectory(buffer,NULL);
    safescat(buffer,"\\cenocipher.dat");
    FILE * fp;
    fp=fopen(buffer,"r+");

    if (fp==0){return;}

    while (fgets(buffer,255,fp)!=NULL)
    {
        char* prefname=buffer;
        char* prefval=strchr(buffer,char(27));
        if (prefval==NULL){return;}
        prefval++;
        prefval[strlen(prefval)-1]='\0';
        long pval=strtol(prefval,NULL,10);

        if (strncmp("HidePassE",prefname,prefval-prefname-1)==0)
        {SendMessage(PMaskE,BM_SETCHECK,pval,0);PMaskEToggle();}

        if (strncmp("USECUST",prefname,prefval-prefname-1)==0)
        {SendMessage(CustNameOpt,BM_SETCHECK,pval,0);SendMessage(RandExtOpt,BM_SETCHECK,!pval,0);}

        if (strncmp("EXTNAME",prefname,prefval-prefname-1)==0)
        {SendMessage(ExtName,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("CUSTNAME",prefname,prefval-prefname-1)==0)
        {SendMessage(CustName,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("USEJFOLDER",prefname,prefval-prefname-1)==0)
        {SendMessage(RandomJpegOpt,BM_SETCHECK,pval,0);SendMessage(SpecificJpegOpt,BM_SETCHECK,!pval,0);}

        if (strncmp("SPECJPEG",prefname,prefval-prefname-1)==0)
        {SendMessage(SpecificJpegName,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("JFOLDER",prefname,prefval-prefname-1)==0)
        {SendMessage(RandomJpegName,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("OFOLDER",prefname,prefval-prefname-1)==0)
        {SendMessage(FolderOutput,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("IncludeFiles",prefname,prefval-prefname-1)==0)
        {SendMessage(IncludeFiles,BM_SETCHECK,pval,0);}

        if (strncmp("WithQuote",prefname,prefval-prefname-1)==0)
        {SendMessage(WithQuote,BM_SETCHECK,pval,0);}

        if (strncmp("ClearPassword",prefname,prefval-prefname-1)==0)
        {SendMessage(ClearPassword,BM_SETCHECK,pval,0);}

        if (strncmp("FORMODE",prefname,prefval-prefname-1)==0)
        {formode=pval;}

        if (strncmp("DESMODE",prefname,prefval-prefname-1)==0)
        {desmode=pval;}

        if (strncmp("AUTODECRYPT",prefname,prefval-prefname-1)==0)
        {SendMessage(AutoDecrypt,BM_SETCHECK,pval,0);}

        if (strncmp("SAVESETTINGS",prefname,prefval-prefname-1)==0)
        {SendMessage(SaveSettings,BM_SETCHECK,pval,0);}

        if (strncmp("OPTPROMPT",prefname,prefval-prefname-1)==0)
        {SendMessage(OptPrompt,BM_SETCHECK,pval,0);}

        if (strncmp("OPTUSEDEFAULT",prefname,prefval-prefname-1)==0)
        {SendMessage(OptUseDefault,BM_SETCHECK,pval,0);}

        if (strncmp("OPTUSESPECIFIED",prefname,prefval-prefname-1)==0)
        {SendMessage(OptUseSpecified,BM_SETCHECK,pval,0);}

        if (strncmp("PROMPTABORT",prefname,prefval-prefname-1)==0)
        {SendMessage(PromptAbort,BM_SETCHECK,pval,0);}

        if (strncmp("PROMPTUSEDEFAULT",prefname,prefval-prefname-1)==0)
        {SendMessage(PromptUseDefault,BM_SETCHECK,pval,0);}

        if (strncmp("PROMPTUSESPECIFIED",prefname,prefval-prefname-1)==0)
        {SendMessage(PromptUseSpecified,BM_SETCHECK,pval,0);}

        if (strncmp("SPECFOLDER",prefname,prefval-prefname-1)==0)
        {SendMessage(SpecFolder,WM_SETTEXT,0,(LPARAM)prefval);}

        if (strncmp("PROMPTSPECFOLDER",prefname,prefval-prefname-1)==0)
        {SendMessage(PromptSpecFolder,WM_SETTEXT,0,(LPARAM)prefval);}

    }
    fclose(fp);
    sureset(buffer,0,sizeof(buffer));
    VirtualUnlock(buffer,sizeof(buffer));
}


//Function to perform encryption of outgoing content and create cipher-file
DWORD WINAPI EncryptComm(long v)
{
    if (SendMessage(OutEdit,WM_GETTEXTLENGTH,0,0)==0 and ListView_GetItemCount(OutList)==0)
    {syme(7,"Nothing to encrypt. Add message text and/or attach outgoing files first.");return 0;}

    if (SendMessage(OutPass,WM_GETTEXTLENGTH,0,0)==0)
    {syme(7,"Please enter a passphrase");SetFocus(OutPass);return 0;}

    if (WaitForSingleObject(mtx,10000)!=WAIT_OBJECT_0){return 0;}
    if (enctraffic==1){enctraffic=2;ReleaseMutex(mtx);return 0;}
    else if (enctraffic==2){ReleaseMutex(mtx);return 0;}
    else {enctraffic=1;ReleaseMutex(mtx);}

    SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)ebuttonpic2);
    syme(3,"Encrypting...");
    SendMessage(EncProg,PBM_SETPOS,0,0);
    ShowWindow(EncProg,SW_SHOW);
    ListView_DeleteAllItems(CFileHolder);
    ListView_SetBkColor(CFileHolder,RGB(255,255,255));
    ListView_SetTextBkColor(CFileHolder,RGB(255,255,255));
    InvalidateRect(CFileHolder,NULL,true);
    ShowWindow(CFileHolder,false);

    keyset * eset = new keyset;
    VirtualLock(eset,sizeof(keyset));
    sureset(eset,0,sizeof(keyset));
    LARGE_INTEGER li;li.LowPart=1;
    unsigned rgbytes=1;

    eset->iseeds[37]=1;
    EnumChildWindows(hwndmain,&EnumChildProc,(LPARAM)&eset->iseeds[0]);

    for (int x=0;x<40;x++)
    {
        QueryPerformanceCounter(&li);
        eset->iseeds[x]*=GetCurrentThreadId();
        eset->iseeds[x]^=li.LowPart;
        if (RtlGenRandom(&rgbytes,sizeof(unsigned))){eset->iseeds[x]^=rgbytes;}
    }


    IsaacRand rng0(eset->iseeds[0],5);
    IsaacRand rng1(eset->iseeds[5],5);
    IsaacRand rng2(eset->iseeds[10],5);
    IsaacRand rng3(eset->iseeds[15],5);
    IsaacRand rng4(eset->iseeds[20],5);
    IsaacRand rng5(eset->iseeds[25],5);
    IsaacRand rng6(eset->iseeds[30],3);
    IsaacRand rng7(eset->iseeds[33],3);
    unsigned randbytes=0;

    for (int x=0;x<4;x++)
    {
        randbytes=rng0.getNext();memcpy(eset->iv+x*4,&randbytes,sizeof(randbytes));
        randbytes=rng1.getNext();memcpy(eset->salt+x*4,&randbytes,sizeof(randbytes));
        randbytes=rng2.getNext();memcpy(eset->twofishiv+x*4,&randbytes,sizeof(randbytes));
        randbytes=rng3.getNext();memcpy(eset->twofishsalt+x*4,&randbytes,sizeof(randbytes));
        randbytes=rng4.getNext();memcpy(eset->serpentiv+x*4,&randbytes,sizeof(randbytes));
        randbytes=rng5.getNext();memcpy(eset->serpentsalt+x*4,&randbytes,sizeof(randbytes));
    }


    if (ListView_GetItemCount(OutList)==0){SendMessage(EncProg,PBM_SETPOS,0,0);ShowWindow(EncProg,SW_SHOW);}

    SendMessage(OutPass,WM_GETTEXT,255,(LPARAM)eset->passkey);

    PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->salt,sizeof(eset->salt),prounds/3,sizeof(eset->key),eset->key);
    PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->twofishsalt,sizeof(eset->twofishsalt),prounds/3,sizeof(eset->twofishkey),eset->twofishkey);
    PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->serpentsalt,sizeof(eset->serpentsalt),prounds/3,sizeof(eset->serpentkey),eset->serpentkey);

    sha256_context ctxhmackey;
    sha256_init(&ctxhmackey);
    sha256_starts(&ctxhmackey,0);

    sha256_update(&ctxhmackey,eset->key,sizeof(eset->key));
    sha256_update(&ctxhmackey,eset->twofishkey,sizeof(eset->twofishkey));
    sha256_update(&ctxhmackey,eset->serpentkey,sizeof(eset->serpentkey));
    sha256_finish(&ctxhmackey,eset->hmackey);
    sha256_free(&ctxhmackey);


    long long messsize=OEditStreamOutCheck()+2;
    messbuff = new char[messsize];
    VirtualLock(messbuff,messsize);
    sureset(messbuff,0,messsize);
    OEditStreamOut();

    long splitchar = rng6.getNext()%messsize;

    char * messbufftemp = new char[messsize];
    VirtualLock(messbufftemp,messsize);
    sureset(messbufftemp,0,messsize);

    strncat(messbufftemp,messbuff+splitchar,messsize-splitchar);
    strncat(messbufftemp,"\xff",1);
    strncat(messbufftemp,messbuff,splitchar);
    memcpy(messbuff,messbufftemp,messsize);
    sureset(messbufftemp,0,messsize);

    long long filetablesize=strlen(filetableheader);
    for (int x=0;x<ListView_GetItemCount(OutList);x++)
    {
        ListView_GetItemText(OutList,x,0,(LPTSTR)eset->buffer,MAX_PATH);
        filetablesize+=strlen((char*)eset->buffer)+strlen("|:*")+19+19;
    }

    filetablesize+=1;
    char * filetablebuff = new char[filetablesize];
    VirtualLock(filetablebuff,filetablesize);
    sureset(filetablebuff,0,filetablesize);

    long long filestart=messsize+filetablesize;
    long long allfilesize=0;
    strncpy(filetablebuff,filetableheader,strlen(filetableheader));

    char * skippedfiles = new char[MAX_PATH*(ListView_GetItemCount(OutList)+1)];
    VirtualLock(skippedfiles,MAX_PATH*(ListView_GetItemCount(OutList)+1));
    sureset(skippedfiles,0,MAX_PATH*(ListView_GetItemCount(OutList)+1));
    strncpy(skippedfiles,"The following files were excluded because they were unavailable:\n\n",MAX_PATH);
    strncpy(skippedfiles,"\0",1);

    for (int x=0;x<ListView_GetItemCount(OutList);x++)
    {
        ListView_GetItemText(OutList,x,1,(LPTSTR)eset->buffer,MAX_PATH);
        LARGE_INTEGER nLargeInteger = {};
        HANDLE hFile = CreateFile((LPCSTR)eset->buffer, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
		if (hFile==INVALID_HANDLE_VALUE){strncpy(skippedfiles,"T",1);strncat(skippedfiles,(char*)eset->buffer,MAX_PATH);strncat(skippedfiles,"\n",3);continue;}
		GetFileSizeEx(hFile, &nLargeInteger);
		long long realsize=nLargeInteger.QuadPart;
        allfilesize+=realsize;
        ListView_GetItemText(OutList,x,0,(LPTSTR)eset->buffer,MAX_PATH);
        strcat(filetablebuff,"|");strcat(filetablebuff,(char*)eset->buffer);
        strcat(filetablebuff,":");
        strncat(filetablebuff,Padleft(QuadToString(filestart),19,'0').c_str(),19);
        strcat(filetablebuff,"*");
        strncat(filetablebuff,Padleft(QuadToString(realsize),19,'0').c_str(),19);
        filestart+=realsize;
        CloseHandle(hFile);
    }



    if (desmode==2)
    {
        SendMessage(FolderOutput,WM_GETTEXT,255,(LPARAM)eset->cipherfile);
        safescat(eset->cipherfile,"\\");
        if(strlen(eset->cipherfile)<2){safescopy(eset->cipherfile,"\x1b");}
    }
    else
    {
        SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,eset->cipherfile);
        safescat(eset->cipherfile,"\\cenocipher\\");
        GetModuleFileNameA(NULL, eset->cipherfile, MAX_PATH); strrchr(eset->cipherfile,'\\')[0]=0;
        safescat(eset->cipherfile,"\\CenoCipherTempFiles\\");
        CreateDirectory(eset->cipherfile,NULL);
    }



    if (!SendMessage(CustNameOpt,BM_GETCHECK,0,0) or !SendMessage(CustName,WM_GETTEXT,255,(LPARAM)eset->buffer))
    {
        for (unsigned x=1;x<(rng7.getNext()%10)+10;x++)
        {
            unsigned letter=(rng7.getNext() % 26)+97;
            safescat(eset->cipherfile,(&letter),1);
        }

        safescat(eset->cipherfile,".");
        char ext[10]={};
        SendMessage(ExtName,WM_GETTEXT,10,(LPARAM)ext);
        safescat(eset->cipherfile,ext);
    }
    else
    {
        SendMessage(CustName,WM_GETTEXT,255,(LPARAM)(eset->cipherfile+strlen((char*)eset->cipherfile)));
    }


    ZeroFile(eset->cipherfile);
    DeleteFile(eset->cipherfile);

    ofstream outfile;
    outfile.rdbuf()->pubsetbuf(0,0);
    outfile.open(eset->cipherfile,ios_base::binary|ios_base::out);


    if (!outfile.is_open())
    {
        if (desmode==2){syme(7,"Encryption aborted: Error writing to the specified output location.",FolderOutput);}
        else {syme(7,"Encryption aborted: Error writing to temporary disk location.");}
        sureset(eset,0,sizeof(keyset));
        serpent_clearkey();
        VirtualUnlock(messbuff,messsize);VirtualUnlock(messbufftemp,messsize);VirtualUnlock(filetablebuff,filetablesize);
        VirtualUnlock(eset,sizeof(keyset));
        delete eset;delete[] filetablebuff;delete[] messbuff;
        if (WaitForSingleObject(mtx,10000)!=WAIT_OBJECT_0){return 0;}
        enctraffic=0;
        ReleaseMutex(mtx);
        SendMessage(EncProg,PBM_SETPOS,0,0);
        commode=0;
        InsToggle();
        return 0;
    }


    outfile.write((char*)eset->buffer,sizeof(eset->hmachash));
    outfile.write((char*)eset->salt,sizeof(eset->salt));
    outfile.write((char*)eset->iv,sizeof(eset->iv));
    outfile.write((char*)eset->twofishsalt,sizeof(eset->twofishsalt));
    outfile.write((char*)eset->twofishiv,sizeof(eset->twofishiv));
    outfile.write((char*)eset->serpentsalt,sizeof(eset->serpentsalt));
    outfile.write((char*)eset->serpentiv,sizeof(eset->serpentiv));

    aes_setkey_enc(&eset->aes, eset->key, 256 );
    Twofish_initialise();
    Twofish_prepare_key(eset->twofishkey,32,&eset->tkey);
    set_key((u4byte*)eset->serpentkey,256); //serpent key

    sha256_context ctx256;
    sha256_init(&ctx256);
    sha256_hmac_starts(&ctx256,eset->hmackey,sizeof(eset->hmackey),0);
    sha256_hmac_update(&ctx256,eset->iv,sizeof(eset->iv));
    sha256_hmac_update(&ctx256,eset->twofishiv,sizeof(eset->twofishiv));
    sha256_hmac_update(&ctx256,eset->serpentiv,sizeof(eset->serpentiv));

    sha256_context ctxobf;
    sha256_init(&ctxobf);
    sha256_starts(&ctxobf,0);

    long long parts = (messsize/segmentsize);
    long rempart=messsize % segmentsize;
    if (rempart > 0){parts+=1;}

    for (long long x=1;x<=parts;x++)
    {
        char * startpoint=(char*)messbuff+(segmentsize*(x-1));
        long copylen = segmentsize;
        if (x==parts and rempart>0){copylen=rempart;}
        memcpy(eset->buffer,startpoint,copylen);
        aes_crypt_ctr(&eset->aes,copylen,&eset->ANCOffset,eset->iv,eset->astreamblock,eset->buffer,eset->buffer);
        Twofish_CTR_Encrypt(&eset->tkey,copylen,&eset->TNCOffset,eset->twofishiv,eset->tstreamblock,eset->buffer,eset->buffer);
        Serpent_CTR_Encrypt(copylen,&eset->SNCOffset,eset->serpentiv,eset->sstreamblock,eset->buffer,eset->buffer);
        sha256_hmac_update(&ctx256,eset->buffer,copylen);
        sha256_update(&ctxobf,eset->buffer,copylen);
        outfile.write((char*)eset->buffer,copylen);
    }
    sureset(messbuff,0,messsize);
    messbuff=NULL;

    parts = (filetablesize/segmentsize);
    rempart=filetablesize % segmentsize;
    if (rempart > 0){parts+=1;}

    for (long long x=1;x<=parts;x++)
    {
        char * startpoint=(char*)filetablebuff+(segmentsize*(x-1));
        long copylen = segmentsize;
        if (x==parts and rempart>0){copylen=rempart;}
        memcpy(eset->buffer,startpoint,copylen);
        aes_crypt_ctr(&eset->aes,copylen,&eset->ANCOffset,eset->iv,eset->astreamblock,eset->buffer,eset->buffer);
        Twofish_CTR_Encrypt(&eset->tkey,copylen,&eset->TNCOffset,eset->twofishiv,eset->tstreamblock,eset->buffer,eset->buffer);
        Serpent_CTR_Encrypt(copylen,&eset->SNCOffset,eset->serpentiv,eset->sstreamblock,eset->buffer,eset->buffer);
        sha256_hmac_update(&ctx256,eset->buffer,copylen);
        sha256_update(&ctxobf,eset->buffer,copylen);
        outfile.write((char*)eset->buffer,copylen);
    }
    sureset(filetablebuff,0,filetablesize);

    long long totalwritten=0;
    if (ListView_GetItemCount(OutList)!=0){SendMessage(EncProg,PBM_SETPOS,0,0);}

    for (int x=0;x<ListView_GetItemCount(OutList);x++)
    {
        ListView_GetItemText(OutList,x,1,(LPTSTR)eset->buffer,MAX_PATH);
        ifstream infile;
        infile.rdbuf()->pubsetbuf(0,0);
        infile.open((char*)eset->buffer,ios_base::in|ios_base::binary);

        if (!infile.good() and strstr(skippedfiles,(char*)eset->buffer)==NULL)
        {
            infile.close();outfile.close();
            ZeroFile(eset->cipherfile);
            DeleteFile(eset->cipherfile);
            sureset(eset,0,sizeof(keyset));
            serpent_clearkey();
            VirtualUnlock(messbuff,messsize);VirtualUnlock(messbufftemp,messsize);VirtualUnlock(filetablebuff,filetablesize);
            VirtualUnlock(eset,sizeof(keyset));
            delete eset;delete[] filetablebuff;delete[] messbuff;
            if (WaitForSingleObject(mtx,10000)!=WAIT_OBJECT_0){return 0;}
            enctraffic=0;
            ReleaseMutex(mtx);
            CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)EncryptComm,NULL,0,NULL);
            return 0;
        }

         while (infile)
         {
            infile.read((char*)eset->buffer,segmentsize);
            long copylen=infile.gcount();

            aes_crypt_ctr(&eset->aes,copylen,&eset->ANCOffset,eset->iv,eset->astreamblock,eset->buffer,eset->buffer);
            Twofish_CTR_Encrypt(&eset->tkey,copylen,&eset->TNCOffset,eset->twofishiv,eset->tstreamblock,eset->buffer,eset->buffer);
            Serpent_CTR_Encrypt(copylen,&eset->SNCOffset,eset->serpentiv,eset->sstreamblock,eset->buffer,eset->buffer);
            sha256_hmac_update(&ctx256,eset->buffer,copylen);
            sha256_update(&ctxobf,eset->buffer,copylen);
            outfile.write((char*)eset->buffer,copylen);
            totalwritten+=copylen;
            double t=(((double)totalwritten/(double)allfilesize)*100);
            int p = (int)t;
            SendMessage(EncProg,PBM_SETPOS,p,0);

            if (WaitForSingleObject(mtx,10000)!=WAIT_OBJECT_0){continue;}
            if (enctraffic==2)
                {
                    infile.close();outfile.close();
                    ZeroFile(eset->cipherfile);
                    DeleteFile(eset->cipherfile);
                    sureset(eset,0,sizeof(keyset));
                    serpent_clearkey();
                    VirtualUnlock(messbuff,messsize);VirtualUnlock(messbufftemp,messsize);VirtualUnlock(filetablebuff,filetablesize);
                    VirtualUnlock(eset,sizeof(keyset));
                    delete eset;delete[] filetablebuff;delete[] messbuff;
                    syme(2,"Encryption canceled.");
                    SendMessage(EncProg,PBM_SETPOS,0,0);
                    enctraffic=0;
                    ReleaseMutex(mtx);
                    InsToggle();
                    return 0;
                }
            ReleaseMutex(mtx);
        }

        infile.close();
    }

    sha256_hmac_finish(&ctx256,eset->hmachash);
    sha256_free(&ctx256);

    sha256_finish(&ctxobf,eset->obfhash);
    sha256_free(&ctxobf);


    //This below is only an additional obfuscation header, constructed after main encryption proper above,
    //as a function of the ciphertext and passphrase, and is used only to obscure the true header (containing salts and IV's) via XOR'ing
    //Thus the low number of PBKDF2 rounds used here is not a crypto-security concern
    PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->obfhash,sizeof(eset->obfhash),1000,headersize,eset->obfheader);


    outfile.seekp(0,ios_base::beg);
    outfile.write((char*)eset->hmachash,sizeof(eset->hmachash));
    outfile.close();

    fstream ffile;
    ffile.rdbuf()->pubsetbuf(0,0);
    ffile.open(eset->cipherfile,ios_base::binary|ios_base::out|ios_base::in);
    ffile.read((char*)eset->oldheader,headersize);

    for (int hx=0;hx<headersize;hx++)
    {eset->oldheader[hx] ^= eset->obfheader[hx];}

    ffile.seekp(0,ios_base::beg);
    ffile.write((char*)eset->oldheader,headersize);
    ffile.close();

    eset->randdist=rng7.getNext();
    EncReady(eset);

    sureset(eset,0,sizeof(keyset));serpent_clearkey();
    VirtualUnlock(messbuff,messsize);VirtualUnlock(messbufftemp,messsize);VirtualUnlock(filetablebuff,filetablesize);
    VirtualUnlock(eset,sizeof(keyset));
    delete eset;delete[] filetablebuff;delete[] messbuff;

    if (strcmp(skippedfiles,"")!=0){Mbox(skippedfiles);}
    sureset(skippedfiles,0,MAX_PATH*(ListView_GetItemCount(OutList)+1));
    VirtualUnlock(skippedfiles,MAX_PATH*(ListView_GetItemCount(OutList)+1));
    delete[] skippedfiles;

    if (WaitForSingleObject(mtx,10000)==WAIT_OBJECT_0){enctraffic=0;ReleaseMutex(mtx);}
    InsToggle();

    return 0;
}

//Function to perform decryption of incoming cipher-file and handle content
DWORD WINAPI DecryptComm(long v)
{
    //Check for valid data
    if (!DecDetails.cformat){syme(7,"Nothing to decrypt. Please load a cipher-file first.");return 0;}
    if(!SendMessage(OutPass,WM_GETTEXTLENGTH,0,0)){syme(7,"Please enter passphrase to decrypt");SetFocus(OutPass);return 0;}

    //Check operation not already in progress
    if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
    if (dectraffic==1){dectraffic=2;ReleaseMutex(mtx2);return 0;}
    else if (dectraffic==2){ReleaseMutex(mtx2);return 0;}
    else {dectraffic=1;ReleaseMutex(mtx2);}
    commode=1;

    keyset * dset = new keyset;
    VirtualLock(dset,sizeof(keyset));
    sureset(dset,0,sizeof(keyset));

    char * messcontent = NULL;
    char * messbufftemp = NULL;
    char * filetablefull = NULL;
    IStream * newisp=NULL;


    //If source is iStream, convert to file instead
    if (DecDetails.cformat==2)
    {
        CoInitializeEx(NULL,COINIT_MULTITHREADED);
        LARGE_INTEGER LIR={};
        CoUnmarshalInterface(DecDetails.csp,IID_IStream,(LPVOID*)&newisp);
        DecDetails.csp->Seek(LIR,STREAM_SEEK_SET,0);
        LIR.QuadPart=0;newisp->Seek(LIR,STREAM_SEEK_SET,0);
        SendMessage(EncProg,PBM_SETPOS,0,0);
        ShowWindow(EncProg,SW_SHOW);
        SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,(LPSTR)dset->buffer);
        GetModuleFileNameA(NULL, (LPSTR)dset->buffer, MAX_PATH); strrchr((LPSTR)dset->buffer,'\\')[0]=0;
        safescat(dset->buffer,"\\CenoCipherTempFiles\\");
        CreateDirectory((LPSTR)dset->buffer,NULL);
        syme(3,"Processing...");
        //safescat(dset->buffer,"\\cenocipher\\");
        safescat(dset->buffer,DecDetails.cfile);
        sureset(DecDetails.cfile,0,sizeof(DecDetails.cfile));
        safescopy(DecDetails.cfile,dset->buffer);
        sureset(dset->buffer,0,sizeof(dset->buffer));
        ZeroFile(DecDetails.cfile);
        DeleteFile(DecDetails.cfile);
        ofstream outfile;
        outfile.rdbuf()->pubsetbuf(0,0);
        outfile.open(DecDetails.cfile,outfile.binary|outfile.out);
        ULONG bytesread=0;
        while ((newisp->Read(dset->buffer,sizeof(dset->buffer),&bytesread))==S_OK)
        {
            if (bytesread==0){break;}
            outfile.write((char*)dset->buffer,bytesread);
        }
        outfile.write((char*)dset->buffer,bytesread);
        outfile.close();
        DecDetails.cformat=1;
        CoUninitialize();
    } //if (DecDetails.cformat==2)


    if (DecDetails.dbuffer)
    {
        sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
        VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
        SetProcessWorkingSetSize(hProcess,defmin,defmax);
        delete[] DecDetails.dbuffer;
        DecDetails.dbuffer=NULL;
    }

    SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)ebuttonpic2);
    ListView_DeleteAllItems(OutList);

    if (DecDetails.cformat==3)
    {
        long texlen=SendMessage(OutEdit,WM_GETTEXTLENGTH,0,0);
        char* b64text = new char[texlen+1];
        char* initialb64text = b64text;
        VirtualLock(b64text,texlen+1);
        sureset(b64text,0,texlen+1);

        SendMessage(OutEdit,WM_GETTEXT,texlen+1,(LPARAM)b64text);
        long i;
        for (i=0; b64text[i]; b64text[i]=='=' ? i++ : *b64text++){};
        long flen = (texlen * 3 /4) - i;
        char* fbuff = new char[flen];
        VirtualLock(fbuff,flen);
        b64text=initialb64text;
        Base64Decode(initialb64text,fbuff);
        SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,DecDetails.cfile);
        GetModuleFileNameA(NULL, DecDetails.cfile, MAX_PATH); strrchr(DecDetails.cfile,'\\')[0]=0;
        safescat(DecDetails.cfile,"\\CenoCipherTempFiles");
        CreateDirectory(DecDetails.cfile,NULL);
        safescat(DecDetails.cfile,"\\CCB64.dat");
        ZeroFile(DecDetails.cfile);
        DeleteFile(DecDetails.cfile);
        ofstream outfile;
        outfile.rdbuf()->pubsetbuf(0,0);
        outfile.open(DecDetails.cfile,ios_base::out|ios_base::binary);
        outfile.write(fbuff,flen);
        outfile.close();
        sureset(b64text,0,texlen+1);
        sureset(fbuff,0,flen);
        VirtualUnlock(b64text,texlen+1);
        VirtualUnlock(fbuff,flen);
        delete[] b64text;
        delete[] fbuff;
        DecDetails.cformat=1;
    }


    // If source is regular file from file system
    if (DecDetails.cformat==1)
    {
        SendMessage(EncProg,PBM_SETPOS,0,0);
        ShowWindow(EncProg,SW_SHOW);
        syme(3,"Authenticating...");

        if (DecDetails.sfile[0]!='\0'){safescopy(DecDetails.cfile,DecDetails.sfile);}

        char* fsel = strrchr(DecDetails.cfile,'.')==NULL ? DecDetails.cfile : strrchr(DecDetails.cfile,'.');
        strncpy((char*)dset->buffer,fsel,5);
        for(int x=0;x<4;x++){dset->buffer[x]=tolower(dset->buffer[x]);}

        if (strcmp((char*)dset->buffer,".jpg")==0 or strcmp((char*)dset->buffer,".jpeg")==0)
        {
            SendMessage(OutPass,WM_GETTEXT,255,(LPARAM)dset->passkey);
            sha256((unsigned char*)"pseudokeystring",strlen("pseudokeystring"),dset->pseudosalt,0);
            PKCS5_PBKDF2_HMAC(dset->passkey,strlen((char*)dset->passkey),dset->pseudosalt,sizeof(dset->pseudosalt),100000,sizeof(dset->pseudokey),dset->pseudokey);

            sureset(DecDetails.dfile,0,sizeof(DecDetails.dfile));
            SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,DecDetails.dfile);
            GetModuleFileNameA(NULL, DecDetails.dfile, MAX_PATH); strrchr(DecDetails.dfile,'\\')[0]=0;
            safescat(DecDetails.dfile,"\\CenoCipherTempFiles");
            CreateDirectory(DecDetails.dfile,NULL);

            safescat(DecDetails.dfile,"\\CCDesteg.dat");
            ZeroFile(DecDetails.dfile);
            DeleteFile(DecDetails.dfile);

            safescopy(DecDetails.sfile,DecDetails.cfile);
            safescopy(DecDetails.cfile,DecDetails.dfile);
            if (cenostegextract(DecDetails.sfile,(char*)DecDetails.dfile,(char*)dset->pseudokey)==1)
            {safescopy(DecDetails.cfile,DecDetails.sfile);}

        }


        fstream infile;
        infile.rdbuf()->pubsetbuf(0,0);
        infile.open(DecDetails.cfile,ios_base::binary|ios_base::in|ios_base::out);

        if (!infile.good())
        {
            syme(7,"Error: File could not be opened for decryption");
            sureset(dset,0,sizeof(keyset));
            VirtualUnlock(dset,sizeof(keyset));
            delete[] dset;
            if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
            dectraffic=0;
            ReleaseMutex(mtx2);
            InsToggle();
            return 0;
        }

        sha256_context ctxobf;
        sha256_init(&ctxobf);
        sha256_starts(&ctxobf,0);

        infile.read((char*)dset->oldheader,headersize);
        while (infile)
        {infile.read((char*)dset->buffer,sizeof(dset->buffer));sha256_update(&ctxobf,dset->buffer,infile.gcount());}
        sha256_finish(&ctxobf,dset->obfhash);
        sha256_free(&ctxobf);

        SendMessage(OutPass,WM_GETTEXT,255,(LPARAM)dset->passkey);
        //See notes in EncryptComm section. Additional obfuscation header only, thus low PBKDF2 rounds used not a concern.
        PKCS5_PBKDF2_HMAC(dset->passkey,strlen((char*)dset->passkey),dset->obfhash,sizeof(dset->obfhash),1000,headersize,dset->obfheader);

        for (int hx=0;hx<headersize;hx++)
        {dset->oldheader[hx] ^= dset->obfheader[hx];}

        infile.clear();
        infile.seekg(headersize,ios_base::beg);

        int hcursor=0;
        memcpy(dset->hmachash,&dset->oldheader[hcursor],sizeof(dset->hmachash));hcursor+=sizeof(dset->hmachash);
        memcpy(dset->salt,&dset->oldheader[hcursor],sizeof(dset->salt));hcursor+=sizeof(dset->salt);
        memcpy(dset->iv,&dset->oldheader[hcursor],sizeof(dset->iv));hcursor+=sizeof(dset->iv);
        memcpy(dset->twofishsalt,&dset->oldheader[hcursor],sizeof(dset->twofishsalt));hcursor+=sizeof(dset->twofishsalt);
        memcpy(dset->twofishiv,&dset->oldheader[hcursor],sizeof(dset->twofishiv));hcursor+=sizeof(dset->twofishiv);
        memcpy(dset->serpentsalt,&dset->oldheader[hcursor],sizeof(dset->serpentsalt));hcursor+=sizeof(dset->serpentsalt);
        memcpy(dset->serpentiv,&dset->oldheader[hcursor],sizeof(dset->serpentiv));

        SendMessage(OutPass,WM_GETTEXT,255,(LPARAM)dset->passkey);
        PKCS5_PBKDF2_HMAC(dset->passkey,strlen((char*)dset->passkey),dset->salt,sizeof(dset->salt),prounds/3,sizeof(dset->key),dset->key);
        PKCS5_PBKDF2_HMAC(dset->passkey,strlen((char*)dset->passkey),dset->twofishsalt,sizeof(dset->twofishsalt),prounds/3,sizeof(dset->twofishkey),dset->twofishkey);
        PKCS5_PBKDF2_HMAC(dset->passkey,strlen((char*)dset->passkey),dset->serpentsalt,sizeof(dset->serpentsalt),prounds/3,sizeof(dset->serpentkey),dset->serpentkey);

        sha256_context ctxhmackey;
        sha256_init(&ctxhmackey);
        sha256_starts(&ctxhmackey,0);

        sha256_update(&ctxhmackey,dset->key,sizeof(dset->key));
        sha256_update(&ctxhmackey,dset->twofishkey,sizeof(dset->twofishkey));
        sha256_update(&ctxhmackey,dset->serpentkey,sizeof(dset->serpentkey));
        sha256_finish(&ctxhmackey,dset->hmackey);
        sha256_free(&ctxhmackey);

        sha256_context ctx256;
        sha256_init(&ctx256);
        sha256_hmac_starts(&ctx256,dset->hmackey,sizeof(dset->hmackey),0);
        sha256_hmac_update(&ctx256,dset->iv,sizeof(dset->iv));
        sha256_hmac_update(&ctx256,dset->twofishiv,sizeof(dset->twofishiv));
        sha256_hmac_update(&ctx256,dset->serpentiv,sizeof(dset->serpentiv));

        while (infile)
        {infile.read((char*)dset->buffer,sizeof(dset->buffer));sha256_hmac_update(&ctx256,dset->buffer,infile.gcount());}
        sha256_hmac_finish(&ctx256,dset->hmachashresult);
        sha256_free(&ctx256);

        if (memcmp((char*)dset->hmachashresult,(char*)dset->hmachash,32)!=0)
        {
             syme(2,"Authenticaction failed. Passphrase incorrect or file invalid/corrupt.");
            infile.close();
            sureset(dset,0,sizeof(keyset));
            VirtualUnlock(dset,sizeof(keyset));
            delete[] dset;
            SetFocus(OutPass);
            SendMessage(OutPass,EM_SETSEL,0,999);
            if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
            dectraffic=0;
            ReleaseMutex(mtx2);
            InsToggle();
            return 0;
        }

        SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,215));

        Twofish_initialise();
        Twofish_prepare_key(dset->twofishkey,32,&dset->tkey);
        aes_setkey_enc(&dset->aes, dset->key, 256 );
        set_key((u4byte*)dset->serpentkey,256); //serpent key

        infile.clear();
        infile.seekg(0,infile.end);
        DecDetails.dbuffsize=(long long)infile.tellg()-(long long)headersize;
        infile.seekg(headersize,infile.beg);
        long long totalwritten=0;
        long long allfilesize=DecDetails.dbuffsize;

        syme(3,"Decrypting...");
        char * sizecheck = new (std::nothrow) char[DecDetails.dbuffsize * 2];
        delete[] sizecheck;

        // Regular File Small enough to fit in memory
        if (sizecheck!=NULL and DecDetails.dbuffsize<=2000000000)
        {
            DecDetails.dbuffer = new (std::nothrow) unsigned char[DecDetails.dbuffsize];
            SetProcessWorkingSetSize(hProcess,defmin+DecDetails.dbuffsize,defmax+DecDetails.dbuffsize);
            VirtualLock(DecDetails.dbuffer,DecDetails.dbuffsize);
            unsigned char * BuffCursor=DecDetails.dbuffer;

            while (!infile.eof())
            {
                infile.read((char*)dset->buffer,sizeof(dset->buffer));
                aes_crypt_ctr(&dset->aes,infile.gcount(),&dset->ANCOffset,dset->iv,dset->astreamblock,dset->buffer,dset->buffer);
                Twofish_CTR_Encrypt(&dset->tkey,infile.gcount(),&dset->TNCOffset,dset->twofishiv,dset->tstreamblock,dset->buffer,dset->buffer);
                Serpent_CTR_Encrypt(infile.gcount(),&dset->SNCOffset,dset->serpentiv,dset->sstreamblock,dset->buffer,dset->buffer);
                memcpy(BuffCursor,dset->buffer,infile.gcount());
                BuffCursor+=infile.gcount();
                totalwritten+=infile.gcount();
                double t=(((double)totalwritten/(double)allfilesize)*100);
                int p = (int)t;
                SendMessage(EncProg,PBM_SETPOS,p,0);
                if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){continue;}
                if (dectraffic==2)
                {
                    infile.close();
                    syme(2,"Decryption canceled.");
                    sureset(dset,0,sizeof(keyset));serpent_clearkey();
                    sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
                    VirtualUnlock(dset,sizeof(keyset));
                    VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
                    delete[] dset;
                    delete[] DecDetails.dbuffer;
                    DecDetails.dbuffer=NULL;
                    SetProcessWorkingSetSize(hProcess,defmin,defmax);
                    dectraffic=0;
                    ReleaseMutex(mtx2);
                    InsToggle();
                    return 0;
                }
                ReleaseMutex(mtx2);
            }
            sureset(dset,0,sizeof(keyset));

            infile.close();
            syme(1,"Decryption complete.");
            SendMessage(EncProg,PBM_SETPOS,100,0);
            ShowWindow(EncProg,SW_HIDE);
            SendMessage(OutEdit, WM_SETREDRAW, 0, 0);
            messbufftemp = new char[strlen((char*)DecDetails.dbuffer)];
            VirtualLock(messbufftemp,strlen((char*)DecDetails.dbuffer));
            sureset(messbufftemp,0,strlen((char*)DecDetails.dbuffer));
            strncpy(messbufftemp,strchr((char*)DecDetails.dbuffer,255)+1,strlen((char*)DecDetails.dbuffer));
            strncat(messbufftemp,(char*)DecDetails.dbuffer,(long)strchr((char*)DecDetails.dbuffer,255)-(long)DecDetails.dbuffer);
            SendMessage(OutEdit,WM_SETFONT,(WPARAM)segoe16,true);
            SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)messbufftemp);

            CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_SIZE;cfor.yHeight=16*fontratio*20;
            SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
            SendMessage(OutEdit, WM_SETREDRAW, -1, 0);
            InvalidateRect(OutEdit,0,0);

            for (unsigned char* x=DecDetails.dbuffer;x<DecDetails.dbuffer+DecDetails.dbuffsize-strlen(filetableheader);x+=1)
            {
                if (memcmp(x,filetableheader,strlen(filetableheader))==0)
                {
                    filetablefull = new char[strlen((char*)x)+1];
                    VirtualLock(filetablefull,strlen((char*)x)+1);
                    sureset(filetablefull,0,strlen((char*)x)+1);
                    strncpy(filetablefull,(char*)x,strlen((char*)x));
                    break;
                }
            }

            commode=2;
            if (DecDetails.dformat!=3){DecDetails.dformat=1;}


        }
        else
        {   //Regular file too large for memory
            sureset(DecDetails.dfile,0,sizeof(DecDetails.dfile));
            SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,DecDetails.dfile);
            GetModuleFileNameA(NULL, DecDetails.dfile, MAX_PATH); strrchr(DecDetails.dfile,'\\')[0]=0;
            safescat(DecDetails.dfile,"\\CenoCipherTempFiles");
            CreateDirectory(DecDetails.dfile,NULL);

            //safescat(DecDetails.dfile,"\\cenocipher");

            if (SendMessage(OptPrompt,BM_GETCHECK,0,0))
            {
                promptresult=-1;
                if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
                dectraffic=5;
                ReleaseMutex(mtx2);
                ShowPrompt(1);

                while (1)
                {
                    if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
                    if (dectraffic==7){SendMessage(PromptSpecFolder,WM_GETTEXT,sizeof(DecDetails.dfile),(LPARAM)DecDetails.dfile);}
                    if (dectraffic!=5){ReleaseMutex(mtx2);break;}
                    ReleaseMutex(mtx2);
                    Sleep(100);
                }
            }

            if (SendMessage(OptUseSpecified,BM_GETCHECK,0,0)){SendMessage(SpecFolder,WM_GETTEXT,sizeof(DecDetails.dfile),(LPARAM)DecDetails.dfile);}

            if (strlen(DecDetails.dfile)>1){safescat(DecDetails.dfile,"\\CCDecrypt.dat");}
            else {DecDetails.dfile[0]=27;}


            ZeroFile(DecDetails.dfile);
            DeleteFile(DecDetails.dfile);
            ofstream outfile;
            outfile.rdbuf()->pubsetbuf(0,0);
            outfile.open(DecDetails.dfile,outfile.binary|outfile.out);

            if (!outfile.good())
            {
                if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
                dectraffic=3;
                ReleaseMutex(mtx2);
            }

            while (infile)
            {
                infile.read((char*)dset->buffer,sizeof(dset->buffer));
                aes_crypt_ctr(&dset->aes,infile.gcount(),&dset->ANCOffset,dset->iv,dset->astreamblock,dset->buffer,dset->buffer);
                Twofish_CTR_Encrypt(&dset->tkey,infile.gcount(),&dset->TNCOffset,dset->twofishiv,dset->tstreamblock,dset->buffer,dset->buffer);
                Serpent_CTR_Encrypt(infile.gcount(),&dset->SNCOffset,dset->serpentiv,dset->sstreamblock,dset->buffer,dset->buffer);
                outfile.write((char*)dset->buffer,infile.gcount());
                totalwritten+=infile.gcount();
                double t=(((double)totalwritten/(double)allfilesize)*100);
                int p = (int)t;
                SendMessage(EncProg,PBM_SETPOS,p,0);
                if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){continue;}
                if (dectraffic==2 or dectraffic==3)
                    {
                        infile.close();outfile.close();
                        ShowWindow(EncProg,SW_HIDE);
                        if (dectraffic==2){syme(2,"Decryption canceled.");}
                        if (dectraffic==3){syme(7,"Decryption aborted: Unable to write to specified location.",(SendMessage(OptUseSpecified,BM_GETCHECK,0,0) ? SpecFolder : PromptSpecFolder));}
                        sureset(dset,0,sizeof(keyset));serpent_clearkey();
                        VirtualUnlock(dset,sizeof(keyset));
                        delete[] dset;
                        ZeroFile((char*)DecDetails.dfile);
                        DeleteFile(DecDetails.dfile);
                        dectraffic=0;
                        ReleaseMutex(mtx2);
                        InsToggle();
                        return 0;
                    }
                ReleaseMutex(mtx2);
            }
            sureset(dset,0,sizeof(keyset));

            infile.close();
            outfile.close();
            ifstream myfile;
            myfile.rdbuf()->pubsetbuf(0,0);
            myfile.open(DecDetails.dfile);
            long messlength=0;
            while (myfile.get()!=0){messlength++;}
            messlength++;
            myfile.seekg(0);

            messcontent = new char[messlength+1];
            VirtualLock(messcontent,messlength+1);
            sureset(messcontent,0,messlength+1);
            myfile.read(messcontent,messlength);

            messbufftemp = new char[messlength+2];
            VirtualLock(messbufftemp,messlength+2);
            sureset(messbufftemp,0,messlength+2);
            strncpy(messbufftemp,strchr(messcontent,255)+1,strlen(messcontent));
            strncat(messbufftemp,messcontent,(long)strchr(messcontent,255)-(long)messcontent);

            long long cursorpos=myfile.tellg();
            sureset(dset->buffer,0,sizeof(dset->buffer));
            cursorpos--;

            while (memcmp(dset->buffer,filetableheader,strlen(filetableheader))!=0 and myfile.good())
            {
                cursorpos++;
                myfile.seekg(cursorpos);
                myfile.read((char*)dset->buffer,strlen(filetableheader));
            }

            myfile.seekg(cursorpos);
            long ftlength=1;
            while (myfile.get()!=0 and myfile.good()){ftlength++;}

            ftlength++;
            myfile.seekg(cursorpos);
            filetablefull = new char[ftlength+1];
            VirtualLock(filetablefull,ftlength+1);
            sureset(filetablefull,0,ftlength+1);
            myfile.read(filetablefull,ftlength);

            syme(1,"Decryption complete.");
            ShowWindow(EncProg,SW_HIDE);
            SendMessage(OutEdit, WM_SETREDRAW, 0, 0);
            SendMessage(OutEdit,WM_SETFONT,(WPARAM)segoe16,true);
            SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)messbufftemp);
            CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_SIZE;cfor.yHeight=16*fontratio*20;
            SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
            SendMessage(OutEdit, WM_SETREDRAW, -1, 0);
            InvalidateRect(OutEdit,0,0);

            sureset(messcontent,0,messlength);
            myfile.close();
            DecDetails.dformat=2;
            commode=2;

        }

    } // cformat ==1 (regular file)


sureset(dset->buffer,0,sizeof(dset->buffer));
char* startpos=filetablefull;
do
{
    char* startpipe=strchr(startpos,'|');
    if (startpipe==NULL){break;}
    char* c=strchr(startpos,':');
    char* a=strchr(startpos,'*');
    char* endpipe=strchr(startpipe+1,'|');
    if (endpipe==NULL){endpipe=strchr(startpipe+1,'\0');}
    startpos=endpipe;
    LVITEM lvi={};
    lvi.iItem=ListView_GetItemCount(OutList)+2;
    int ni=ListView_InsertItem(OutList,&lvi);
    sureset(dset->buffer,0,sizeof(dset->buffer));
    strncpy((char*)dset->buffer,startpipe+1,c-startpipe-1);
    ListView_SetItemText(OutList,ni,0,(LPSTR)dset->buffer);
    sureset(dset->buffer,0,sizeof(dset->buffer));
    strncpy((char*)dset->buffer,c+1,a-c-1);
    ListView_SetItemText(OutList,ni,1,(LPSTR)dset->buffer);
    sureset(dset->buffer,0,sizeof(dset->buffer));
    strncpy((char*)dset->buffer,a+1,endpipe-a-1);
    ListView_SetItemText(OutList,ni,2,(LPSTR)dset->buffer);


} while (*startpos!='\0');
Addicons(OutList);

//Clean up
sureset(dset,0,sizeof(keyset));
serpent_clearkey();
long ftlength=strlen(filetablefull);
long mbtlength=strlen(messbufftemp);
sureset(messbufftemp,0,strlen(messbufftemp));
sureset(filetablefull,0,strlen(filetablefull));
if (messcontent)
{
    long messlength=strlen(messcontent);
    sureset(messcontent,0,strlen(messcontent));
    VirtualUnlock(messcontent,messlength);
    delete[] messcontent;
}
VirtualUnlock(messbufftemp,mbtlength);
VirtualUnlock(filetablefull,ftlength);
VirtualUnlock(dset,sizeof(keyset));
delete[] dset;
delete[] messbufftemp;
delete[] filetablefull;

if (DecDetails.dbuffer)
{VirtualLock(DecDetails.dbuffer,DecDetails.dbuffsize);}

if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return 0;}
dectraffic=0;
ReleaseMutex(mtx2);

if (DecDetails.dformat==3){OpenInListFiles(2);commode=0;}
InsToggle();
return 0;
}




//Function to quickly fill a string with leading zeros
string Padleft(string s, unsigned int num, char c)
{
    if (s.length()<num)
    {s=string(num-s.length(),c)+s;}
    return s;
}


//Function to open or save received file attachments
void OpenInListFiles(int mode)
{
if (CheckStatus()){return;}
if (ListView_GetItemCount(OutList)<1){return;}

char datapath[MAX_PATH*2+1]={};
VirtualLock(datapath,sizeof(datapath));
SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,(LPSTR)datapath);
GetModuleFileNameA(NULL, (LPSTR)datapath, MAX_PATH); strrchr((LPSTR)datapath,'\\')[0]=0;
safescat(datapath,"\\CenoCipherTempFiles\\");
CreateDirectory((LPSTR)datapath,NULL);

//safescat(datapath,"\\cenocipher\\");

char itex[MAX_PATH]={};
VirtualLock(itex,sizeof(itex));
int newcount=0;

for (int x=0;x<ListView_GetItemCount(OutList);x++)
{
    ListView_GetItemText(OutList,x,3,itex,MAX_PATH);
    if (ListView_GetItemState(OutList,x,LVIS_SELECTED)==LVIS_SELECTED and itex[0]=='\0'){newcount++;}
    if (ListView_GetSelectedCount(OutList)<1 and itex[0]=='\0'){newcount++;}
}


if ((mode==0 or mode==2) and newcount>0 and DecDetails.dformat!=3)
{
    if (SendMessage(OptPrompt,BM_GETCHECK,0,0))
    {
        if (promptresult==0 and mode==0){ShowPrompt(2);return;}
        if (promptresult==0 and mode==2){ShowPrompt(3);return;}
        promptresult=0;
        if (SendMessage(PromptUseSpecified,BM_GETCHECK,0,0))
        {SendMessage(PromptSpecFolder,WM_GETTEXT,sizeof(datapath),(LPARAM)datapath);safescat(datapath,"\\");}
        if(strlen(datapath)<2){safescopy(datapath,"\x1b");}
    }
}

if (SendMessage(OptUseSpecified,BM_GETCHECK,0,0))
{
    {SendMessage(SpecFolder,WM_GETTEXT,sizeof(datapath),(LPARAM)datapath);safescat(datapath,"\\");}
    if(strlen(datapath)<2){safescopy(datapath,"\x1b");}
}


if (mode==1)
    {
        FolderOutputRequestor();
        if (strlen(SaveFilePath)<1){return;}
        safescopy(datapath,SaveFilePath);
    }

    long n = strlen(datapath);


 for (int x=0;x<ListView_GetItemCount(OutList);x++)
    {
        if (ListView_GetItemState(OutList,x,LVIS_SELECTED)==LVIS_SELECTED or ListView_GetSelectedCount(OutList)<1)
        {
            ListView_GetItemText(OutList,x,0,itex,MAX_PATH);
            datapath[n]='\0';
            safescat(datapath,itex);
            ListView_GetItemText(OutList,x,1,itex,MAX_PATH);
            string ilocation=itex;
            ListView_GetItemText(OutList,x,2,itex,MAX_PATH);
            string ilength=itex;
            long long iloc=strtoll(ilocation.c_str(),NULL,10);
            long long ilen=strtoll(ilength.c_str(),NULL,10);
            ListView_GetItemText(OutList,x,3,itex,MAX_PATH);

            if (strlen(itex)==0)
            {ZeroFile(datapath);DeleteFile(datapath);}
            ifstream testfile(datapath);
            if (!testfile.is_open()){itex[0]='\0';}
            testfile.close();

            if ((DecDetails.dformat==1 or DecDetails.dformat==3) and (strlen(itex)==0 or mode==1))
            {
                ofstream outfile;
                outfile.rdbuf()->pubsetbuf(0,0);
                outfile.open(datapath,ios_base::binary|ios_base::out);
                if (!outfile.good())
                {
                    if (mode==2){ListView_DeleteAllItems(OutList);InvalidateRect(OutList,NULL,true);UpdateWindow(OutList);}
                    syme(7,"File open/save failed: Error writing to specified location",(SendMessage(OptUseSpecified,BM_GETCHECK,0,0) ? SpecFolder : PromptSpecFolder));
                    return;
                }
                outfile.write((char*)DecDetails.dbuffer+iloc,ilen);
                outfile.close();
            }

            else if (DecDetails.dformat==2 and (strlen(itex)==0 or mode==1))
            {
                char buffer[segmentsize]={};
                VirtualLock(buffer,sizeof(buffer));

                ofstream outfile;
                outfile.rdbuf()->pubsetbuf(0,0);
                outfile.open(datapath,ios_base::binary|ios_base::out);
                if (!outfile.good())
                {
                    if (mode==2){ListView_DeleteAllItems(OutList);InvalidateRect(OutList,NULL,true);UpdateWindow(OutList);}
                    syme(7,"File open/save failed: Error writing to specified location",(SendMessage(OptUseSpecified,BM_GETCHECK,0,0) ? SpecFolder : PromptSpecFolder));
                    return;
                }
                ifstream infile;
                infile.rdbuf()->pubsetbuf(0,0);
                infile.open(DecDetails.dfile,ios_base::binary|ios_base::in);
                if (!infile.good())
                {
                    syme(7,"Error reading from source");
                    infile.close();
                    outfile.close();
                    sureset(buffer,0,sizeof(buffer));
                    sureset(itex,0,sizeof(itex));
                    sureset(datapath,0,sizeof(datapath));
                    VirtualUnlock(buffer,sizeof(buffer));
                    VirtualUnlock(datapath,sizeof(datapath));
                    VirtualUnlock(itex,sizeof(itex));
                    return;
                }
                infile.seekg(iloc);

                long long totalread=0;

                while (totalread < ilen)
                {
                    infile.read(buffer,sizeof(buffer) < ilen-totalread ? sizeof(buffer) : ilen-totalread );
                    totalread+=infile.gcount();
                    outfile.write(buffer,infile.gcount());
                }
                infile.close();
                outfile.close();
                sureset(buffer,0,sizeof(buffer));
                VirtualUnlock(buffer,sizeof(buffer));

            }


            if (mode==0)
            {
                ShellExecute(NULL,"open",datapath,"","",SW_SHOW);
                ListView_SetItemText(OutList,x,3,(char*)"z");
            }


            if (mode==2)
            {ListView_SetItemText(OutList,x,1,(LPSTR)datapath);}

            sureset(itex,0,sizeof(itex));
            VirtualUnlock(itex,sizeof(itex));
       }

    }

    if (mode==2)
    {
        for (int x=0;x<ListView_GetItemCount(OutList);x++)
        {
            if (ListView_GetItemState(OutList,x,LVIS_SELECTED)!=LVIS_SELECTED and ListView_GetSelectedCount(OutList)>0)
            {ListView_DeleteItem(OutList,x);}
        }
    }


    sureset(datapath,0,sizeof(datapath));
    sureset(itex,0,sizeof(datapath));
    VirtualUnlock(datapath,sizeof(datapath));
    VirtualUnlock(itex,sizeof(datapath));


}



//Function to facilitate replying to messages
void ReplyWithQuote()
{
     if (CheckStatus()){return;}
     if (commode!=2){return;}
     commode=0;
     InsToggle();
     syme();
     ShowWindow(CFileHolder,SW_HIDE);

     if(SendMessage(WithQuote,BM_GETCHECK,0,0))
      {
            SendMessage(OutEdit,EM_SETSEL,0,0);
            CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_FACE;strcpy(cfor.szFaceName,"Segoe UI");
            SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)&cfor);
            SendMessage(OutEdit,EM_REPLACESEL,false,(LPARAM)"\n\n\n======================================================\n");
            SendMessage(OutEdit,EM_SETSEL,0,0);
      }
      else
      {
          SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"");
          CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_FACE;strcpy(cfor.szFaceName,"Segoe UI");
          SendMessage(OutEdit,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
      }

     InvalidateRect(OutEdit,NULL,true);UpdateWindow(OutEdit);
     SetFocus(OutEdit);
     if(SendMessage(IncludeFiles,BM_GETCHECK,0,0))
     {OpenInListFiles(2);} else {ListView_DeleteAllItems(OutList);}

}

//Function to create and/or show Help window pop-up
void ShowHelp()
{
    if (HelpWindow){ShowWindow(HelpWindow,SW_SHOW);SetFocus(HelpEdit1);HideCaret(HelpEdit1);return;}
    HelpWindow = CreateWindowEx (0,szClassName,NULL,WS_OVERLAPPED|WS_SYSMENU,0 hh,980 vv,1000 hh,800 vv,hwndmain,NULL,hti,NULL);
    RECT hrec;
    GetClientRect(HelpWindow,&hrec);
    HelpEdit1 = CreateWindowEx(0,RICHEDIT_CLASS,"",ES_MULTILINE | WS_CHILD | WS_TABSTOP | WS_VSCROLL | ES_NOHIDESEL | ES_AUTOVSCROLL| ES_READONLY,
    0 hh,0 vv,hrec.right-hrec.left,hrec.bottom-hrec.top,HelpWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    ShowWindow(HelpEdit1,SW_SHOW);
    SendMessage(HelpEdit1,EM_SETBKGNDCOLOR,0,beigecolor);
    SetWindowLong(HelpEdit1, GWL_EXSTYLE, GetWindowLong(HelpEdit1,GWL_EXSTYLE)&~WS_EX_CLIENTEDGE );
    SetWindowLong(HelpEdit1, GWL_STYLE, GetWindowLong(HelpEdit1,GWL_STYLE)&~WS_TABSTOP );
    SendMessage(HelpEdit1,EM_SETEVENTMASK,0,ENM_LINK);
    SendMessage(HelpEdit1, EM_AUTOURLDETECT, true, 0);
    dproc=SetWindowLong(HelpEdit1,GWL_WNDPROC,(long)EditorStaticProc);
    SetWindowLong(HelpWindow,GWL_WNDPROC,(long)WindowProcedureH);
    ShowWindow(HelpWindow,SW_SHOW);

    SendMessage(HelpEdit1,WM_SETFONT,(WPARAM)arial14,true);
    SendMessage(HelpEdit1,EM_SETMARGINS,EC_LEFTMARGIN|EC_RIGHTMARGIN,MAKEWORD(50,50));

    SendMessage(HelpEdit1,EM_GETRECT,0,(LPARAM)&hrec);
    hrec.left=20;hrec.top=20;hrec.right-=20;hrec.bottom-=10;
    SendMessage(HelpEdit1,EM_SETRECT,0,(LPARAM)&hrec);
    HRSRC htext1 = FindResource( NULL, "HELPFILE3", RT_RCDATA );
    HGLOBAL ghtext1 = LoadResource( NULL, htext1 );
    const char* chtext1 = (char*)LockResource( ghtext1 );

    SendMessage(HelpEdit1,WM_SETTEXT,0,(LPARAM)chtext1);
    SendMessage(HelpEdit1, WM_SETREDRAW, 0, 0);
    CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_SIZE;cfor.yHeight=12*fontratio*20;
    SendMessage(HelpEdit1,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
    SendMessage(HelpEdit1, WM_SETREDRAW, -1, 0);
    InvalidateRect(HelpEdit1,0,0);

    ShowWindow(HelpWindow,SW_SHOW);
    MoveWindow(HelpWindow,340 hh,94 vv,1000 hh,800 vv,true);
    SetFocus(HelpEdit1);

}


void ShowPrompt(int mode)
{
    if (mode!=0){promptver=mode;}

    if (PromptWindow)
    {
        if (mode==0)
        {
            SetForegroundWindow(hwndmain);
            EnableWindow(hwndmain,true);
            ShowWindow(PromptWindow,SW_HIDE);
            return;
        }

        MoveWindow(PromptWindow,-2590 hh,294 vv,500 hh,400 vv,true);
        ShowWindow(PromptWindow,SW_SHOW);
        if (mode==1){SendMessage(promptmess,WM_SETTEXT,0,(LPARAM)"The currently loaded data is too large to be decrypted in memory, and will need to be decrypted to disk instead.\n\nHow would you like to proceed?");}
        if (mode==2){SendMessage(promptmess,WM_SETTEXT,0,(LPARAM)"The selected file(s) will have to be written to disk in plain unencrypted form to continue with opening.\n\n How would you like to proceed?");}
        if (mode==3){SendMessage(promptmess,WM_SETTEXT,0,(LPARAM)"File(s) selected for re-inclusion will have to be written to disk in plain unencrypted form first.\n\n How would you like to proceed?");}
        InvalidateRect(promptmess,NULL,true);
        UpdateWindow(promptmess);
        MoveWindow(PromptWindow,590 hh,294 vv,500 hh,400 vv,true);
        EnableWindow(hwndmain,false);
        return;
    }

    PromptWindow = CreateWindowEx (0,szClassName,NULL,WS_OVERLAPPED,0 hh,980 vv,1 hh,1 vv,hwndmain,NULL,hti,NULL);
    SetWindowLong(PromptWindow,GWL_WNDPROC,(long)WindowProcedureH);
    SetWindowText(PromptWindow,"Disk-write needed");

    promptmess=CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE,30 hh,30 vv,440 hh,200 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(promptmess,WM_SETFONT,(WPARAM)arial14,true);

    PromptAbort = CreateWindow("BUTTON","Abort",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,30 hh,140 vv,220 hh,20 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptAbort,WM_SETFONT,(WPARAM)arial12,true);
    SendMessage(PromptAbort,BM_SETCHECK,BST_CHECKED,0);

    PromptUseDefault = CreateWindow("BUTTON","Use efault location",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,30 hh,170 vv,220 hh,20 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptUseDefault,WM_SETFONT,(WPARAM)arial12,true);

    PromptUseSpecified = CreateWindow("BUTTON","Use specified location:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,30 hh,210 vv,220 hh,20 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptUseSpecified,WM_SETFONT,(WPARAM)arial12,true);

    PromptSpecFolder=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","",WS_CHILD | WS_VISIBLE|WS_TABSTOP,50 hh,232 vv,350 hh,30 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptSpecFolder,WM_SETFONT,(WPARAM)arial12,true);

    PromptChooseSpecFolder= CreateWindow("BUTTON","Choose",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,310 hh,205 vv,90 hh,25 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptChooseSpecFolder,WM_SETFONT,(WPARAM)arial12,true);

    PromptOkay= CreateWindow("BUTTON","OK",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,50 hh,305 vv,90 hh,40 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptOkay,WM_SETFONT,(WPARAM)arial12,true);

    PromptCancel= CreateWindow("BUTTON","Cancel",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,310 hh,305 vv,90 hh,40 vv,PromptWindow,NULL,(HINSTANCE)GetWindowLong(hwndmain, GWL_HINSTANCE),NULL);
    SendMessage(PromptCancel,WM_SETFONT,(WPARAM)arial12,true);

    MoveWindow(PromptWindow,590 hh,294 vv,500 hh,400 vv,true);

}




//Function to control read-only type editor controls further
LRESULT CALLBACK EditorStaticProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{


    switch (message)
    {

    case WM_KEYDOWN:
        if (wParam==27)
        {return 0;}
        HideCaret(hwnd);
        break;


    case WM_LBUTTONDOWN:
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONDBLCLK:
    case WM_KEYUP:
    {HideCaret(hwnd);break;}


    default:

        LRESULT res = CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);
        HideCaret(hwnd);
        return res;
    }

        LRESULT res = CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);
        HideCaret(hwnd);
        return res;

}



//Function to memory-set an address buffer in a way that won't be optimized out by compiler
static void sureset( void *v, unsigned char u, size_t n )
{
    volatile unsigned char *p = (volatile unsigned char *)v;
    while( n-- )
    {*p++ = u;}
}


//Function to create and resize bitmaps from images already loaded
inline void MakeBitmap(LPCTSTR resId, HBITMAP* h, int width, int height)
{
    HBITMAP hbm = LoadPictResource(resId, RT_HTML);
    Bitmap * bmp = Bitmap::FromHBITMAP(hbm,NULL);
    Bitmap* newBitmap = new Gdiplus::Bitmap((int)width, (int)height);
    {
    Graphics graphics(newBitmap);
    graphics.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
    graphics.DrawImage(bmp, 0, 0, (INT)width, (INT)height);
    }
    newBitmap->GetHBITMAP(RGB(0,0,0),h);
    delete newBitmap;
}

void ClearDecDetails()
{
    if (DecDetails.csp){DecDetails.csp->Release();DecDetails.csp=NULL;}
    if (DecDetails.cstgmed){ReleaseStgMedium(DecDetails.cstgmed);}
    if (DecDetails.dbuffer)
    {
        sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
        VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
        SetProcessWorkingSetSize(hProcess,defmin,defmax);
        delete[] DecDetails.dbuffer;
        DecDetails.dbuffer=NULL;
    }
    sureset(&DecDetails,0,sizeof(DecDetails));

}



void ClipLoader()
{
    if (CheckStatus()){return;}

    ClearDecDetails();

    UINT cf_content = RegisterClipboardFormat("FileContents");
    UINT cf_descrip = RegisterClipboardFormat("FileGroupDescriptor");

    IDataObject* pDataObject;
    OleGetClipboard(&pDataObject);

    STGMEDIUM * stgmed = new STGMEDIUM;
    FORMATETC fmtetc = { (CLIPFORMAT)cf_descrip, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
    ZeroMemory(stgmed,sizeof(STGMEDIUM));

	if(pDataObject->GetData(&fmtetc,stgmed) == S_OK) //Outlook/Thunderbird
	{
        FILEGROUPDESCRIPTOR * fgdbuffer;
        fgdbuffer=(FILEGROUPDESCRIPTOR*)GlobalLock(stgmed->hGlobal);
        safescopy(DecDetails.cfile,fgdbuffer->fgd[0].cFileName);
        GlobalUnlock(stgmed->hGlobal);

        fmtetc.cfFormat=(CLIPFORMAT)cf_content;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=0;fmtetc.tymed=TYMED_ISTREAM;

        if(pDataObject->GetData(&fmtetc,stgmed) != S_OK)
        {
            fmtetc.cfFormat=(CLIPFORMAT)cf_content;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=0;fmtetc.tymed=TYMED_HGLOBAL;
            if (pDataObject->GetData(&fmtetc,stgmed) !=S_OK)
            {
                syme(7,"Note: From desktop email client, copy file attachment instead of entire email message.");
                return;
            }
        }


        DecDetails.cformat=2;
        DecDetails.cstgmed=stgmed;

        IStream * idt;
        stgmed->pstm->QueryInterface(IID_IStream,(LPVOID*)&idt);
        CoMarshalInterThreadInterfaceInStream(IID_IStream,idt,&DecDetails.csp);

        commode=1;
        InsToggle();
        ListView_DeleteAllItems(OutList);
        ContAck(1,3,DecDetails.cfile);
        SetFocus(OutPass);
        return;

	}
	 else  //Regular file paste
    {
        fmtetc.cfFormat=CF_HDROP;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=-1;fmtetc.tymed=TYMED_HGLOBAL;
        if (pDataObject->GetData(&fmtetc,stgmed) == S_OK)
        {
            DragQueryFile((HDROP)stgmed->hGlobal,0,DecDetails.cfile,sizeof(DecDetails.cfile));
            SetFocus(OutPass);
            SendMessage(OutPass,EM_SETSEL,0,999);
            ListView_DeleteAllItems(OutList);
            ListView_SetBkColor(OutList,RGB(215,215,215));
            ListView_SetTextBkColor(OutList,RGB(215,215,215));
            DecDetails.cformat=1;
            commode=1;
            InsToggle();
            ContAck(1,3,strrchr(DecDetails.cfile,'\\')+1);
            return;
        }
    }
    ReleaseStgMedium(stgmed);

    OpenClipboard(0);
    HANDLE hData = GetClipboardData(CF_TEXT);
    if (hData)
    {
        char * b64text = (char*)(GlobalLock(hData));
        CloseClipboard();
        while (b64text[0]==' ' or b64text[0]==10 or b64text[0]==13){b64text++;}
        if (strchr(b64text,' ')!=NULL){strchr(b64text,' ')[0]='\0';}
        if (strchr(b64text,10)!=NULL){strchr(b64text,10)[0]='\0';}
        if (strchr(b64text,13)!=NULL){strchr(b64text,13)[0]='\0';}
        long texlen=strlen(b64text);
        long i; char* bp;
        for (i=0,bp=b64text; bp[i]; bp[i]=='=' ? i++ : *bp++){};
        long flen = (texlen * 3 /4) - i;
        long blen = (texlen * 3 /4) +4;
        char* fbuff = new char[blen];
        VirtualLock(fbuff,blen);
        Base64Decode(b64text,fbuff);
        GlobalUnlock(hData);
        SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,DecDetails.cfile);
        GetModuleFileNameA(NULL, DecDetails.cfile, MAX_PATH); strrchr(DecDetails.cfile,'\\')[0]=0;
        safescat(DecDetails.cfile,"\\CenoCipherTempFiles");
        CreateDirectory(DecDetails.cfile,NULL);

        safescat(DecDetails.cfile,"\\CCB64.dat");
        ZeroFile(DecDetails.cfile);
        DeleteFile(DecDetails.cfile);
        ofstream outfile;
        outfile.rdbuf()->pubsetbuf(0,0);
        outfile.open(DecDetails.cfile,ios_base::out|ios_base::binary);
        outfile.write(fbuff,flen);
        outfile.close();
        sureset(fbuff,0,blen);
        VirtualUnlock(fbuff,blen);
        ListView_DeleteAllItems(OutList);
        DecDetails.cformat=1;
        commode=1;
        InsToggle();
        ContAck(2,3,0);
        SetFocus(OutPass);
        SendMessage(OutPass,EM_SETSEL,0,999);
    }
    else
    {
        syme(7,"Error loading requested data");
    }

}


void syme(int mode, const char* text, HWND flash)
{
    CHARFORMAT cfor;
    LPARAM bcol;
    COLORREF fcol;

    SendMessage(SysMessage,EM_GETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
    cfor.cbSize = sizeof(cfor);
    cfor.dwMask = CFM_COLOR;
    cfor.dwEffects = 0;


    if (mode==0){bcol=RGB(100,100,100);fcol=RGB(255,255,255);}
    if (mode==1){bcol=RGB(0,150,0);fcol=RGB(255,255,255);}
    if (mode==2){bcol=RGB(200,0,0);fcol=RGB(255,255,0);ShowWindow(EncProg,SW_HIDE);}
    if (mode==3){bcol=RGB(120,150,120);fcol=RGB(255,255,255);}
    if (mode==4){bcol=RGB(50,50,255);fcol=RGB(255,255,255);}
    if (mode==5){bcol=RGB(255,255,0);fcol=RGB(200,0,0);}
    if (mode==6){bcol=RGB(200,0,0);fcol=RGB(255,255,0);}
    if (mode==7){bcol=RGB(200,0,0);fcol=RGB(255,255,0);}



    if (mode==7)
    {
        syme(5,text,flash);
        Sleep(100);syme(6,text,flash);
        Sleep(100);syme(5,text,flash);
        Sleep(100);syme(6,text,flash);
        if (flash!=0){Sleep(1000);SendMessage(flash,EM_SETBKGNDCOLOR,0,RGB(255,255,255));InvalidateRect(flash,NULL,true);UpdateWindow(flash);flash=0;}
    }



    cfor.crTextColor=fcol;
    SendMessage(SysMessage,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
    SendMessage(SysMessage,EM_SETBKGNDCOLOR,0,bcol);
    SendMessage(SysMessage,WM_SETTEXT,0,(LPARAM)text);
    InvalidateRect(SysMessage,NULL,true);UpdateWindow(SysMessage);

    if (flash!=0){SendMessage(flash,EM_SETBKGNDCOLOR,0,RGB(255,255,0));InvalidateRect(flash,NULL,true);UpdateWindow(flash);}
}



void Addicons(HWND hwnd)
{
    char tex[1000]={};
    VirtualLock(tex,sizeof(tex));
    for (int x=0;x<ListView_GetItemCount(hwnd);x++)
    {
        LVITEM lvi={};
        ListView_GetItemText(hwnd,x,0,(LPTSTR)tex,MAX_PATH);
        SHFILEINFO shinfo={};
        SHGetFileInfo((LPCSTR)strrchr(tex,'.'),FILE_ATTRIBUTE_NORMAL,&shinfo,sizeof(SHFILEINFO),SHGFI_SYSICONINDEX|SHGFI_SMALLICON|SHGFI_USEFILEATTRIBUTES|SHGFI_ICON|SHGFI_TYPENAME|SHGFI_DISPLAYNAME);
        HIMAGELIST lvil=ListView_GetImageList(hwnd,LVSIL_SMALL);
        int lindex=ImageList_AddIcon(lvil,shinfo.hIcon);
        lvi.mask=LVIF_IMAGE;
        lvi.iImage=lindex;
        lvi.iItem=x;
        ListView_SetItem(hwnd,&lvi);
    }
    sureset(tex,0,sizeof(tex));
    VirtualUnlock(tex,sizeof(tex));
}


void EncReady(void * kset)
{

    keyset * eset = (keyset*)kset;
    fstream ffile;
    ffile.rdbuf()->pubsetbuf(0,0);
    int tempsteg=0;

    if (formode==2)
    {
        StegOption(eset);
        if (eset->cipherfile[0]=='\0'){return;}
        formode=1;
        tempsteg=1;
    }

    SendMessage(EncProg,PBM_SETPOS,100,0);
    ShowWindow(EncProg,SW_HIDE);


    if (formode==1)
    {
        strcpy((char*)eset->buffer,"Encryption complete. Cipher-file");
        if (strlen(strrchr(eset->cipherfile,'\\')+1)<25)
        {
            strcat((char*)eset->buffer," ('");
            strcat((char*)eset->buffer,strrchr(eset->cipherfile,'\\')+1);
            strcat((char*)eset->buffer,"')");
        }


        if (desmode==1)
        {
            ListView_DeleteAllItems(CFileHolder);
            LVITEM lvi={};
            lvi.iItem=1;
            int ni=ListView_InsertItem(CFileHolder,&lvi);
            ListView_SetItemText(CFileHolder,ni,0,(LPSTR)strrchr(eset->cipherfile,'\\')+1);
            ListView_SetItemText(CFileHolder,ni,1,(LPSTR)eset->cipherfile);
            Addicons(CFileHolder);
            ListView_SetBkColor(CFileHolder,RGB(0,255,0));
            ListView_SetTextBkColor(CFileHolder,RGB(0,255,0));
            ListView_SetItemState(CFileHolder,-1,LVIS_SELECTED,LVIS_SELECTED);
            InvalidateRect(CFileHolder,NULL,true);
            SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)ebuttonpic0);
            ShowWindow(CFileHolder,SW_SHOW);
            SendMessage(CFileHolder,WM_SETFOCUS,1,1);
            SendMessage(EncryptButton,WM_SETFOCUS,1,1);
            strcat((char*)eset->buffer," is ready for drag-drop from green box below.");
        }

        if (desmode==3)
        {
            HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE|GMEM_ZEROINIT|GMEM_DDESHARE, sizeof(DROPFILES) + strlen(eset->cipherfile) + 2);
            DROPFILES *df = (DROPFILES*) GlobalLock(hGlobal);
            ZeroMemory(df, sizeof(DROPFILES) + strlen(eset->cipherfile) + 2);
            df->pFiles = sizeof(DROPFILES);
            df->fWide = FALSE;
            memcpy(&df[1],eset->cipherfile,strlen(eset->cipherfile));
            GlobalUnlock(hGlobal);
            OpenClipboard(0);
            EmptyClipboard();
            SetClipboardData(CF_HDROP, hGlobal);
            CloseClipboard();
            strcat((char*)eset->buffer," has been copied to clipboard for Paste.");
        }

        if (desmode==2)
        {
            strcat((char*)eset->buffer," has been placed in specified output folder.");
        }

        syme(1,(char*)eset->buffer);
        if (tempsteg==1){formode=2;}
    }



    if (formode==3)
    {
        ffile.open(eset->cipherfile,ios_base::binary|ios_base::out|ios_base::in);
        ffile.clear();
        ffile.seekg(0,ios_base::beg);
        ffile.seekg(0,ios_base::end);
        long long cfilesize=ffile.tellg();
        ffile.seekg(0,ios_base::beg);

        char * cfilebuff = new char[cfilesize];
        VirtualLock(cfilebuff,cfilesize);
        sureset(cfilebuff,0,cfilesize);
        ffile.read(cfilebuff,cfilesize);

        long long textsize=((cfilesize - 1) / 3) * 4 + 4+1;
        char * b64buff = new char[textsize];
        VirtualLock(b64buff,textsize);
        sureset(b64buff,0,textsize);

        Base64Encode(cfilebuff,b64buff,cfilesize);
        ffile.close();


        if (desmode==1)
        {
            SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"");
            commode=1;
            SendMessage(OutEdit,WM_SETFONT,(WPARAM)segoe16,true);
            SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)b64buff);
            ListView_DeleteAllItems(OutList);
            ClearDecDetails();
            DecDetails.cformat=3;DecDetails.dformat=3;
            syme(1,"Encryption complete. Ciphertext is ready on-screen.");
        }

        if (desmode==2)
        {
            ZeroFile(eset->cipherfile);
            DeleteFile(eset->cipherfile);
            ffile.open(eset->cipherfile,ios_base::in|ios_base::out|ios_base::binary|ios_base::trunc);
            ffile.clear();
            ffile.write(b64buff,textsize);
            ffile.close();
            syme(1,"Encryption complete. Ciphertext has been written to file.");
        }

        if (desmode==3)
        {
            HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, textsize+1);
            memcpy(GlobalLock(hMem), b64buff, textsize);
            GlobalUnlock(hMem);
            OpenClipboard(0);
            EmptyClipboard();
            SetClipboardData(CF_TEXT, hMem);
            CloseClipboard();
            syme(1,"Encryption complete. Ciphertext has been copied to clipboard for Paste.");
        }


        sureset(cfilebuff,0,cfilesize);
        sureset(b64buff,0,textsize);
        VirtualUnlock(cfilebuff,cfilesize);
        VirtualUnlock(b64buff,textsize);
    }
}


void StegOption (void * kset)
{

    keyset * eset = (keyset*)kset;
    ifstream infile;
    infile.rdbuf()->pubsetbuf(0,0);
    infile.clear();
    infile.open(eset->cipherfile);
    infile.seekg(0,infile.end);
    long long totalsize=(long long)infile.tellg()+(long long)1000;
    infile.seekg(0);
    infile.close();


    // Specific jpeg steg option
    if (SendMessage(SpecificJpegOpt,BM_GETCHECK,1,0))
        {
            do {
                    SendMessage(SpecificJpegName,WM_GETTEXT,sizeof(eset->stegofile),(LPARAM)eset->stegofile);
                    if (strlen(eset->stegofile)<1)
                        {Mbox("You have chosen to use a specific Jpeg image as a carrier for the encrypted data,\nbut not selected a Jpeg for this purpose.\n\nYou will now be asked to pick a suitable Jpeg file for usage. If you do not select such a file, encryption will be canceled.");JpegRequestor(1);}

                    SendMessage(SpecificJpegName,WM_GETTEXT,sizeof(eset->stegofile),(LPARAM)eset->stegofile);

                    if (strlen(eset->stegofile)<1)
                    {
                        eset->buffer[0]='\0';
                        syme(2,"Encryption canceled: No Jpeg selected.");
                        break;
                    }

                    ifstream stegfile;
                    stegfile.rdbuf()->pubsetbuf(0,0);
                    stegfile.open(eset->stegofile);

                    if (!stegfile.good())
                    {
                        stegfile.close();
                        eset->buffer[0]='\0';
                        syme(7,"Encryption canceled: Unable to access selected Jpeg file.");
                        break;
                    }
                    stegfile.close();


                    if (desmode==2)
                    {
                        SendMessage(FolderOutput,WM_GETTEXT,MAX_PATH,(LPARAM)eset->buffer);
                        safescat(eset->buffer,"\\");
                        if(strlen((char*)eset->buffer)<2){strcpy((char*)eset->buffer,"\x1b");}
                        safescat(eset->buffer,strrchr(eset->stegofile,'\\')+1);
                    }
                    else
                    {
                        SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,(LPSTR)eset->buffer);
                        GetModuleFileNameA(NULL, (LPSTR)eset->buffer, MAX_PATH); strrchr((LPSTR)eset->buffer,'\\')[0]=0;
                        safescat(eset->buffer,"\\CenoCipherTempFiles\\");
                        CreateDirectory((LPSTR)eset->buffer,NULL);

                        //safescat(eset->buffer,"\\cenocipher\\");
                        safescat(eset->buffer,strrchr(eset->stegofile,'\\')+1);
                    }


                    ofstream outfile;
                    outfile.rdbuf()->pubsetbuf(0,0);
                    outfile.open((char*)eset->buffer,ios_base::binary|ios_base::out);

                    if (!outfile.is_open())
                    {
                        eset->buffer[0]='\0';
                        syme(7,"Encryption canceled: Unable to write outgoing data.");
                        break;
                    }
                    outfile.close();


                    ZeroFile((char*)eset->buffer);
                    DeleteFile((char*)eset->buffer);

                    sha256((unsigned char*)"pseudokeystring",strlen("pseudokeystring"),eset->pseudosalt,0);
                    PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->pseudosalt,sizeof(eset->pseudosalt),100000,sizeof(eset->pseudokey),eset->pseudokey);

                    int stegres=cenostegembed(eset->stegofile,(char*)eset->buffer,eset->cipherfile,(char*)eset->pseudokey,eset->randdist);

                    if (stegres==2)
                    {
                        syme(7,"Encryption aborted: The selected Jpeg file has insufficient capacity to store the data.");
                        ZeroFile((char*)eset->buffer);
                        DeleteFile((char*)eset->buffer);
                        eset->buffer[0]='\0';
                        break;
                    }

                    if(stegres==1)
                    {
                        syme(7,"Encryption aborted: The selected Jpeg file appears to be either corrupted or invalid.");
                        ZeroFile((char*)eset->buffer);
                        DeleteFile((char*)eset->buffer);
                        eset->buffer[0]='\0';
                        break;
                    }


                } while (0);


            ZeroFile(eset->cipherfile);
            DeleteFile(eset->cipherfile);
            safescopy(eset->cipherfile,eset->buffer);

            if (eset->cipherfile[0]=='\0')
            {
                SendMessage(EncryptButton,BM_SETIMAGE,IMAGE_BITMAP,(LPARAM)ebuttonpic0);
                SendMessage(EncProg,PBM_SETPOS,0,0);
                ShowWindow(EncProg,SW_SHOW);ShowWindow(EncProg,SW_HIDE);
            }


        }



  // Random folder steg option
  if (SendMessage(RandomJpegOpt,BM_GETCHECK,1,0))
        {
          vector<char*> jpegs;
          do{
            SendMessage(RandomJpegName,WM_GETTEXT,sizeof(eset->stegofile),(LPARAM)eset->stegofile);
            if (strlen(eset->stegofile)<1)
                {Mbox("You have chosen to use a Random Jpeg image as a carrier for the encrypted data,\nbut not selected a folder for this purpose.\n\nYou will now be asked to pick a suitable folder for usage. If you do not select such a folder, encryption will be canceled.");FolderOutputRequestor(RandomJpegName,1);}

            SendMessage(RandomJpegName,WM_GETTEXT,sizeof(eset->stegofile),(LPARAM)eset->stegofile);

            if (strlen(eset->stegofile)<1)
            {eset->buffer[0]='\0';syme(2,"Encryption aborted: No Jpeg folder selected.");break;}

            ListJFiles(eset->stegofile,jpegs,totalsize *67 );

            if (jpegs.size()<1)
            {syme(7,"Encryption aborted: No suitable Jpeg carrier files found in chosen location.");eset->buffer[0]='\0';break;}

            long picselect=eset->randdist%(jpegs.size());
            safescopy(eset->stegofile,jpegs.at(picselect));

            sha256((unsigned char*)"pseudokeystring",strlen("pseudokeystring"),eset->pseudosalt,0);
            PKCS5_PBKDF2_HMAC(eset->passkey,strlen((char*)eset->passkey),eset->pseudosalt,sizeof(eset->pseudosalt),100000,sizeof(eset->pseudokey),eset->pseudokey);


            for (int x=-1;x<(int)jpegs.size();x++)
            {
                if (x>=0){safescopy(eset->stegofile,jpegs.at(x));}
                SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,(LPSTR)eset->buffer);
                GetModuleFileNameA(NULL, (LPSTR)eset->buffer, MAX_PATH); strrchr((LPSTR)eset->buffer,'\\')[0]=0;
                safescat(eset->buffer,"\\CenoCipherTempFiles\\");
                CreateDirectory((LPSTR)eset->buffer,NULL);

                //safescat(eset->buffer,"\\cenocipher\\");

                if (desmode==2){SendMessage(FolderOutput,WM_GETTEXT,MAX_PATH,(LPARAM)eset->buffer);safescat(eset->buffer,"\\");}
                safescat(eset->buffer,strrchr(eset->stegofile,'\\')+1);
                ZeroFile((char*)eset->buffer);
                DeleteFile((char*)eset->buffer);
                if (!cenostegembed(eset->stegofile,(char*)eset->buffer,eset->cipherfile,(char*)eset->pseudokey,eset->randdist));
                {break;}
                ZeroFile((char*)eset->buffer);
                DeleteFile((char*)eset->buffer);
                eset->buffer[0]='\0';
            }


            if(eset->buffer[0]=='\0')
            {syme(7,"Encryption aborted. No suitable Jpeg carrier files found in chosen location.");}

          } while (0);

          for (int x=0;x<(int)jpegs.size();x++)
          {
              sureset(jpegs.at(x),0,MAX_PATH*2+1);
              VirtualUnlock(jpegs.at(x),MAX_PATH*2+1);
              delete[] jpegs.at(x);
          }

            ZeroFile(eset->cipherfile);
            DeleteFile(eset->cipherfile);
            safescopy(eset->cipherfile,eset->buffer);

        }

}



void ContAck(int mode, int source, char * p)
{

    SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"");
    SendMessage(OutEdit ,WM_SETFONT,(WPARAM)webdings108,true);
    SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,255));



    char symess[MAX_PATH]={};
    VirtualLock(symess,sizeof(symess));

    if (mode==1)
    {
        SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"\n   \x3d");
        strcpy(symess,"File");
        if (strlen(p)<25)
        {
            strcat(symess," (");
            strcat(symess,p);
            strcat(symess,")");
        }

    }
    if (mode==2)
    {
        SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"\n   \x33");
        strcpy(symess,"Text");
    }


    if (source==1){strcat(symess," has been accepted. Please enter passphrase.");}
    if (source==2){strcat(symess," has been loaded. Please enter passphrase.");}
    if (source==3){strcat(symess," has been loaded from clipboard. Please enter passphrase.");}
    syme(4,symess);

    sureset(symess,0,sizeof(symess));
    VirtualUnlock(symess,sizeof(symess));

    ListView_DeleteAllItems(OutList);
    if (SendMessage(AutoDecrypt,BM_GETCHECK,0,0) and SendMessage(OutPass,WM_GETTEXTLENGTH,0,0)>0){CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)DecryptComm,NULL,0,NULL);SetFocus(OutPass);return;}
    SetFocus(OutPass);return;

}


bool RtlFallback(PVOID p,ULONG u)
{
    return false;
}


int CheckStatus()
{
    if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return(1);}
    if (dectraffic!=0 or enctraffic!=0){ReleaseMutex(mtx2);syme(7," \xbb Please cancel or wait for current operation to complete \xab");return 1;}
    else {ReleaseMutex(mtx2);}
    return (0);
}




void QuickHelp()
{
    if (CheckStatus()){return;}

    RECT mwp;
    GetWindowRect(OutEdit,&mwp);

    insmode=1;InsToggle();
    MoveWindow(WaitWindow,-2590 hh,294 vv,760 hh,500 vv,true);
    ShowWindow(WaitWindow,SW_SHOW);
    SendMessage(WaitLabel,WM_SETTEXT,0,(LPARAM)"TO CREATE AN OUTGOING COMMUNICATION: \n(See onscreen highlighted guide)\n\n\nStep 1. Choose an output mode (Normal file, Modified Jpeg, or Text)                       \n\nStep 2. Choose an output destination (Screen, Clipboard, or Folder) \n\nStep 3. Enter outgoing message text in box provided \n\nStep 4. Attach any files if desired \n\nStep 5. Type a passphrase of your choice \n\nStep 6. Click button (or hit Enter) to encrypt \n\n\nClick OK below to see Incoming communication instructions");
    InvalidateRect(WaitLabel,NULL,true);
    UpdateWindow(WaitLabel);
    SetWindowPos(WaitWindow, NULL, mwp.left + (100 hh),mwp.top+(50 vv),10 vv,10 vv,SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
    EnableWindow(hwndmain,false);
}


BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam)
{
    static int hcount=0;

    if (((unsigned*)lParam)[hcount]==0)
    {
        ((unsigned*)lParam)[hcount]=(unsigned)hwnd;
        hcount++;
        return true;
    }
    else
    {
        hcount=0;
        return false;
    }
}





void NewMessage()
{
    if (CheckStatus()){return;}
    commode=0;
    SendMessage(OutEdit,EM_SETREADONLY,false,0);
    SendMessage(OutEdit,WM_SETTEXT,0,(LPARAM)"");
    ListView_DeleteAllItems(CFileHolder);
    ListView_DeleteAllItems(OutList);
    ListView_SetBkColor(CFileHolder,RGB(255,255,255));
    ListView_SetTextBkColor(CFileHolder,RGB(255,255,255));
    InvalidateRect(CFileHolder,NULL,true);
    SendMessage(EncStat,WM_SETTEXT,0,(LPARAM)"");
    SendMessage(OutEdit,WM_SETFONT,(WPARAM)segoe16,true);
    ShowWindow(EncProg,SW_HIDE);
    if (SendMessage(ClearPassword,BM_GETCHECK,0,0))
    {SendMessage(OutPass,WM_SETTEXT,0,(LPARAM)"");}
    ShowWindow(CFileHolder,SW_HIDE);
    syme();
    InsToggle();
    SetFocus(OutEdit);
    return;
}


void PromptAction(int choice)
{
    ShowPrompt(0);

    if (choice==1 and promptver==2 and !SendMessage(PromptAbort,BM_GETCHECK,0,0)){promptresult=1;OpenInListFiles();}
    if (choice==1 and promptver==3 and !SendMessage(PromptAbort,BM_GETCHECK,0,0)){promptresult=1;OpenInListFiles(2);SetFocus(OutEdit);}
    if (choice==1 and promptver==3 and SendMessage(PromptAbort,BM_GETCHECK,0,0)){ListView_DeleteAllItems(OutList);SetFocus(OutEdit);}
    if (choice==1 and promptver==1)
    {
        if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return;}
        if (SendMessage(PromptAbort,BM_GETCHECK,0,0)){dectraffic=2;}
        if (SendMessage(PromptUseDefault,BM_GETCHECK,0,0)){dectraffic=6;}
        if (SendMessage(PromptUseSpecified,BM_GETCHECK,0,0)){dectraffic=7;}
        ReleaseMutex(mtx2);
    }


    if (choice==0 and promptver==1)
    {
        if (WaitForSingleObject(mtx2,10000)!=WAIT_OBJECT_0){return;}
        dectraffic=2;
        ReleaseMutex(mtx2);
    }
    if (choice==0 and promptver==3){ListView_DeleteAllItems(OutList);SetFocus(OutEdit);}

    promptresult=0;

}


long redge(HWND h)
{
    RECT r;
    GetWindowRect(h,&r);
    return r.right-1;
}

long ledge(HWND h)
{
    RECT r;
    GetWindowRect(h,&r);
    return r.left;
}
