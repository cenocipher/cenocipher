#include <ole2.h>
#include <shlobj.h>
#include <objidl.h>
#include <vector>


UINT cf_email = RegisterClipboardFormat("Internet Message (rfc822/rfc1522)");
UINT cf_content = RegisterClipboardFormat("FileContents");
UINT cf_descrip = RegisterClipboardFormat("FileGroupDescriptor");
UINT cf_descripw = RegisterClipboardFormat("FileGroupDescriptorW");
UINT cf_rpi = RegisterClipboardFormat("RenPrivateItem");

class CDropTarget : public IDropTarget
{
public:
    // IUnknown implementation
    HRESULT __stdcall QueryInterface (REFIID iid, void ** ppvObject);
    ULONG   __stdcall AddRef (void);
    ULONG   __stdcall Release (void);

    // IDropTarget implementation
    HRESULT __stdcall DragEnter(IDataObject * pDataObject, DWORD grfKeyState, POINTL pt, DWORD * pdwEffect);
    HRESULT __stdcall DragOver(DWORD grfKeyState, POINTL pt, DWORD * pdwEffect);
    HRESULT __stdcall DragLeave(void);
    HRESULT __stdcall Drop(IDataObject * pDataObject, DWORD grfKeyState, POINTL pt, DWORD * pdwEffect);

    // Constructor
     CDropTarget(HWND hwnd);
    virtual ~CDropTarget();


private:
    // internal helper function
    DWORD DropEffect(DWORD grfKeyState, POINTL pt, DWORD dwAllowed);
    bool  QueryDataObject(IDataObject *pDataObject);

    // Private member variables
    long   m_lRefCount;
    HWND   m_hWnd;
    bool   m_fAllowDrop;
    COLORREF curcol;




    // Other internal window members

};

HRESULT __stdcall CDropTarget::QueryInterface(REFIID iid, void** ppvObject)
{
    if(iid == ::IID_IDropTarget || iid == ::IID_IUnknown)
	{
		AddRef();
		*ppvObject = this;
		return S_OK;
	}
	else
	{
		*ppvObject = 0;
		return E_NOINTERFACE;
	}
}

ULONG __stdcall CDropTarget::AddRef(void)
{
    return InterlockedIncrement(&m_lRefCount);
}

ULONG __stdcall CDropTarget::Release(void)
{
    LONG count = InterlockedDecrement(&m_lRefCount);

	if(count == 0)
	{
		delete this;
		return 0;
	}
	else
	{
		return count;

	}
}

CDropTarget::CDropTarget(HWND hwnd)
{
    m_lRefCount  = 1;
	m_hWnd       = hwnd;
	m_fAllowDrop = false;
}

HRESULT __stdcall CDropTarget::DragEnter(IDataObject* pDataObject, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect)
{
    if (CheckStatus()){return E_INVALIDARG;}
    if (*pdwEffect==0)
    {
        return E_INVALIDARG;
    }
	else
    {
        m_fAllowDrop=true;
        *pdwEffect=DROPEFFECT_COPY;
        curcol=SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,245,255));
    }
	return S_OK;
}

HRESULT __stdcall CDropTarget::DragLeave(void)
{
    SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,curcol);
    return S_OK;
}

HRESULT __stdcall CDropTarget::DragOver(DWORD grfKeyState, POINTL pt, DWORD* pdwEffect)
{
     if (*pdwEffect==0)
    {
          return E_INVALIDARG;
    }
	else if (m_fAllowDrop==true)
    {
        *pdwEffect=DROPEFFECT_COPY;
    }
	return S_OK;
}

HRESULT __stdcall CDropTarget::Drop(IDataObject* pDataObject, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect)
{
    if (CheckStatus()){return 0;}

    if (DecDetails.csp){DecDetails.csp->Release();DecDetails.csp=NULL;}
    if (DecDetails.cstgmed){ReleaseStgMedium(DecDetails.cstgmed);}

    if (DecDetails.dbuffer)
        {
            sureset(DecDetails.dbuffer,0,DecDetails.dbuffsize);
            VirtualUnlock(DecDetails.dbuffer,DecDetails.dbuffsize);
            SetProcessWorkingSetSize(hProcess,defmin,defmax);
            delete[] DecDetails.dbuffer;
            DecDetails.dbuffer=NULL;
        }

    ListView_DeleteAllItems(OutList);
    SendMessage(DecStat,WM_SETTEXT,0,(WPARAM)"");
    ShowWindow(DecProg,SW_HIDE);

    sureset(&DecDetails,0,sizeof(DecDetails));
    STGMEDIUM * stgmed = new STGMEDIUM;

    FORMATETC fmtetc = { (CLIPFORMAT)cf_descrip, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };


    //Outlook or Thunderbird type
	if(pDataObject->GetData(&fmtetc,stgmed) == S_OK)
	{
        FILEGROUPDESCRIPTOR * fgdbuffer;
        fgdbuffer=(FILEGROUPDESCRIPTOR*)GlobalLock(stgmed->hGlobal);
        safescopy(DecDetails.cfile,fgdbuffer->fgd[0].cFileName);
        GlobalUnlock(stgmed->hGlobal);

        fmtetc.cfFormat=(CLIPFORMAT)cf_content;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=0;fmtetc.tymed=TYMED_ISTREAM;

        if(pDataObject->GetData(&fmtetc,stgmed) != S_OK)
        {
            fmtetc.cfFormat=(CLIPFORMAT)cf_content;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=0;fmtetc.tymed=TYMED_HGLOBAL;
            if (pDataObject->GetData(&fmtetc,stgmed) !=S_OK)
            {
                SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,curcol);UpdateWindow(OutEdit);
                syme(7,"Note: From desktop email client, drag-drop file attachment instead of entire email message.");

                return 0;
            }
        }


        DecDetails.cformat=2;
        DecDetails.cstgmed=stgmed;
        ContAck(1,1,DecDetails.cfile);

        IStream * idt;
        stgmed->pstm->QueryInterface(IID_IStream,(LPVOID*)&idt);
        CoMarshalInterThreadInterfaceInStream(IID_IStream,idt,&DecDetails.csp);
        ListView_DeleteAllItems(InList);
        commode=1;
        InsToggle();
        SetFocus(OutPass);
        SendMessage(OutPass,EM_SETSEL,0,999);

	}
    else  //Regular file drop
    {
        fmtetc.cfFormat=CF_HDROP;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=0;fmtetc.tymed=TYMED_HGLOBAL;
        if (pDataObject->GetData(&fmtetc,stgmed) == S_OK)
        {
            DragQueryFile((HDROP)stgmed->hGlobal,0,DecDetails.cfile,sizeof(DecDetails.cfile));
            SendMessage(OutEdit,EM_SETBKGNDCOLOR,0,RGB(215,215,255));
            ListView_DeleteAllItems(OutList);
            ListView_SetBkColor(OutList,RGB(215,215,215));
            ListView_SetTextBkColor(OutList,RGB(215,215,215));
            DecDetails.cformat=1;
            commode=1;
            InsToggle();
            ContAck(1,1,strrchr(DecDetails.cfile,'\\')+1);
        }
        else //Text drop
        {
            fmtetc.cfFormat=CF_TEXT;fmtetc.ptd=0;fmtetc.dwAspect=DVASPECT_CONTENT;fmtetc.lindex=-1;fmtetc.tymed=TYMED_HGLOBAL;
            if (pDataObject->GetData(&fmtetc,stgmed) == S_OK)
            {
                char * b64text = (char*)(GlobalLock(stgmed->hGlobal));
                while (b64text[0]==' ' or b64text[0]==10 or b64text[0]==13){b64text++;}
                if (strchr(b64text,' ')!=NULL){strchr(b64text,' ')[0]='\0';}
                if (strchr(b64text,10)!=NULL){strchr(b64text,10)[0]='\0';}
                if (strchr(b64text,13)!=NULL){strchr(b64text,13)[0]='\0';}
                long texlen=strlen(b64text);
                long i; char* bp;
                for (i=0,bp=b64text; bp[i]; bp[i]=='=' ? i++ : *bp++){};
                long flen = (texlen * 3 /4) - i;
                long blen = (texlen * 3 /4) +4;
                char* fbuff = new char[blen];
                VirtualLock(fbuff,blen);
                Base64Decode(b64text,fbuff);
                SHGetFolderPath(0,CSIDL_APPDATA,NULL,0,DecDetails.cfile);
                safescat(DecDetails.cfile,"\\cenocipher\\CCB64.dat");
                ZeroFile(DecDetails.cfile);
                DeleteFile(DecDetails.cfile);
                ofstream outfile;
                outfile.open(DecDetails.cfile,ios_base::out|ios_base::binary);
                outfile.write(fbuff,flen);
                outfile.close();
                sureset(fbuff,0,blen);
                VirtualUnlock(fbuff,blen);
                DecDetails.cformat=1;
                commode=1;
                InsToggle();
                ContAck(2,1,0);
                SetFocus(OutPass);
                SendMessage(OutPass,EM_SETSEL,0,999);
                GlobalUnlock(stgmed->hGlobal);
            }
            else
            {
                syme(7,"Error loading requested data");
            }

        }

    }
    return 0;
}

CDropTarget::~CDropTarget()
{

}




