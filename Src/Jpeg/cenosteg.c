/*
 *
 *  CenoCipher 3.0 Steganography Module
 *
 *  Copyright (C) 2015, the CenoCipher development team
 *
 *  All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */




#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include "jpeglib.h"
#include <setjmp.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "irand.h"



struct jpegErrorManager {
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
};

char jpegLastErrorMsg[JMSG_LENGTH_MAX];

static void jpegErrorExit (j_common_ptr cinfo)
{
    struct jpegErrorManager* myerr = (struct jpegErrorManager*) cinfo->err;
    ( *(cinfo->err->format_message) ) (cinfo, jpegLastErrorMsg);
    longjmp(myerr->setjmp_buffer, 1);
}

static void mmu(int matA[4][15], int matB[15],int matC[4])
{
    memset(matC,0,sizeof(matC));
    int i,j;
    for (i=0;i<4;i++)
    {
        for(j=0;j<15;j++)
        {
            matC[i]+= matA[i][j] * matB[j];matC[i]%=2;
        }
    }
}

static void sureset( void *v, unsigned char u, size_t n )
{
    volatile unsigned char *p = (volatile unsigned char *)v;
    while( n-- )
    {*p++ = u;}
}



int cenostegembed (const char* infilename, const char* outfilename, const char* hidefilename,const char* pseudokey,long rseed)
{
    struct jpeg_decompress_struct inputinfo;
    struct jpeg_compress_struct outputinfo;
    struct jpeg_error_mgr jerr;
    jvirt_barray_ptr *coef_arrays;
    JDIMENSION i, compnum, rownum, blocknum;
    JBLOCKARRAY coef_buffers[MAX_COMPONENTS];
    JBLOCKARRAY row_ptrs[MAX_COMPONENTS];
    FILE * input_file;
    FILE * output_file;

    FILE * f= fopen(hidefilename,"rb");
    remove(outfilename);

    if ((input_file = fopen(infilename, "rb")) == NULL) {return (1);}
    if ((output_file = fopen(outfilename, "wb")) == NULL) {return (1);}

    fseek(f,0,SEEK_END);
    long long fsize=ftell(f);
    fseek(f,0,SEEK_SET);

    struct jpegErrorManager custerr;
    inputinfo.err = jpeg_std_error(&custerr.pub);
    custerr.pub.error_exit = jpegErrorExit;

    if (setjmp(custerr.setjmp_buffer)) {
    jpeg_destroy_decompress(&inputinfo);
    fclose(input_file);
    fclose(output_file);
    fclose(f);
    return 1;
    }


  jpeg_create_decompress(&inputinfo);
  outputinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&outputinfo);
  jpeg_stdio_src(&inputinfo, input_file);
  jpeg_stdio_dest(&outputinfo, output_file);

  (void) jpeg_read_header(&inputinfo, TRUE);
  for (compnum=0; compnum<inputinfo.num_components; compnum++)
    coef_buffers[compnum] = ((&inputinfo)->mem->alloc_barray)
                            ((j_common_ptr) &inputinfo, JPOOL_IMAGE,
                             inputinfo.comp_info[compnum].width_in_blocks,
                             inputinfo.comp_info[compnum].height_in_blocks);


  coef_arrays = jpeg_read_coefficients(&inputinfo);

  int num_components = inputinfo.num_components;
  size_t block_row_size[num_components];
  int width_in_blocks[num_components];
  int height_in_blocks[num_components];
  for (compnum=0; compnum<num_components; compnum++)
  {
    height_in_blocks[compnum] = inputinfo.comp_info[compnum].height_in_blocks;
    width_in_blocks[compnum] = inputinfo.comp_info[compnum].width_in_blocks;
    block_row_size[compnum] = (size_t) sizeof(JCOEF)*DCTSIZE2*width_in_blocks[compnum];
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      row_ptrs[compnum] = ((&inputinfo)->mem->access_virt_barray)
                          ((j_common_ptr) &inputinfo, coef_arrays[compnum],
                            rownum, (JDIMENSION) 1, FALSE);
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      {
        for (i=0; i<DCTSIZE2; i++)
        {
          coef_buffers[compnum][rownum][blocknum][i] = row_ptrs[compnum][0][blocknum][i];
        }
      }
    }
  }


    int x=0;
    randctx rctx2={};
    for (x=0;x<8;x++)
    {rctx2.randrsl[x]=*(ub4*)(pseudokey+4*x);}
    randinit(&rctx2,1);

    randctx rctx={};
    rctx.randrsl[0]=(ub4)(rseed);
    randinit(&rctx,1);


  long long uac=0;

  for (compnum=0; compnum<num_components; compnum++)
  {
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      { int cmade=0;int crand=irand(&rctx)%4;
        for (i=0; i<DCTSIZE2; i++)
        {
          if ((int)i>0 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){uac+=1;}
          if(((coef_buffers[compnum][rownum][blocknum][i]<-3 && !(coef_buffers[compnum][rownum][blocknum][i] & 1)) || (coef_buffers[compnum][rownum][blocknum][i]>2 && (coef_buffers[compnum][rownum][blocknum][i] & 1)) )  && (i>=0) && (cmade==0) && (crand==0))
            {
                if(coef_buffers[compnum][rownum][blocknum][i]<-3){coef_buffers[compnum][rownum][blocknum][i]++;}
                else if(coef_buffers[compnum][rownum][blocknum][i]>2){coef_buffers[compnum][rownum][blocknum][i]--;}
                cmade=1;
            }
        }
      }
    }
  }


  if ((uac/15)/2<fsize+8){fclose(f);fclose(input_file);fclose(output_file);return(2);}

  long cgasize=(uac * sizeof(int));
  int* cga = malloc(cgasize);
  sureset(cga,0,cgasize);
  long long iac=0;

  double perused = ((((double)(fsize)+8)*30)/uac)*100;
  if (perused>78){fclose(f);fclose(input_file);fclose(output_file);return(2);}
  int peruse=(int)perused+10;
  if (peruse>80){peruse=80;}
  int attempts=0;

  while (iac<(fsize+8)*30 && attempts<100)
  {
   sureset(cga,0,cgasize);iac=0;attempts++;
      for (compnum=0; compnum<num_components; compnum++)
      {
        for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
        {
          for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
          {
           for (i=0; i<DCTSIZE2; i++)
            {
              unsigned checkrand=irand(&rctx2);
              if ((int)i>0 &&  (checkrand%5 <4) && iac<240 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){cga[iac++]=coef_buffers[compnum][rownum][blocknum][i];}
              else if ((int)i>0 &&  (checkrand % 100 <peruse) && iac>=240 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){cga[iac++]=coef_buffers[compnum][rownum][blocknum][i];}
            }
          }
        }
      }
  } //while

    if (attempts>=99){fclose(f);fclose(input_file);fclose(output_file);sureset(cga,0,cgasize);free(cga);return(2);}
    if ((iac/15)/2<fsize+8){fclose(f);fclose(input_file);fclose(output_file);sureset(cga,0,cgasize);free(cga);return(2);}


    unsigned char cbyte=0;
    int cgroup=0;
    int cseg=0;
    int bita[8]={};
    int mref[4][15]={{0,0,0,0,0,0,0,1,1,1,1,1,1,1,1},{0,0,0,1,1,1,1,0,0,0,0,1,1,1,1},{0,1,1,0,0,1,1,0,0,1,1,0,0,1,1},{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1}};
    int mtest[15]={};
    int mresult[4]={};
    int diff[4]={};

    unsigned char* ucp=(unsigned char*)&fsize;


    while (fread(&cbyte,1,1,f))
    {
        for(x=0;x<8;x++){bita[x]= (cbyte >> (7-x)) & 1;}

        if (cgroup<16)
        {
            fseek(f,-1,SEEK_CUR);
            cbyte=ucp[(int)(cgroup/2)];
            for(x=0;x<8;x++){bita[x]= (cbyte >> (7-x)) & 1;}
        }

        for(cseg=0;cseg<2;cseg++)
        {
            for(x=0;x<15;x++){mtest[x]=(cga[15*cgroup+x])&1;}
            memset(mresult,0,sizeof(mresult));
            mmu(mref,mtest,mresult);
            for(x=0;x<4;x++){diff[x]=abs(mresult[x]-bita[4*cseg+x]);diff[x]%=2;}
            if(diff[0]==0 && diff[1] == 0 && diff[2] == 0 && diff[3] == 0){cgroup++;continue;}
            for (x=0;x<15;x++)
            {
                if (mref[0][x]==diff[0] &&  mref[1][x]==diff[1] && mref[2][x]==diff[2] && mref[3][x]==diff[3])
                {
                    if(cga[15*cgroup+x]==2){cga[15*cgroup+x]+=1;}
                    else if(cga[15*cgroup+x]>=3){cga[15*cgroup+x]--;}
                    else if(cga[15*cgroup+x]==-2){cga[15*cgroup+x]-=1;}
                    else if(cga[15*cgroup+x]<=-3){cga[15*cgroup+x]++;}
                    break;
                }
            }
            cgroup++;
        }
    }

    fclose(f);

    memset(&rctx2,0,sizeof(rctx2));
    for (x=0;x<8;x++){rctx2.randrsl[x]=*(ub4*)(pseudokey+4*x);}
    randinit(&rctx2,1);


  long celem=0;
  for (compnum=0; compnum<num_components; compnum++)
  {
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      {
       for (i=0; i<DCTSIZE2; i++)
        {
          unsigned checkrand=irand(&rctx2);
          if ((int)i>0 && (checkrand%5 <4) && celem<240 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){coef_buffers[compnum][rownum][blocknum][i]=cga[celem++];}
          else if ((int)i>0 && (checkrand%100 <peruse) && celem>=240 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){coef_buffers[compnum][rownum][blocknum][i]=cga[celem++];}
        }
      }
    }
  }


  for (compnum=0; compnum<num_components; compnum++)
  {
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      row_ptrs[compnum] = ((&outputinfo)->mem->access_virt_barray)
                          ((j_common_ptr) &outputinfo, coef_arrays[compnum],
                           rownum, (JDIMENSION) 1, TRUE);
      memcpy(row_ptrs[compnum][0][0],
             coef_buffers[compnum][rownum][0],
             block_row_size[compnum]);
    }
  }

  jpeg_copy_critical_parameters(&inputinfo, &outputinfo);
  outputinfo.optimize_coding = TRUE;
  jpeg_write_coefficients(&outputinfo, coef_arrays);
  jpeg_finish_compress(&outputinfo);
  jpeg_destroy_compress(&outputinfo);
  jpeg_finish_decompress(&inputinfo);
  jpeg_destroy_decompress(&inputinfo);
  fclose(input_file);
  fclose(output_file);
  sureset(cga,0,cgasize);
  free(cga);
  return 0;
}

int cenostegextract (const char * infilename, const char * outfilename, const char * pseudokey)
{
  struct jpeg_decompress_struct inputinfo;
  jvirt_barray_ptr *coef_arrays;
  JDIMENSION i, compnum, rownum, blocknum;
  JBLOCKARRAY coef_buffers[MAX_COMPONENTS];
  JBLOCKARRAY row_ptrs[MAX_COMPONENTS];
  FILE * input_file;
  FILE * output_file;

  remove(outfilename);


    if ((input_file = fopen(infilename, "rb")) == NULL) {return 1;}
    if ((output_file = fopen(outfilename, "wb")) == NULL) {fclose(input_file);return 1;}

    struct jpegErrorManager custerr;
    inputinfo.err = jpeg_std_error(&custerr.pub);
    custerr.pub.error_exit = jpegErrorExit;

    if (setjmp(custerr.setjmp_buffer))
    {
        jpeg_destroy_decompress(&inputinfo);
        fclose(input_file);
        fclose(output_file);
        return 1;
    }

  jpeg_create_decompress(&inputinfo);
  jpeg_stdio_src(&inputinfo, input_file);
  (void) jpeg_read_header(&inputinfo, TRUE);

  for (compnum=0; compnum<inputinfo.num_components; compnum++)
    coef_buffers[compnum] = ((&inputinfo)->mem->alloc_barray)
                            ((j_common_ptr) &inputinfo, JPOOL_IMAGE,
                             inputinfo.comp_info[compnum].width_in_blocks,
                             inputinfo.comp_info[compnum].height_in_blocks);

  coef_arrays = jpeg_read_coefficients(&inputinfo);

  int num_components = inputinfo.num_components;
  int width_in_blocks[num_components];
  int height_in_blocks[num_components];
  for (compnum=0; compnum<num_components; compnum++)
  {
    height_in_blocks[compnum] = inputinfo.comp_info[compnum].height_in_blocks;
    width_in_blocks[compnum] = inputinfo.comp_info[compnum].width_in_blocks;
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      row_ptrs[compnum] = ((&inputinfo)->mem->access_virt_barray)
                          ((j_common_ptr) &inputinfo, coef_arrays[compnum],
                            rownum, (JDIMENSION) 1, FALSE);
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      {
        for (i=0; i<DCTSIZE2; i++)
        {
          coef_buffers[compnum][rownum][blocknum][i] = row_ptrs[compnum][0][blocknum][i];
        }
      }
    }
  }


    long uac=0;
    int x=0;
    randctx rctx2={};
    for (x=0;x<8;x++)
    {rctx2.randrsl[x]=*(ub4*)(pseudokey+4*x);}
    randinit(&rctx2,1);


  for (compnum=0; compnum<num_components; compnum++)
  {
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      {
        for (i=0; i<DCTSIZE2; i++)
        {
          if ((int)i>0 && abs(coef_buffers[compnum][rownum][blocknum][i])>1){uac++;}
        }
      }
    }
  }

  long long iac=0;
  int cga[15]={};

  int cseg=0;
  int bita[8]={};
  int mref[4][15]={{0,0,0,0,0,0,0,1,1,1,1,1,1,1,1},{0,0,0,1,1,1,1,0,0,0,0,1,1,1,1},{0,1,1,0,0,1,1,0,0,1,1,0,0,1,1},{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1}};
  int mtest[15]={};
  int mresult[4]={};

  long long sizefile=0;
  unsigned char c=0;
  int factor=5;
  int peruse=4;



  for (compnum=0; compnum<num_components; compnum++)
  {
    for (rownum=0; rownum<height_in_blocks[compnum]; rownum++)
    {
      for (blocknum=0; blocknum<width_in_blocks[compnum]; blocknum++)
      {
       for (i=0; i<DCTSIZE2; i++)
        {
          unsigned checkrand=irand(&rctx2);int act=0;
          if ((int)i>0 && (checkrand%factor <peruse) && abs(coef_buffers[compnum][rownum][blocknum][i])>1){cga[(iac++)%15]=coef_buffers[compnum][rownum][blocknum][i];act=1;}
          if (iac>0 && iac%15==0 && act==1)
          {
              for(x=0;x<15;x++){mtest[x]=(cga[x])&1;}
              memset(mresult,0,sizeof(mresult));
              mmu(mref,mtest,mresult);
              for(x=0;x<4;x++){bita[4*cseg+x]=mresult[x];}
              if(cseg==1)
              {
                  c=0;
                  for(x=0;x<8;x++) {c |= bita[x] * (int)pow(2,7-x);}
                  if (iac<=240){sizefile += c * (int)pow(256,((iac/30)-1));}
                  if (iac==240){double perused = ((((double)(sizefile)+8)*30)/uac)*100;peruse=(int)perused+10;factor=100;}
                  if (iac>240 && iac>sizefile*30 + 240){break;}
                  if (iac>240){fwrite(&c,1,1,output_file);}
              }
              cseg=!cseg;
          }
        }
      }
    }
  }




  fclose(output_file);
  fclose(input_file);
  sureset(cga,0,sizeof(cga));
  return 0;
}
