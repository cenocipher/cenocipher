CenoCipher  |  [Download (910KB)](https://gitlab.com/cenocipher/cenocipher/-/raw/master/bin/CenoCipher.exe)  |  [Screenshot](https://gitlab.com/cenocipher/cenocipher#screenshot)  |  [Documentation](https://gitlab.com/cenocipher/cenocipher/uploads/05a242b2000e7e3ea726637b6a14550f/CenoCipherHelp.pdf) 
==========

## Easy-to-use cryptography and steganography tool

### CenoCipher is a free, open-source, easy-to-use tool for exchanging secure encrypted communications over the internet. It uses strong cryptography to convert messages and files into encrypted cipher-data, which can then be sent to the recipient via regular email or any other channel available, such as instant messaging or shared cloud storage.  
Runs on all versions of Windows, and also on Linux using Wine.

[![Award](https://gitlab.com/cenocipher/cenocipher/uploads/43bfd0271043348a9e9190759cca2926/softpedia.gif)  Certified 100% Free and rated 4.5/5 by Softpedia](http://www.softpedia.com/get/Security/Encrypting/CenoCipher.shtml)

## Features at a glance

* Simple for anyone to use: Just type a message, click Encrypt, and go  
* **Optional steganography feature for embedding encrypted data within a Jpeg image**  
* Handles messages and file attachments together easily
* End-to-end encryption, performed entirely on the user's machine
* No dependence on any specific intermediary channel - works with any communication method available
* Uses three strong cryptographic algorithms in combination to triple-protect data
* No installation needed - fully portable application can be run from anywhere
* Unencrypted data is never written to disk - unless requested by the user
* Multiple input/output modes for convenient operation


## Technical details

* Open source, written in C++
* AES/Rijndael, Twofish and Serpent ciphers (256-bit keysize variants), cascaded together in CTR mode for triple-encryption of messages and files
* HMAC-SHA-256 for construction of message authentication code
* PBKDF2-HMAC-SHA256 for derivation of separate AES, Twofish and Serpent keys from user-chosen passphrase
* Cryptographically safe pseudo-random number generator ISAAC for production of Initialization Vectors (AES/Twofish/Serpent) and Salts (PBKDF2)



## License

CenoCipher is free software, and is released under the GNU General Public License, version 2 or any later version, as published by the Free Software Foundation, Inc.


## Screenshot

![Screenshot](https://gitlab.com/cenocipher/cenocipher/uploads/f388be5d9903e4a17347080b0b5e2e0b/CC4Screenshot.png)
  
## [Download (910KB)](https://gitlab.com/cenocipher/cenocipher/-/raw/master/bin/CenoCipher.exe)  
  
  
| Release Info  |   |
| -------- | -------|  
| Version | 4.1 |  
| Release Date | September 20, 2020 |  
| SHA256 Checksum | 99608a3552752a3c73ea06798ea468a512dd4b2cf649e1252c982cb6672cf003 |  
| Compatible OS | Windows/Linux (wine) |  
  
  
  
## Version History (Change Log)  
  
### Version 4.1 (September 20, 2020)  
* Fixed bug where auto-opening file occasionally failed  
* Changed default folder to application-local directory, for complete portability  
* Minor cosmetic fixes


### Version 4.0 (December 05, 2015)
* Drastically overhauled and streamlined interface
* Added multiple input/output modes for cipher-data
* Added user control over unencrypted disk writes
* Added auto-decrypt and open-with support
* Added more entropy to Salt/IV generation
  
  
### Version 3.0 (June 29, 2015)
* Added Serpent algorithm for cascaded triple-encryption 
* Added steganography option for concealing data within Jpeg
* Added conversation mode for convenience
* Improved header obfuscation for higher security
* Increased entropy in generation of separate salt/IVs used by ciphers
* Many other enhancements under the hood
  
    
### Version 2.1 (December 6, 2014)
* Change cascaded encryption cipher modes from CBC to CTR for extra security
* Improve PBKDF2 rounds determination and conveyance format
* Fix minor bug related to Windows DPI font scaling
* Fix minor bug affecting received filenames when saved by user
  
    
### Version 2.0 (November 26, 2014)
* Initial open-source release
* Many enhancements to encryption algorithms and hash functions
  
    
### Version 1.0 (June 10, 2014)
* Original program release (closed source / beta)